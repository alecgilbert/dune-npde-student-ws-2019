// -*- tab-width: 2; indent-tabs-mode: nil -*-
#ifndef DUNE_NPDE_MESHSIZE_HH
#define DUNE_NPDE_MESHSIZE_HH

template<class GV>
double meshsize (const GV& gv)
{
  // constants and types
  const int dim = GV::Grid::dimension;
  typedef typename GV::Grid::ctype DF;
  typedef typename GV::Traits::template Codim<0>::Iterator ElementIterator;

  // loop over grid view
  DF h = 0.0;
  for (ElementIterator eit = gv.template begin<0>(); eit!=gv.template end<0>(); ++eit)
    {
      Dune::FieldVector<DF,dim> x0 = eit->geometry().corner(0);
      for (int i=1; i<eit->geometry().corners(); i++)
        {
          Dune::FieldVector<DF,dim> x = eit->geometry().corner(i);
          x -= x0;
          h = std::max(h,x.two_norm());
        }
    }
  return h;
}

#endif
