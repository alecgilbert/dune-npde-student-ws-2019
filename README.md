Preparing the Sources for the student tutorials
===============================================

The original `dune-npde` repository contains the solutions to the
exercises. Simply removing these directories is not a good idea because
they can be easily reconstructed with the version control
system. Therefore open a new repository without the solutions as follows

1. In the top-level directory of `dune-npde` extract the sources to a
   tar-ball (similar to `svn export`)

    ```
    git archive --format=tar master | gzip > dune-npde-student.tar.gz
    ```

2. This creates the tar-ball `dune-npde-student.tar.gz` in the current
   directory (the top-level directory). Move it out of the current
   place, e.g. with

    ```
    mv -v dune-npde-student.tar.gz ../
    ```

3. Go to the new position of the tar-ball. Create a directory of the form

    ```
    mkdir -v dune-npde-student-tt-yyyy
    ```

    where `tt=ss,ws` (summer term or winter term) and `yyyy` stands for
    the year the exercises take place.

4. Now extract the tar-ball into this directory

    ```
    tar -xzf dune-npde-student.tar.gz -C dune-npde-student-tt-yyyy/
    ```

5. Go inside the directory `dune-npde-student-tt-yyyy` and delete the
   solution subfolders. Then create an empty repository

    ```
    git init
    ```

    and collect all files with the command

    ```
    git add .
    ```

    followed by the initial commit.

6. Create a new repository in GitLab on `conan2`. It should have the
   name `dune-npde-student-tt-yyyy`. Follow the instructions documented
   in the section `I have an existing Git repository`.

Preparing the Sources
=========================

Additional to the software mentioned in README.md you'll need the
following programs installed on your system:

    cmake >= 2.8.12

Getting started
---------------

If these preliminaries are met, you should run

    dunecontrol all

which will find all installed dune modules as well as all dune modules
(not installed) which sources reside in a subdirectory of the current
directory. Note that if dune is not installed properly you will either
have to add the directory where the dunecontrol script resides (probably
`./dune-common/bin`) to your path or specify the relative path of the script.

Most probably you'll have to provide additional information to dunecontrol
(e. g. compilers, configure options) and/or make options.

The most convenient way is to use options files in this case. The files
define four variables:

```cmake
CMAKE_FLAGS      flags passed to cmake (during configure)
MAKE_FLAGS       flags passed to make
```

An example options file might look like this:

```
#use this options to autogen, configure and make if no other options are given
CMAKE_FLAGS=" \
-DCMAKE_CXX_COMPILER=g++-4.9 \
-DCMAKE_CXX_FLAGS='-Wall -pedantic' \
-DCMAKE_INSTALL_PREFIX=/install/path" #Force g++-4.9 and set compiler flags
MAKE_FLAGS=install #Per default run make install instead of simply make
```

If you save this information into example.opts you can pass the opts file to
dunecontrol via the --opts option, e. g.

    dunecontrol --opts=example.opts all

More info
---------

See

     dunecontrol --help

for further options.


The full build-system is described in the dune-common/doc/buildsystem (Git version) or under share/doc/dune-common/buildsystem if you installed DUNE!
