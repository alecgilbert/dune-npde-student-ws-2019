#!/bin/sh

./ldomain 2 1 2 1e-2 50 0.1 fractiontest > fractiontest_ug_conforming_P1_01.log
./ldomain 2 1 2 1e-2 50 0.2 fractiontest > fractiontest_ug_conforming_P1_02.log
./ldomain 2 1 2 1e-2 50 0.3 fractiontest > fractiontest_ug_conforming_P1_03.log
./ldomain 2 1 2 1e-2 50 0.4 fractiontest > fractiontest_ug_conforming_P1_04.log
./ldomain 2 1 2 1e-2 50 0.5 fractiontest > fractiontest_ug_conforming_P1_05.log
./ldomain 2 1 2 1e-2 50 0.6 fractiontest > fractiontest_ug_conforming_P1_06.log
./ldomain 2 1 2 1e-2 50 0.7 fractiontest > fractiontest_ug_conforming_P1_07.log
./ldomain 2 1 2 1e-2 50 0.8 fractiontest > fractiontest_ug_conforming_P1_08.log
./ldomain 2 1 2 1e-2 50 0.9 fractiontest > fractiontest_ug_conforming_P1_09.log


# Alberta runs with various degrees
./ldomain 1 1 4 5e-2 50 1.0 ldomain_uniform > alberta_P1_uniform.log
./ldomain 1 1 4 2e-2 50 0.7 ldomain_07 > alberta_P1_07.log
./ldomain 1 2 4 2e-3 50 0.7 ldomain_07 > alberta_P2_07.log
./ldomain 1 3 4 2e-4 50 0.7 ldomain_07 > alberta_P3_07.log
./ldomain 1 4 4 2e-5 50 0.7 ldomain_07 > alberta_P4_07.log

# UG conforming runs with various degrees
./ldomain 2 1 2 5e-2 50 1.0 ldomain_uniform > ug_conforming_P1_uniform.log
./ldomain 2 1 2 2e-2 50 0.7 ldomain_07 > ug_conforming_P1_07.log
./ldomain 2 2 2 2e-3 50 0.7 ldomain_07 > ug_conforming_P2_07.log
./ldomain 2 3 2 2e-4 50 0.7 ldomain_07 > ug_conforming_P3_07.log
./ldomain 2 4 2 2e-5 50 0.7 ldomain_07 > ug_conforming_P4_07.log

# Degree 1 runs with all meshes
./ldomain 1 1 4 5e-3 50 0.7 ldomain_07 > alberta_P1_07_compare_meshes.log
./ldomain 2 1 2 5e-3 50 0.7 ldomain_07 > ug_conforming_P1_07_compare_meshes.log
./ldomain 3 1 2 5e-3 50 0.7 ldomain_07 > alusimplex_P1_07_compare_meshes.log
./ldomain 4 1 2 5e-3 50 0.7 ldomain_07 > ug_nonconforming_Q1_07_compare_meshes.log
