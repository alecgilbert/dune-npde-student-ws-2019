#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <dune/common/parallel/mpihelper.hh> // include mpi helper class
#include <dune/grid/onedgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#if HAVE_UG
#include <dune/grid/uggrid.hh>
#include <dune/grid/uggrid/uggridfactory.hh>
#endif
#if HAVE_ALBERTA
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/dgfparser.hh>
#endif
#if HAVE_DUNE_ALUGRID
#include<dune/alugrid/grid.hh>
#include<dune/alugrid/dgf.hh>
#include<dune/grid/io/file/dgfparser/dgfparser.hh>
#endif

#include <dune/grid/io/file/gmshreader.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/io.hh>
#include <dune/istl/paamg/amg.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/common/functionutilities.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/finiteelementmap/p0fem.hh>
#include <dune/pdelab/finiteelementmap/pkfem.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/constraints/hangingnode.hh>
#include <dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>
#include <dune/pdelab/stationary/linearproblem.hh>
#include <dune/pdelab/adaptivity/adaptivity.hh>


template<typename Grid>
void doRefinement( Grid & grid, double radius )
{
  auto gv = grid.leafGridView();

  for (const auto& c : elements(gv))
    {
      double eps = 1e-5;
      auto center = c.geometry().center();
      if (std::abs(center[0])<radius+eps && std::abs(center[1])<radius+eps)
        grid.mark(1,c);
    }

  std::cout << "marked elements for refinement" << std::endl;

  grid.preAdapt();
  grid.adapt();
  grid.postAdapt();
}

//! Solve problem on leaf grid view and adapt grid
template<class Grid>
void refinement (Grid& grid, std::string filename_base, int maxsteps, int j)
{
  // some types
  typedef typename Grid::LeafGridView GV;
  typedef typename Grid::ctype Coord;
  typedef double Real;
  const int dim = GV::dimension;

  // refinement loop
  double radius = 1.0;
  for (int step=0; step<maxsteps; step++)
    {
      std::cout << "refinement " << step << " radius " << radius << " on " << filename_base << std::endl;

      // write vtk file
      Dune::VTKWriter<GV> vtkwriter(grid.leafGridView());
      std::stringstream fullname;
      fullname << filename_base << "_step" << step;
      vtkwriter.write(fullname.str(),Dune::VTK::appendedraw);

      if (step<maxsteps-1)
        {
          std::cout << "adapting grid" << std::endl;
          doRefinement(grid,radius);
        }

      if ( j==1 || (step%j==0) ) radius *= 0.5;
    }
}

template<class Factory>
void ldomain_tmesh (Factory& factory)
{
  /* 5 - 6 - 7
     |   |   |
     2 - 3 - 4
     |   |
     0 - 1
  */
  Dune::FieldVector< double, 2 > pos;
  pos[0] = -1;  pos[1] = -1; factory.insertVertex(pos);
  pos[0] =  0;  pos[1] = -1; factory.insertVertex(pos);
  pos[0] = -1;  pos[1] =  0; factory.insertVertex(pos);
  pos[0] =  0;  pos[1] =  0; factory.insertVertex(pos);
  pos[0] =  1;  pos[1] =  0; factory.insertVertex(pos);
  pos[0] = -1;  pos[1] =  1; factory.insertVertex(pos);
  pos[0] =  0;  pos[1] =  1; factory.insertVertex(pos);
  pos[0] =  1;  pos[1] =  1; factory.insertVertex(pos);

  const Dune::GeometryType type(Dune::GeometryTypes::simplex(2));
  std::vector< unsigned int > cornerIDs( 3 );
  cornerIDs[0] = 3; cornerIDs[1] = 0; cornerIDs[2] = 1;
  factory.insertElement( type, cornerIDs );
  cornerIDs[0] = 0; cornerIDs[1] = 3; cornerIDs[2] = 2;
  factory.insertElement( type, cornerIDs );
  cornerIDs[0] = 3; cornerIDs[1] = 5; cornerIDs[2] = 2;
  factory.insertElement( type, cornerIDs );
  cornerIDs[0] = 5; cornerIDs[1] = 3; cornerIDs[2] = 6;
  factory.insertElement( type, cornerIDs );
  cornerIDs[0] = 7; cornerIDs[1] = 3; cornerIDs[2] = 4;
  factory.insertElement( type, cornerIDs );
  cornerIDs[0] = 3; cornerIDs[1] = 7; cornerIDs[2] = 6;
  factory.insertElement( type, cornerIDs );
}

template<class Factory>
void ldomain_qmesh (Factory& factory)
{
  /* 5 - 6 - 7
     |   |   |
     2 - 3 - 4
     |   |
     0 - 1
  */
  Dune::FieldVector< double, 2 > pos;
  pos[0] = -1;  pos[1] = -1; factory.insertVertex(pos);
  pos[0] =  0;  pos[1] = -1; factory.insertVertex(pos);
  pos[0] = -1;  pos[1] =  0; factory.insertVertex(pos);
  pos[0] =  0;  pos[1] =  0; factory.insertVertex(pos);
  pos[0] =  1;  pos[1] =  0; factory.insertVertex(pos);
  pos[0] = -1;  pos[1] =  1; factory.insertVertex(pos);
  pos[0] =  0;  pos[1] =  1; factory.insertVertex(pos);
  pos[0] =  1;  pos[1] =  1; factory.insertVertex(pos);

  const Dune::GeometryType type( Dune::GeometryTypes::cube(2) );
  std::vector< unsigned int > cornerIDs( 4 );
  cornerIDs[0] = 0; cornerIDs[1] = 1; cornerIDs[2] = 2; cornerIDs[3] = 3;
  factory.insertElement( type, cornerIDs );
  cornerIDs[0] = 2; cornerIDs[1] = 3; cornerIDs[2] = 5; cornerIDs[3] = 6;
  factory.insertElement( type, cornerIDs );
  cornerIDs[0] = 3; cornerIDs[1] = 4; cornerIDs[2] = 6; cornerIDs[3] = 7;
  factory.insertElement( type, cornerIDs );
}

int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // start try/catch block to get error messages from dune
  try {

    // read command line arguments
    if (argc!=3)
      {
        std::cout << "usage: " << argv[0] << " <mode> <max steps>" << std::endl;
        std::cout << "mode=1 Alberta" << std::endl;
        std::cout << "mode=2 UG conforming" << std::endl;
        std::cout << "mode=3 ALUSimplex" << std::endl;
        std::cout << "mode=4 UG quads nonconforming" << std::endl;
        std::cout << "mode=5 UG quads conforming" << std::endl;
        return 0;
      }
    int mode; sscanf(argv[1],"%d",&mode);
    int maxsteps; sscanf(argv[2],"%d",&maxsteps);

#if HAVE_ALBERTA
    if (mode==1)
      {
        const int dim=2;
        typedef Dune::AlbertaGrid<dim,dim> GridType;
        Dune::GridFactory<GridType> factory;
        ldomain_tmesh(factory);
        GridType *grid=factory.createGrid();
        refinement(*grid,"refinement_alberta",maxsteps,2);
      }
#endif

#if HAVE_UG
    if (mode==2)
      {
        const int dim=2;
        typedef Dune::UGGrid<dim> GridType;
        GridType::setDefaultHeapSize(1500);
        Dune::GridFactory<GridType> factory;
        ldomain_tmesh(factory);
        GridType *grid=factory.createGrid();
        refinement(*grid,"refinement_ug_triangles",maxsteps,1);
      }
    if (mode==4)
      {
        const int dim=2;
        typedef Dune::UGGrid<dim> GridType;
        GridType::setDefaultHeapSize(1500);
        Dune::GridFactory<GridType> factory;
        ldomain_qmesh(factory);
        GridType *grid=factory.createGrid();
        grid->setRefinementType( GridType::LOCAL );
        grid->setClosureType( GridType::NONE );
        refinement(*grid,"refinement_ug_quads_hanging",maxsteps,1);
      }
    if (mode==5)
      {
        const int dim=2;
        typedef Dune::UGGrid<dim> GridType;
        GridType::setDefaultHeapSize(1500);
        Dune::GridFactory<GridType> factory;
        ldomain_qmesh(factory);
        GridType *grid=factory.createGrid();
        refinement(*grid,"refinement_ug_quads_conforming",maxsteps,1);
      }
#endif

#if HAVE_DUNE_ALUGRID
    if (mode==3)
      {
        const int dim=2;
        typedef Dune::ALUGrid<dim,dim,Dune::simplex,Dune::nonconforming> GridType;
        Dune::GridFactory<GridType> factory;
        ldomain_tmesh(factory);
        GridType *grid=factory.createGrid();
        refinement(*grid,"refinement_alusimplex",maxsteps,1);
      }
#endif
  }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
