set term postscript eps enhanced color "Helvetica,14"
set style data linespoints
set key right top

set output "ldomain_adaptive_alberta.eps"
set xlabel "N" font "Helvetica,14"
set ylabel "H1-seminorm of error" font "Helvetica,14"
set yrange [*:*]
set xrange [2e1:5e5]
set xtics 0,10 font "Helvetica,14"
set ytics 0,10 font "Helvetica,14"
set logscale x
set logscale y
plot "alberta_P1_uniform.dat" u 2:6 w lp lw 3 title "uniform", \
"alberta_P1_07.dat" u 2:6 w lp lw 3 title "P1", \
"alberta_P2_07.dat" u 2:6 w lp lw 3 title "P2", \
"alberta_P3_07.dat" u 2:6 w lp lw 3 title "P3", \
"alberta_P4_07.dat" u 2:6 w lp lw 3 title "P4"
! epstopdf ldomain_adaptive_alberta.eps
