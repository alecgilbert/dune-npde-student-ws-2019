#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <dune/common/parallel/mpihelper.hh> // include mpi helper class
#include <dune/common/parametertreeparser.hh>
#include <dune/grid/onedgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/finiteelementmap/pkfem.hh>
#include <dune/pdelab/constraints/common/constraints.hh>

//! A function demonstrating a finite element space on a grid view
template<class GV, int degree>
void lagrange1d_basis (const GV& gv)
{
  using Dune::PDELab::Backend::native;

  // coordinate and result type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;

  // make the finite element map
  typedef Dune::PDELab::PkLocalFiniteElementMap<GV,Coord,Real,degree> FEM; // basis on reference element
  FEM fem(gv);

  // make grid function space
  typedef Dune::PDELab::NoConstraints CON;
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem);

  // make a std::vector of degree of freedom vectors
  using U = Dune::PDELab::Backend::Vector<GFS,Real>;
  gfs.update();
  std::vector<U> u(gfs.globalSize(),U(gfs,0.0));


  // write a file for each global finite element function
  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
  std::vector<DGF *> udgf(gfs.globalSize());
  Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,Dune::refinementLevels(6));
  std::cout << "writing " << gfs.globalSize() << " basis functions" << std::endl;
  for (std::size_t i=0; i<u.size(); i++)
    {
      native(u[i])[i] = 1.0;
      std::stringstream componentName;
      componentName << "comp_" << i;
      udgf[i] = new DGF(gfs,u[i]);
      vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF> >(*(udgf[i]),componentName.str()));
    }
  vtkwriter.write("u",Dune::VTK::appendedraw);
  for (std::size_t i=0; i<udgf.size(); i++) delete udgf[i];

}

const double pi = Dune::MathematicalConstants<double>::pi();

// a function to initialize from
template<typename GV, typename RF>
class F : public Dune::PDELab::AnalyticGridFunctionBase<
  Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,
  F<GV,RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits,F<GV,RF> > B;

  F (const GV& gv) : B(gv) {}

  inline void evaluateGlobal (const typename Traits::DomainType& x,
                              typename Traits::RangeType& y) const
  {
    y[0] = sin(3.0*pi*x[0])*sin(3.0*pi*x[0]);
    return;
  }
};

//! A function demonstrating a finite element space on a grid view
template<class GV, int degree>
void lagrange_interpolation (const GV& gv)
{
  // coordinate and result type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;

  // make the finite element map
  typedef Dune::PDELab::PkLocalFiniteElementMap<GV,Coord,Real,degree> FEM; // basis on reference element
  FEM fem(gv);

  // make grid function space
  typedef Dune::PDELab::NoConstraints CON;
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem);

  // make a degree of freedom vector
  typedef typename GFS::template VectorContainer<Real>::Type U;
  U u(gfs,0.0);

  // interpolate from a given grid function
  F<GV,Real> f(gv); // make analytic function object
  Dune::PDELab::interpolate(f,gfs,u);

  // write vtk file
  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
  DGF udgf(gfs,u);
  Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,Dune::refinementLevels(6));
  vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<DGF>(udgf,"u"));
  vtkwriter.write("interpolatedf",Dune::VTK::appendedraw);
}

/** \brief boundary grid function selecting boundary conditions
 * 0 means Neumann, 1 means Dirichlet */
template<typename GV>
class BCType : public Dune::PDELab::BoundaryGridFunctionBase<
  Dune::PDELab::BoundaryGridFunctionTraits<GV,int,1,Dune::FieldVector<int,1> >,BCType<GV> >
{
  GV gv;
public:
  typedef Dune::PDELab::BoundaryGridFunctionTraits<GV,int,1,Dune::FieldVector<int,1> > Traits;

  //! construct from grid view
  BCType (const GV& gv_) : gv(gv_) {}

  //! return bc type at point on intersection
  template<typename I>
  inline void evaluate (I& i, const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const {
    Dune::FieldVector<typename GV::Grid::ctype,GV::dimension>
      x = i.geometry().global(xlocal);
    y = 0; // default is Dirichlet
    if (x[0]<1e-6)
      y = 1; // Neumann
    return;
  }

  //! get a reference to the grid view
  inline const GV& getGridView () {return gv;}
};

int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // start try/catch block to get error messages from dune
  try {
    using namespace Dune;
    // read parameters from file
    Dune::ParameterTree configuration;
    Dune::ParameterTreeParser parser;
    parser.readINITree("pk1dglobalbasis.ini", configuration);

    // The continuous floating point type of the grid.
    typedef Dune::OneDGrid::ctype ctype;

    // Set number of vertices
    const unsigned int N = configuration.get<int>("grid.cells");
    const ctype L = 1.0;

    // Define position intervals in reference domain [0,1].
    typedef std::vector<ctype> Intervals;
    Intervals intervals(N+1);
    for(unsigned int i=0; i<N+1; ++i)
      intervals[i] = ctype(i) / ctype(N);

    // Construct grid
    Dune::OneDGrid grid(intervals);

    // call generic function
    typedef Dune::OneDGrid::LeafGridView GV;
    GV gv=grid.leafGridView();
    lagrange1d_basis<GV,3>(gv);
  }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
