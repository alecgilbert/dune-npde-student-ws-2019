#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <dune/common/parallel/mpihelper.hh> // include mpi helper class
#include <dune/common/parametertreeparser.hh>
#include <dune/grid/onedgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/yaspgrid.hh>
#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/geometry/type.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/io.hh>
#include <dune/istl/paamg/amg.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/common/functionutilities.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/stationary/linearproblem.hh>
#include <dune/pdelab/finiteelementmap/pkfem.hh>
#include <dune/pdelab/constraints/conforming.hh>

//! solve the diffusion problem on a given GridView using the given polynomial degree
template<class GV, int degree>
void driver (const GV& gv, std::string filename_base)
{
  // extract useful types from Grid
  typedef typename GV::Grid::ctype Coord;  // type the grid uses for representing coordinates
  typedef double Real;                     // type to be used for degrees of freedom, parameters, etc.

  // make finite element map
  typedef Dune::PDELab::PkLocalFiniteElementMap<GV,Coord,Real,degree> FEM; // basis on reference element
  FEM fem(gv);

  // make grid function space
  typedef Dune::PDELab::NoConstraints CON;                  // no constraints on the space
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;           // how to represent degrees of freedom
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS; // collect local to global map
  GFS gfs(gv,fem);

  // write a file for each global finite element function
  using U = Dune::PDELab::Backend::Vector<GFS,Real>;
  gfs.update();
  std::vector<U> u(gfs.globalSize(),U(gfs,0.0));
  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
  std::vector<DGF *> udgf(gfs.globalSize());
  Dune::VTKWriter<GV> vtkwriter0(gv);
  vtkwriter0.write("mesh",Dune::VTK::appendedraw);
  Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,Dune::refinementLevels(6));
  std::cout << "writing " << gfs.globalSize() << " basis functions" << std::endl;
  for (std::size_t i=0; i<u.size(); i++)
    {
      u[i].block(i) = 1.0;
      std::stringstream componentName;
      componentName << "comp_" << i;
      udgf[i] = new DGF(gfs,u[i]);
      vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF> >(*(udgf[i]),componentName.str()));
    }
  vtkwriter.write(filename_base,Dune::VTK::appendedraw);
  for (std::size_t i=0; i<udgf.size(); i++) delete udgf[i];
}


int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // start try/catch block to get error messages from dune
  try
    {
      // read parameters from file
      Dune::ParameterTree configuration;
      Dune::ParameterTreeParser parser;
      parser.readINITree("pk2dglobalbasis.ini", configuration);
      std::string grid_file = configuration.get<std::string>("grid.filename");
      std::cout << "processing grid " << grid_file << std::endl;
      const int max_level = configuration.get<int>("grid.max_level");
      std::cout << "refining grid to max_level " << max_level << std::endl;
      const int degree = configuration.get<int>("method.degree");
      std::cout << "requested polynomial degree is " << degree << std::endl;
      if (degree<1 || degree>5) {
        std::cout << "dimension should be between 1 and 5!" << std::endl;
        return 1;
      }

#if HAVE_UG
      // make uggrid
      typedef Dune::UGGrid<2> GridType;
      GridType::setDefaultHeapSize(1000);
      GridType grid;
      Dune::GridFactory<GridType> factory(&grid);
      Dune::GmshReader<GridType>::read(factory,grid_file,true,true);
      factory.createGrid();

      // refine grid uniformly to max_level
      for (int i=0; i<max_level; i++)
        grid.globalRefine(1);

      // compile filename
      std::stringstream filename;
      filename << "pk2dbasis_" << grid_file << "_l" << max_level << "_k" << degree;

      // solve problem with corresponding degree
      typedef GridType::LeafGridView GV;
      GV gv=grid.leafGridView();
      if (degree==1) driver<GV,1>(gv,filename.str());
      if (degree==2) driver<GV,2>(gv,filename.str());
      if (degree==3) driver<GV,3>(gv,filename.str());
      if (degree==4) driver<GV,4>(gv,filename.str());
      if (degree==5) driver<GV,5>(gv,filename.str());
#endif
    }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
