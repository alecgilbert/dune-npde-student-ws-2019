#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <dune/common/parallel/mpihelper.hh> // include mpi helper class
#include <dune/grid/onedgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif
#include<dune/grid/io/file/gmshreader.hh>
#include<dune/istl/bvector.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/io.hh>
#include<dune/istl/paamg/amg.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/common/functionutilities.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/finiteelementmap/p0fem.hh>
#include<dune/pdelab/finiteelementmap/pkfem.hh>
#include<dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include<dune/pdelab/localoperator/convectiondiffusionfem.hh>
#include<dune/pdelab/stationary/linearproblem.hh>
#include<dune/pdelab/adaptivity/adaptivity.hh>

template<typename GV, typename RF>
class TwoBodyProblem
{
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

public:
  typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

  TwoBodyProblem ()
    : pos1(0.0), pos2(0.0)
  {
    r1 = 6.0E6;
    r2 = 3.0E6;
    pos1[0] = -7.0E6;
    pos2[0] = 1E7;
    V1 = 4.0/3.0*r1*r1*r1*M_PI;
    V2 = 4.0/3.0*r2*r2*r2*M_PI;
    rho1 = 1000.0;
    rho2 = 2500.0;
    m1 = V1*rho1;
    m2 = V2*rho2;
    G = 6.67E-11;
  }

  //! tensor diffusion constant per cell? return false if you want more than one evaluation of A per cell.
  static constexpr bool permeabilityIsConstantPerCell()
  {
    return true;
  }

  //! tensor diffusion coefficient
  typename Traits::PermTensorType
  A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::PermTensorType I;
    for (std::size_t i=0; i<Traits::dimDomain; i++)
      for (std::size_t j=0; j<Traits::dimDomain; j++)
        I[i][j] = (i==j) ? 1.0 : 0.0;
    return I;
  }

  //! velocity field
  typename Traits::RangeType
  b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::RangeType v(0.0);
    return v;
  }

  //! sink term
  typename Traits::RangeFieldType
  c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! source term
  typename Traits::RangeFieldType
  f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::DomainType xglobal = e.geometry().global(x);
    typename Traits::DomainType d1(pos1);
    d1 -= xglobal;
    if (d1.two_norm()<=r1) return -4.0*M_PI*G*rho1;
    typename Traits::DomainType d2(pos2);
    d2 -= xglobal;
    if (d2.two_norm()<=r2) return -4.0*M_PI*G*rho2;
    return 0.0;
  }

  //! boundary condition type function
  BCType
  bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    //typename Traits::DomainType xglobal = is.geometry().global(x);
    return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
  }

  //! Dirichlet boundary condition value
  typename Traits::RangeFieldType
  g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::DomainType xglobal = e.geometry().global(x);
    typename Traits::RangeFieldType phi(0.0);
    typename Traits::DomainType d1(pos1);
    d1 -= xglobal;
    if (d1.two_norm()<=r1)
      phi += 0.5*m1*G*( d1.two_norm2()/(r1*r1) - 3.0 )/r1;
    else
      phi += - m1*G/d1.two_norm();
    typename Traits::DomainType d2(pos2);
    d2 -= xglobal;
    if (d2.two_norm()<=r2)
      phi += 0.5*m2*G*( d2.two_norm2()/(r2*r2) - 3.0 )/r2;
    else
      phi += - m2*G/d2.two_norm();
    return phi;
  }

  //! Neumann boundary condition
  typename Traits::RangeFieldType
  j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! Neumann boundary condition
  typename Traits::RangeFieldType
  o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }
private:
  typename Traits::RangeFieldType rho1, rho2, m1, m2, r1, r2, V1, V2, G;
  typename Traits::DomainType pos1, pos2;
};

namespace Dune {
  namespace PDELab {
    //! \brief convert a single component function space with a grid function
    //! representing the gradient
    /**
     * The function values should be single-component vectors.  The Gradien
     * will be a dimDomain-component function.
     *
     * \tparam T Type of GridFunctionSpace.  The LocalBasis must provide the
     *           evaluateJacobian() method.
     * \tparam X Type of coefficients vector
     */
    template<typename T, typename X>
    class DiscreteGridFunctionNegativeGradient
      : public GridFunctionInterface<
      GridFunctionTraits<
        typename T::Traits::GridViewType,
        typename T::Traits::FiniteElementType::Traits::LocalBasisType
        ::Traits::RangeFieldType,
        T::Traits::FiniteElementType::Traits::LocalBasisType::Traits
        ::dimDomain,
        FieldVector<
          typename T::Traits::FiniteElementType::Traits
          ::LocalBasisType::Traits::RangeFieldType,
          T::Traits::FiniteElementType::Traits::LocalBasisType::Traits
          ::dimDomain> >,
      DiscreteGridFunctionNegativeGradient<T,X> >
    {
      typedef T GFS;
      typedef typename GFS::Traits::FiniteElementType::Traits::
      LocalBasisType::Traits LBTraits;

    public:
      typedef GridFunctionTraits<
      typename GFS::Traits::GridViewType,
      typename LBTraits::RangeFieldType,
      LBTraits::dimDomain,
      FieldVector<
        typename LBTraits::RangeFieldType,
        LBTraits::dimDomain> > Traits;

    private:
      typedef GridFunctionInterface<
      Traits,
      DiscreteGridFunctionNegativeGradient<T,X> > BaseT;

    public:
      /** \brief Construct a DiscreteGridFunctionNegativeGradient
       *
       * \param gfs The GridFunctionsSpace
       * \param x_  The coefficients vector
       */
      DiscreteGridFunctionNegativeGradient (const GFS& gfs, const X& x_)
        : pgfs(stackobject_to_shared_ptr(gfs))
        , lfs(gfs)
        , lfs_cache(lfs)
        , x_view(x_)
        , xl(lfs.size())
      { }

      // Evaluate
      inline void evaluate (const typename Traits::ElementType& e,
                            const typename Traits::DomainType& x,
                            typename Traits::RangeType& y) const
      {
        // get and bind local functions space
        lfs.bind(e);
        lfs_cache.update();
        x_view.bind(lfs_cache);

        // get local coefficients
        xl.resize(lfs.size());
        x_view.read(xl);
        x_view.unbind();

        // get Jacobian of geometry
        const typename Traits::ElementType::Geometry::JacobianInverseTransposed
          JgeoIT = e.geometry().jacobianInverseTransposed(x);

        // get local Jacobians/gradients of the shape functions
        std::vector<typename LBTraits::JacobianType> J(lfs.size());
        lfs.finiteElement().localBasis().evaluateJacobian(x,J);

        typename Traits::RangeType gradphi;
        y = 0;
        for(unsigned int i = 0; i < lfs.size(); ++i) {
          // compute global gradient of shape function i
          gradphi = 0;
          JgeoIT.umv(J[i][0], gradphi);

          // sum up global gradients, weighting them with the appropriate coeff
          y.axpy(-xl[i], gradphi);
        }

      }

      //! get a reference to the GridView
      inline const typename Traits::GridViewType& getGridView () const
      {
        return pgfs->gridView();
      }

    private:
      typedef LocalFunctionSpace<GFS> LFS;
      typedef LFSIndexCache<LFS> LFSCache;
      typedef typename X::template ConstLocalView<LFSCache> XView;

      shared_ptr<GFS const> pgfs;
      mutable LFS lfs;
      mutable LFSCache lfs_cache;
      mutable XView x_view;
      mutable std::vector<typename Traits::RangeFieldType> xl;
    };
  }
}

//! Solve problem on leaf grid view and adapt grid
template<class Grid>
void driver (Grid& grid, std::string filename_base, double TOL, int maxsteps, double fraction)
{
  // some types
  typedef typename Grid::LeafGridView GV;
  typedef typename Grid::ctype Coord;
  typedef double Real;
  const int dim = GV::dimension;

  // make finite element map
  // note: adaptivity currently relies on finite element map not depending on grid view
  typedef Dune::PDELab::PkLocalFiniteElementMap<GV,Coord,Real,1> FEM;
  FEM fem(grid.leafGridView());

  // make grid function space
  // note: adaptivity relies on leaf grid view object being updated by the grid on adaptation
  typedef Dune::PDELab::ConformingDirichletConstraints CON;
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(grid.leafGridView(),fem);

  // make a degree of freedom vector;
  using U = Dune::PDELab::Backend::Vector<GFS,Real>;
  U u(gfs,0.0);

  // refinement loop
  for (int step=0; step<maxsteps; step++)
    {
      // get current leaf view
      GV gv=grid.leafGridView();

      // make problem
      typedef TwoBodyProblem<GV,Real> Problem;
      Problem problem;

      // initialize DOFS from Dirichlet extension
      // note: currently we start from scratch on every grid
      typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<Problem> G;
      G g(gv,problem);
      Dune::PDELab::interpolate(g,gfs,u);

      // make constraints container and initialize it
      typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
      CC cc;
      Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<Problem> bctype(gv,problem);
      Dune::PDELab::constraints(bctype,gfs,cc);
      Dune::PDELab::set_nonconstrained_dofs(cc,0.0,u);
      std::cout << "constrained dofs=" << cc.size() << " of " << gfs.globalSize() << std::endl;

      // make local operator
      typedef Dune::PDELab::ConvectionDiffusionFEM<Problem,FEM> LOP;
      LOP lop(problem);
      typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
      MBE mbe(27); // number of nonzeroes per row can be cross-checked by patternStatistics().
      typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;
      GO go(gfs,cc,gfs,cc,lop,mbe);

      // make linear solver and solve problem
      typedef Dune::PDELab::ISTLBackend_SEQ_CG_AMG_SSOR<GO> LS;
      LS ls (5000, 2);
      // typedef Dune::PDELab::ISTLBackend_SEQ_CG_ILU0 LS;
      // LS ls(10000,2);
      typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
      SLP slp(go,ls,u,1e-10);
      slp.apply();

      // compute estimated error
      typedef Dune::PDELab::P0LocalFiniteElementMap<Coord,Real,dim> P0FEM;
      P0FEM p0fem(Dune::GeometryType(Dune::GeometryTypes::simplex(dim)));
      typedef Dune::PDELab::NoConstraints NOCON;
      typedef Dune::PDELab::GridFunctionSpace<GV,P0FEM,NOCON,VBE> P0GFS;
      P0GFS p0gfs(gv,p0fem);
      typedef Dune::PDELab::ConvectionDiffusionFEMResidualEstimator<Problem> ESTLOP;
      ESTLOP estlop(problem);
      typedef Dune::PDELab::EmptyTransformation NoTrafo;
      NoTrafo notrafo;

      typedef Dune::PDELab::GridOperator<GFS,P0GFS,ESTLOP,MBE,Real,Real,Real,CC,NoTrafo> ESTGO;
      ESTGO estgo(gfs,cc,p0gfs,notrafo,estlop,mbe);

      using U0 = Dune::PDELab::Backend::Vector<P0GFS,Real>;
      U0 eta(p0gfs,0.0);
      estgo.residual(u,eta);
      using Dune::PDELab::Backend::native;
      for (unsigned int i=0; i<eta.N(); i++)  native(eta)[i] = sqrt(native(eta)[i]);
      Real estimated_error = eta.two_norm();

      // write vtk file
      typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
      DGF udgf(gfs,u);
      typedef Dune::PDELab::DiscreteGridFunction<P0GFS,U0> DGF0;
      DGF0 udgf0(p0gfs,eta);
      typedef Dune::PDELab::DiscreteGridFunctionNegativeGradient<GFS,U> DGFGrad;
      DGFGrad udgfgrad(gfs,u);
      //Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,Dune::refinementLevels(3));
      Dune::VTKWriter<GV> vtkwriter(gv);
      std::stringstream fullname;
      fullname << filename_base << "_step" << step;
      vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF> >(udgf,"u_h"));
      vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGFGrad> >(udgfgrad,"grad u_h"));
      vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<G> >(g,"u"));
      vtkwriter.addCellData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF0> >(udgf0,"estimated error"));
      vtkwriter.write(fullname.str(),Dune::VTK::appendedraw);

      // error control
      if (estimated_error <= TOL) break;

      // Use eta to refine the grid following two different strategies based
      // (1) element fraction
      // (2) error fraction

      double alpha(0.4);       // refinement fraction
      double eta_alpha(0);     // refinement threshold
      double beta(0.0);        // coarsening fraction
      double eta_beta(0);      // coarsening threshold
      int verbose = 2;

      // <<<10>>> Adapt the grid locally...
      // with strategy 1:
      element_fraction( eta, alpha, beta, eta_alpha, eta_beta, verbose );
      // or, alternatively, with strategy 2:
      //error_fraction( eta, alpha, beta, eta_alpha, eta_beta, verbose );

      // adapt grid
      if (step<maxsteps-1)
        {
          mark_grid( grid, eta, eta_alpha, 0.0 );
          adapt_grid( grid, gfs, u , 2);

          Dune::PDELab::constraints(bctype,gfs,cc);
          Dune::PDELab::interpolate(g,gfs,u);
        }
    }
}

int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // start try/catch block to get error messages from dune
  try {

    // UGGrid/Pk3d version
#if HAVE_UG
    if (true)
      {
        // read command line arguments
        if (argc!=6)
          {
            std::cout << "usage: " << argv[0] << " <mesh file> <start level> <TOL> <max steps> <fraction>" << std::endl;
            std::cout << "mesh file = e.g. twobody1.msh" << std::endl;
            return 0;
          }
        std::string grid_file(argv[1]);
        int start_level; sscanf(argv[2],"%d",&start_level);
        double TOL; sscanf(argv[3],"%lg",&TOL);
        int maxsteps; sscanf(argv[4],"%d",&maxsteps);
        double fraction; sscanf(argv[5],"%lg",&fraction);

        // make uggrid
        const int dim=3;

        // make UG grid
        typedef Dune::UGGrid<dim> GridType;
        GridType grid;
        Dune::GridFactory<GridType> factory(&grid);
        Dune::GmshReader<GridType>::read(factory,grid_file,true,false);
        factory.createGrid();
        for (int i=0; i<start_level; i++) grid.globalRefine(1);
        driver(grid,grid_file,TOL,maxsteps,fraction);
      }
#endif
  }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
