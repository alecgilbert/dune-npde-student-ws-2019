// Gmsh project created on Fri Mar 18 18:18:41 2011
Point(1) = {0, 0, 0};
Point(2) = {1, 0, 0};
Point(3) = {0, 1, 0};
Point(4) = {0, 0, 1};
Circle(1) = {2, 1, 4};
Circle(2) = {2, 1, 3};
Circle(3) = {3, 1, 4};
Line Loop(4) = {2, 3, -1};
Ruled Surface(5) = {4};
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{5}; }
}
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{6}; }
}
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{9}; }
}
Point(11) = {0, 0, -1};
Circle(14) = {10, 1, 11};
Circle(15) = {2, 1, 11};
Line Loop(16) = {14, -15, -13};
Ruled Surface(17) = {16};
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{17}; }
}
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{18}; }
}
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{21}; }
}
