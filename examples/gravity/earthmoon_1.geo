// Gmsh project created on Fri Mar 18 18:18:41 2011
Point(1) = {0, 0, 0};
Point(2) = {1, 0, 0};
Point(3) = {0, 1, 0};
Point(4) = {0, 0, 1};
Circle(1) = {2, 1, 4};
Circle(2) = {2, 1, 3};
Circle(3) = {3, 1, 4};
Line Loop(4) = {2, 3, -1};
Ruled Surface(5) = {4};
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{5}; }
}
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{6}; }
}
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{9}; }
}
Point(11) = {0, 0, -1};
Circle(14) = {10, 1, 11};
Circle(15) = {2, 1, 11};
Line Loop(16) = {14, -15, -13};
Ruled Surface(17) = {16};
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{17}; }
}
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{18}; }
}
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{21}; }
}
Dilate {{0, 0, 0}, 6000000} {
  Duplicata { Surface{18, 21, 5, 6, 17, 24, 12, 9}; }
}
Dilate {{0, 0, 0}, 3000000} {
  Surface{18, 12, 5, 6, 17, 24, 21, 9};
}
Translate {-7000000, 0, 0} {
  Duplicata { Surface{33, 25, 29, 45, 53, 49, 41, 37}; }
}
Delete {
  Line{44, 42, 48, 26, 36, 28};
}
Delete {
  Surface{41, 49, 25};
}
Delete {
  Surface{33};
}
Delete {
  Surface{37};
}
Delete {
  Surface{53};
}
Delete {
  Surface{29};
}
Delete {
  Surface{45};
}
Delete {
  Line{28};
}
Delete {
  Line{35};
}
Delete {
  Line{36};
}
Delete {
  Line{26};
}
Delete {
  Line{44};
}
Delete {
  Line{42};
}
Delete {
  Line{48};
}
Delete {
  Line{52};
}
Delete {
  Line{31};
}
Delete {
  Line{27};
}
Delete {
  Line{39};
}
Delete {
  Line{32};
}
Delete {
  Point{14};
}
Delete {
  Point{72};
}
Delete {
  Point{12};
}
Delete {
  Point{49};
}
Delete {
  Point{19};
}
Delete {
  Point{34};
}
Delete {
  Point{74};
}
Delete {
  Point{74};
}
Translate {10000000, 0, 0} {
  Duplicata { Surface{12, 9, 24, 17, 18, 5, 6, 21}; }
}
Delete {
  Surface{5};
}
Delete {
  Surface{18};
}
Delete {
  Surface{12};
}
Delete {
  Surface{6};
}
Delete {
  Surface{17};
}
Delete {
  Surface{24};
}
Delete {
  Surface{9};
}
Delete {
  Surface{21};
}
Delete {
  Line{1};
}
Delete {
  Line{3};
}
Delete {
  Line{2};
}
Delete {
  Line{13};
}
Delete {
  Line{15};
}
Delete {
  Line{20};
}
Delete {
  Line{7};
}
Delete {
  Line{11};
}
Delete {
  Line{14};
}
Delete {
  Line{23};
}
Delete {
  Line{8};
}
Delete {
  Line{10};
}
Delete {
  Point{7};
}
Delete {
  Point{4};
}
Delete {
  Point{3};
}
Delete {
  Point{2};
}
Delete {
  Point{11};
}
Delete {
  Point{10};
}
Point(194) = {300000000, 0, 0};
Point(195) = {0, 300000000, 0};
Point(196) = {0, 0, 300000000};
Circle(112) = {195, 1, 196};
Circle(113) = {195, 1, 194};
Circle(114) = {194, 1, 196};
Line Loop(115) = {113, 114, -112};
Ruled Surface(116) = {115};
Point(197) = {0, 0, -300000000};
Circle(117) = {197, 1, 195};
Circle(118) = {197, 1, 196};
Delete {
  Line{118};
}
Circle(118) = {197, 1, 194};
Line Loop(119) = {117, 113, -118};
Ruled Surface(120) = {119};
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{116}; }
}
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{121}; }
}
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{125}; }
}
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{120}; }
}
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{131}; }
}
Rotate {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Duplicata { Surface{133}; }
}
Surface Loop(136) = {121, 131, 133, 135, 120, 116, 129, 125};
Surface Loop(137) = {70, 66, 62, 58, 78, 74, 54, 82};
Surface Loop(138) = {91, 111, 99, 95, 83, 103, 107, 87};
Volume(139) = {136, 137, 138};
