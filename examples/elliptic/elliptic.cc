#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <dune/common/parallel/mpihelper.hh> // include mpi helper class
#include <dune/common/parametertreeparser.hh>
#include <dune/grid/onedgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/grid/yaspgrid.hh>
#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/geometry/type.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/io.hh>
#include <dune/istl/paamg/amg.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/common/functionutilities.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/stationary/linearproblem.hh>
#include <dune/pdelab/finiteelementmap/pkfem.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>

#include "problem.hh"

//! solve the diffusion problem on a given GridView using the given polynomial degree
template<class GV, int degree>
void driver (const GV& gv, std::string filename_base)
{
  // extract useful types from Grid
  typedef typename GV::Grid::ctype Coord;  // type the grid uses for representing coordinates
  typedef double Real;                     // type to be used for degrees of freedom, parameters, etc.

  // make finite element map
  typedef Dune::PDELab::PkLocalFiniteElementMap<GV,Coord,Real,degree> FEM; // basis on reference element
  FEM fem(gv);

  // make grid function space
  typedef Dune::PDELab::ConformingDirichletConstraints CON; // how to assemble constraints
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;           // how to represent degrees of freedom
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS; // collect local to global map
  GFS gfs(gv,fem);

  // make problem parameters
  typedef GenericEllipticProblem<GV,double> Problem; // parameters for finite element method
  Problem problem;

  // make a std::vector of degree of freedom vectors and initialize it with Dirichlet extension
  using U = Dune::PDELab::Backend::Vector<GFS,Real>;                            // type for vectors of degrees of freedom
  U u(gfs,0.0);                                                                 // the solution vector z representing u_h
  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<Problem> G;// the extension function type
  G g(gv,problem);                                                              // the extension function u_g
  Dune::PDELab::interpolate(g,gfs,u);                                           // set coefficients such that u_h interpolates u_g

  // make constraints container and initialize it
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;                   // container type for storing indices of Dirichlet dofs
  CC cc;                                                                                // an empty container
  Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<Problem> bctype(gv,problem);// the boundary condition function
  Dune::PDELab::constraints(bctype,gfs,cc);                                             // detect indices of constrained dofs
  Dune::PDELab::set_nonconstrained_dofs(cc,0.0,u);                                      // set non-constrained dofs to zero
  std::cout << "constrained dofs=" << cc.size() << " of " << gfs.globalSize() << std::endl;

  // make local operator
  typedef Dune::PDELab::ConvectionDiffusionFEM<Problem,FEM> LOP;              // local operator type for the conforming finite element method
  LOP lop(problem);                                                           // the local operator
  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;                       // Backend for storing sparse matrices
  MBE mbe(5);
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;// the assembler type
  GO go(gfs,cc,gfs,cc,lop,mbe);                                                   // the assembler

  // make linear solver and solve problem
  typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_AMG_SSOR<GO> LS;                 // linear solver type from solver backend
  LS ls(500);                                                                 // the solver
  typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;           // type for linear problem solver (assembles A, b, solves)
  SLP slp(go,ls,u,1e-12);                                                     // the solver object
  slp.apply();                                                                // and now action !

  // write vtk file
  int subsampling = degree-1; if (degree>1) subsampling++;                           // determine degree of subsampling for higher order polynomials
  Dune::SubsamplingVTKWriter<GV> vtkwriter(gv, Dune::refinementLevels(subsampling)); // a VTK writer object
  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;                             // type making a function from dof vector (FE isomorphism)
  DGF udgf(gfs,u);                                                                   // the function object
  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF> >(udgf,"u_h"));// register discrete solution u_h for graphics output
  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<G> >(g,"u_g"));     // register extension function u_g for graphics output
  vtkwriter.write(filename_base,Dune::VTK::appendedraw);                             // now write Paraview/VTK file
}


int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // start try/catch block to get error messages from dune
  try
    {
      // read parameters from file
      Dune::ParameterTree configuration;
      Dune::ParameterTreeParser parser;
      parser.readINITree("elliptic.ini", configuration);
      std::string grid_file = configuration.get<std::string>("grid.filename");
      std::cout << "processing grid " << grid_file << std::endl;
      const int dimension = configuration.get<int>("grid.dimension");
      if (dimension<2 || dimension>3) {
        std::cout << "dimension should be 2 or 3!" << std::endl;
        return 1;
      }
      std::cout << "in " << dimension << " dimensions" << std::endl;
      const int max_level = configuration.get<int>("grid.max_level");
      std::cout << "refining grid to max_level " << max_level << std::endl;
      const int degree = configuration.get<int>("method.degree");
      if (degree<1 || degree>5) {
        std::cout << "dimension should be between 1 and 5!" << std::endl;
        return 1;
      }
      std::cout << "requested polynomial degree is " << degree << std::endl;

#if HAVE_UG
      if (dimension==2)
        {
          // make uggrid
          typedef Dune::UGGrid<2> GridType;
          GridType::setDefaultHeapSize(2000);
          GridType grid;

          // read gmsh file
          Dune::GridFactory<GridType> factory(&grid);
          Dune::GmshReader<GridType>::read(factory,grid_file,true,true);
          factory.createGrid();

          // refine grid uniformly to max_level
          for (int i=0; i<max_level; i++)
            grid.globalRefine(1);

          // compile filename
          std::stringstream filename;
          filename << "elliptic_" << grid_file << "_l" << max_level << "_k" << degree;

          // solve problem with corresponding degree
          typedef GridType::LeafGridView GV;
          GV gv=grid.leafGridView();
          if (degree==1) driver<GV,1>(gv,filename.str());
          if (degree==2) driver<GV,2>(gv,filename.str());
          if (degree==3) driver<GV,3>(gv,filename.str());
          if (degree==4) driver<GV,4>(gv,filename.str());
          if (degree==5) driver<GV,5>(gv,filename.str());
        }
      if (dimension==3)
        {
          // make uggrid
          typedef Dune::UGGrid<3> GridType;
          GridType::setDefaultHeapSize(2000);
          GridType grid;

          // read gmsh file
          Dune::GridFactory<GridType> factory(&grid);
          Dune::GmshReader<GridType>::read(factory,grid_file,true,true);
          factory.createGrid();

          // refine grid uniformly to max_level
          for (int i=0; i<max_level; i++)
            grid.globalRefine(1);

          // compile filename
          std::stringstream filename;
          filename << "elliptic_" << grid_file << "_l" << max_level << "_k" << degree;

          // solve problem with corresponding degree
          typedef GridType::LeafGridView GV;
          GV gv=grid.leafGridView();
          if (degree==1) driver<GV,1>(gv,filename.str());
          if (degree==2) driver<GV,2>(gv,filename.str());
          if (degree==3) driver<GV,3>(gv,filename.str());
          if (degree==4) driver<GV,4>(gv,filename.str());
          if (degree==5) driver<GV,5>(gv,filename.str());
        }
#endif
    }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
