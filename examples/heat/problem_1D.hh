//***********************************************************************
//***********************************************************************
// 1D diffusion problem with initial condition and zero rhs
//***********************************************************************
//***********************************************************************

template<typename GV, typename RF>
class GenericProblem1D
{
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

public:
  typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

  GenericProblem1D ()
    : time(0.0), len(1000.0), diffCoeff(2.5e4*M_PI*M_PI/1000.0/1000.0), kFourier(10), amp(kFourier, 0.0)
  {
    for (int i=1; i<kFourier; ++i)
      amp[i] = 50.0 / i;
  }

  //! tensor diffusion coefficient
  typename Traits::PermTensorType
  A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::PermTensorType I;
    for (std::size_t i=0; i<Traits::dimDomain; i++)
      for (std::size_t j=0; j<Traits::dimDomain; j++)
        I[i][j] = (i==j) ? (len * len * diffCoeff / M_PI / M_PI) : 0.0;
    return I;
  }

  //! velocity field
  typename Traits::RangeType
  b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::RangeType v(0.0);
    return v;
  }

  //! sink term
  typename Traits::RangeFieldType
  c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! source term
  typename Traits::RangeFieldType
  f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! boundary condition type function
  BCType
  bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    typename Traits::DomainType xglobal = is.geometry().global(x);
    return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
  }

  //! Dirichlet boundary condition value
  typename Traits::RangeFieldType
  g (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
  {
    typename Traits::DomainType x = e.geometry().global(xlocal);

    typename Traits::RangeFieldType y(0.0);
    for (int k=1; k<kFourier; ++k)
      {
        y += amp[k] * std::sin(k * x[0] * M_PI / len) * std::exp(-diffCoeff * k * k * time);
      }

    //printf("%6.3e  %8.4e   %12.8e\n", time, (double) x, (double) y);

    return y;
  }

  //! Neumann boundary condition
  typename Traits::RangeFieldType
  j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! Neumann boundary condition
  typename Traits::RangeFieldType
  o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! set time for subsequent evaluation
  void setTime (RF t)
  {
    time = t;
    //std::cout << "problem1D setting time to: " << time << std::endl;
  }

  //! get diffusion coefficient
  RF getDiffusion()
  {
    // coefficient was scaled in constructor
    return (diffCoeff * len * len / M_PI / M_PI);
  }

private:
  RF  time;
  RF  diffCoeff;
  RF  len;
  int kFourier;
  std::vector<RF> amp;
};

//***********************************************************************
//***********************************************************************
// a grid function giving the gradient of the exact solution
// needed for computing H^1 errors
//***********************************************************************
//***********************************************************************

//! exact gradient of solution
template<typename GV, typename RF>
class ExactGradient
  : public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits
                                          <GV,RF,
                                           GV::dimension,Dune::FieldVector<RF,GV::dimension> >,
                                          ExactGradient<GV,RF> >
{
  GV gv;

public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,
                                           GV::dimension,Dune::FieldVector<RF,GV::dimension> > Traits;
  typedef Dune::PDELab::GridFunctionBase<Traits,ExactGradient<GV,RF> > BaseT;

  ExactGradient (const GV& gv_)
    : gv(gv_), time(0.0), len(1000.0), diffCoeff(2.5e4*M_PI*M_PI/1000.0/1000.0), kFourier(10), amp(kFourier, 0.0)
  {
    for (int i=1; i<kFourier; ++i)
      amp[i] = 50.0 / i;
  }

  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    typename Traits::DomainType x = e.geometry().global(xlocal);

    y = 0.0;
    for (int k=1; k<kFourier; ++k)
      {
        // Ableitung:
        y += amp[k] * std::cos(k * M_PI * x[0] / len) * (k * M_PI / len) * std::exp(-k * k * 1.0  * diffCoeff * time);

        // Loesung:
        // y += amp[k] * std::sin(k * x[0] * M_PI / len) * std::exp(-diffCoeff * k * k * time);

        // y += amp[k] * std::exp(-(k * k) * diffCoeff * time) * (k * M_PI / len) * std::cos(k * M_PI * x[0] / len);

        //printf(" k: %d  kdone: %4.2e  exp: %4.2e", k, -k*k*1.0, std::exp(-k * k * 1.0));
      }
  }

  inline const typename Traits::GridViewType& getGridView () const
  {
    return gv;
  }

  //! set time for subsequent evaluation
  void setTime (RF t)
  {
    time = t;
  }

private:

  RF  time;
  RF  diffCoeff;
  RF  len;
  int kFourier;
  std::vector<RF> amp;
};
