/** \file output.hh
 *
 *  Created on: May 11, 2011
 *  Author:     jpods
 */

#ifndef NDPDELAB_OUTPUT_HH
#define NDPDELAB_OUTPUT_HH

#include <iostream>
#include <iomanip>
#include <string>
#include <valarray>

class Output
{
public:

  static void gnuplotInitialize( const std::string filename )
  {
    std::ofstream file;
    file.open(filename.c_str() );
    file.close();
  }

  // add xy data to file
  template<typename T>
  static void gnuplotAppend( const std::string filename, const T& x, const T& y )
  {
    std::ofstream file;
    file.open(filename.c_str(), std::ofstream::app);
    file << std::scientific << x << " " << y << std::endl;
    file.close();
  }

  // add xyz data to file
  template<typename T>
  static void gnuplotDoubleAppend( const std::string filename, const T& x, const T& y, const T& z )
  {
    std::ofstream file;
    file.open(filename.c_str(), std::ofstream::app);
    file << std::scientific << x << " " << y << " " << z << std::endl;
    file.close();
  }

  // write two valarrays to file, deleting all previous entries
  template<typename T>
  static void gnuplotArray(const std::string filename, const std::valarray<T>& x, const std::valarray<T>& y)
  {
    std::ofstream file;
    file.open(filename.c_str());
    if (! file.is_open())
      DUNE_THROW(Dune::IOError, "Could not write to file " << filename);

    for(size_t i=0; i<x.size(); ++i)
    {
      file << std::scientific << x[i] << " " << y[i] << std::endl;
    }

    file.close();
  }

  // write two or more valarrays to file, deleting all previous entries
  template<typename T>
  static void gnuplotMultiArray(const std::string filename,
                                const std::valarray<T>& x,
                                const std::vector<std::valarray<T> >& y )
  {
    std::ofstream file;
    file.open(filename.c_str());
    if (! file.is_open())
      DUNE_THROW(Dune::IOError, "Could not write to file " << filename);

    for(size_t i=0; i<x.size(); ++i)
    {
      file << std::scientific << x[i];
      for (size_t j=0; j<y.size(); j++) file << " " << y[j][i];
      file << std::endl;
    }

    file.close();
  }

  // write two or more valarrays to file, deleting all previous entries
  template<typename T>
  static void gnuplotAppendMultiArray(const std::string filename,
                                      const std::valarray<T>& x,
                                      const std::vector<std::valarray<T> >& y )
  {
    std::ofstream file;
    file.open(filename.c_str(), std::ofstream::app);
    if (! file.is_open())
      DUNE_THROW(Dune::IOError, "Could not write to file " << filename);

    for(size_t i=0; i<x.size(); ++i)
    {
      file << std::scientific << std::setprecision(4) << x[i];
      for (size_t j=0; j<y.size(); j++) file << " " << std::setprecision(10) << y[j][i];
      file << std::endl;
    }
    file << std::endl;
    file << std::endl;

    file.close();
  }

  // add valarrays to file, seperated by two blank lines from previous entry (accesible with
  // "index" in gnuplot), optionally with a info line starting with "#" (to be skipped in gnuplot)
  template<typename T>
  static void gnuplotAppendArray(const std::string filename,
                                 const std::valarray<T>& x,
                                 const std::valarray<T>& y,
                                 const std::string infoString = "")
  {
    std::ofstream file;
    file.open(filename.c_str(), std::ofstream::app);
    if (! file.is_open())
      DUNE_THROW(Dune::IOError, "Could not write to file " << filename);

    file << "" << std::endl;
    file << "" << std::endl;
    if ( infoString != "") file << "# " << infoString << std::endl;

    for(size_t i=0; i<x.size(); ++i)
    {
      file << std::scientific << x[i] << " " << y[i] << std::endl;
    }

    file.close();
  }
};

#endif
