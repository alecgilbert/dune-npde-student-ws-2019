//***********************************************************************
//***********************************************************************
// diffusion problem with initial condition and zero rhs
//***********************************************************************
//***********************************************************************

const double radius = 0.5, alpha = 50.0;

template<typename GV, typename RF>
class GenericProblem
{
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

public:
  typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

  GenericProblem () : time(0.0) {}

  //! tensor diffusion constant per cell? return false if you want more than one evaluation of A per cell.
  static constexpr bool permeabilityIsConstantPerCell()
  {
    return true;
  }

  //! tensor diffusion coefficient
  typename Traits::PermTensorType
  A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::PermTensorType I;
    for (std::size_t i=0; i<Traits::dimDomain; i++)
      for (std::size_t j=0; j<Traits::dimDomain; j++)
        I[i][j] = (i==j) ? 1.0 : 0.0;
    return I;
  }

  //! velocity field
  typename Traits::RangeType
  b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::RangeType v(0.0);
    return v;
  }

  //! sink term
  typename Traits::RangeFieldType
  c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! source term
  typename Traits::RangeFieldType
  f (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
  {
    typename Traits::DomainType x = e.geometry().global(xlocal);

    RF X0 = radius*std::cos(2*M_PI*time);
    RF X1 = radius*std::sin(2*M_PI*time);

    return std::exp(-alpha*( (x[0]-X0)*(x[0]-X0) + (x[1]-X1)*(x[1]-X1)))
      * (-4*alpha*M_PI*radius*( (x[0]-X0)*std::sin(2*M_PI*time) - (x[1]-X1)*std::cos(2*M_PI*time))
         -4*alpha*alpha*( (x[0]-X0)*(x[0]-X0) + (x[1]-X1)*(x[1]-X1))
         +4*alpha);
  }

  //! boundary condition type function
  BCType
  bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    //typename Traits::DomainType xglobal = is.geometry().global(x);
    return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
  }

  //! Dirichlet boundary condition value
  typename Traits::RangeFieldType
  g (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
  {
    typename Traits::DomainType x = e.geometry().global(xlocal);

    RF X0 = radius*std::cos(2*M_PI*time);
    RF X1 = radius*std::sin(2*M_PI*time);

    return std::exp(-alpha*( (x[0]-X0)*(x[0]-X0) + (x[1]-X1)*(x[1]-X1)));
  }

  //! Neumann boundary condition
  typename Traits::RangeFieldType
  j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! Neumann boundary condition
  typename Traits::RangeFieldType
  o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! set time for subsequent evaluation
  void setTime (RF t)
  {
    time = t;
    //std::cout << "setting time to " << time << std::endl;
  }

private:
  RF time;
};

//***********************************************************************
//***********************************************************************
// a grid function giving the gradient of the exact solution
// needed for computing H^1 errors
//***********************************************************************
//***********************************************************************

//! exact gradient of solution
template<typename GV, typename RF>
class ExactGradient
  : public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits
                                          <GV,RF,
                                           GV::dimension,Dune::FieldVector<RF,GV::dimension> >,
                                          ExactGradient<GV,RF> >
{
  GV gv;

public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,
                                           GV::dimension,Dune::FieldVector<RF,GV::dimension> > Traits;
  typedef Dune::PDELab::GridFunctionBase<Traits,ExactGradient<GV,RF> > BaseT;

  ExactGradient (GV gv_) : gv(gv_) {}

  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    typename Traits::DomainType x = e.geometry().global(xlocal);

    RF X0 = radius*std::cos(2*M_PI*time);
    RF X1 = radius*std::sin(2*M_PI*time);

    y[0] = std::exp(-alpha*( (x[0]-X0)*(x[0]-X0) + (x[1]-X1)*(x[1]-X1))) * (-2*alpha*(x[0]-X0));
    y[1] = std::exp(-alpha*( (x[0]-X0)*(x[0]-X0) + (x[1]-X1)*(x[1]-X1))) * (-2*alpha*(x[1]-X1));
  }

  inline const typename Traits::GridViewType& getGridView () const
  {
    return gv;
  }

  //! set time for subsequent evaluation
  void setTime (RF t)
  {
    time = t;
  }

private:
  RF time;
};
