// -*- tab-width: 2; indent-tabs-mode: nil -*-
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/pdelab/boilerplate/pdelab.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>
#include <dune/pdelab/localoperator/l2.hh>

#include "problem2.hh"

//***********************************************************************
//***********************************************************************
// some function adapters
//***********************************************************************
//***********************************************************************

/*! \brief Adapter returning f1(x)-f2(x) for two given grid functions

  \tparam T1  a grid function type
  \tparam T2  a grid function type
*/
template<typename T1, typename T2>
class DifferenceAdapter
  : public Dune::PDELab::GridFunctionBase<
  Dune::PDELab::GridFunctionTraits<typename T1::Traits::GridViewType,
                                   typename T1::Traits::RangeFieldType,
                                   1,Dune::FieldVector<typename T1::Traits::RangeFieldType,1> >
  ,DifferenceAdapter<T1,T2> >
{
public:
  typedef Dune::PDELab::GridFunctionTraits<typename T1::Traits::GridViewType,
                                           typename T1::Traits::RangeFieldType,
                                           1,Dune::FieldVector<typename T1::Traits::RangeFieldType,1> > Traits;

  //! constructor
  DifferenceAdapter (const T1& t1_, const T2& t2_) : t1(t1_), t2(t2_) {}

  //! \copydoc GridFunctionBase::evaluate()
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    typename Traits::RangeType y1;
    t1.evaluate(e,x,y1);
    typename Traits::RangeType y2;
    t2.evaluate(e,x,y2);
    y1 -= y2;
    y = y1;
  }

  inline const typename Traits::GridViewType& getGridView () const
  {
    return t1.getGridView();
  }

private:
  const T1& t1;
  const T2& t2;
};

/*! \brief Adapter returning ||f1(x)-f2(x)||^2 for two given grid functions

  \tparam T1  a grid function type
  \tparam T2  a grid function type
*/
template<typename T1, typename T2>
class DifferenceSquaredAdapter
  : public Dune::PDELab::GridFunctionBase<
  Dune::PDELab::GridFunctionTraits<typename T1::Traits::GridViewType,
                                   typename T1::Traits::RangeFieldType,
                                   1,Dune::FieldVector<typename T1::Traits::RangeFieldType,1> >
  ,DifferenceSquaredAdapter<T1,T2> >
{
public:
  typedef Dune::PDELab::GridFunctionTraits<typename T1::Traits::GridViewType,
                                           typename T1::Traits::RangeFieldType,
                                           1,Dune::FieldVector<typename T1::Traits::RangeFieldType,1> > Traits;

  //! constructor
  DifferenceSquaredAdapter (const T1& t1_, const T2& t2_) : t1(t1_), t2(t2_) {}

  //! \copydoc GridFunctionBase::evaluate()
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    typename T1::Traits::RangeType y1;
    t1.evaluate(e,x,y1);
    typename T2::Traits::RangeType y2;
    t2.evaluate(e,x,y2);
    y1 -= y2;
    y = y1.two_norm2();
  }

  inline const typename Traits::GridViewType& getGridView () const
  {
    return t1.getGridView();
  }

private:
  const T1& t1;
  const T2& t2;
};

/*! \brief Adapter returning ||f1(x)-f2(x)||^2 for two given grid functions

  \tparam T1  a grid function type
*/
template<typename T1>
class SquaredAdapter
  : public Dune::PDELab::GridFunctionBase<
  Dune::PDELab::GridFunctionTraits<typename T1::Traits::GridViewType,
                                   typename T1::Traits::RangeFieldType,
                                   1,Dune::FieldVector<typename T1::Traits::RangeFieldType,1> >
  ,SquaredAdapter<T1> >
{
public:
  typedef Dune::PDELab::GridFunctionTraits<typename T1::Traits::GridViewType,
                                           typename T1::Traits::RangeFieldType,
                                           1,Dune::FieldVector<typename T1::Traits::RangeFieldType,1> > Traits;

  //! constructor
  SquaredAdapter (const T1& t1_) : t1(t1_) {}

  //! \copydoc GridFunctionBase::evaluate()
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    typename T1::Traits::RangeType y1;
    t1.evaluate(e,x,y1);
    y = y1.two_norm2();
  }

  inline const typename Traits::GridViewType& getGridView () const
  {
    return t1.getGridView();
  }

private:
  const T1& t1;
};


//***********************************************************************
//***********************************************************************
// meshes for the ldomain
//***********************************************************************
//***********************************************************************

template<class Factory>
void ldomain_mesh (Factory& factory, Dune::GeometryType::BasicType elemtype)
{
  if (elemtype == Dune::GeometryType::simplex)
    {
      /* 5 - 6 - 7
         |   |   |
         2 - 3 - 4
         |   |
         0 - 1
      */
      Dune::FieldVector< double, 2 > pos;
      pos[0] = -1;  pos[1] = -1; factory.insertVertex(pos);
      pos[0] =  0;  pos[1] = -1; factory.insertVertex(pos);
      pos[0] = -1;  pos[1] =  0; factory.insertVertex(pos);
      pos[0] =  0;  pos[1] =  0; factory.insertVertex(pos);
      pos[0] =  1;  pos[1] =  0; factory.insertVertex(pos);
      pos[0] = -1;  pos[1] =  1; factory.insertVertex(pos);
      pos[0] =  0;  pos[1] =  1; factory.insertVertex(pos);
      pos[0] =  1;  pos[1] =  1; factory.insertVertex(pos);

      const Dune::GeometryType type( Dune::GeometryTypes::simplex(2) );
      std::vector< unsigned int > cornerIDs( 3 );
      cornerIDs[0] = 3; cornerIDs[1] = 0; cornerIDs[2] = 1;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 0; cornerIDs[1] = 3; cornerIDs[2] = 2;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 3; cornerIDs[1] = 5; cornerIDs[2] = 2;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 5; cornerIDs[1] = 3; cornerIDs[2] = 6;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 7; cornerIDs[1] = 3; cornerIDs[2] = 4;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 3; cornerIDs[1] = 7; cornerIDs[2] = 6;
      factory.insertElement( type, cornerIDs );
    }
  if (elemtype == Dune::GeometryType::cube)
    {
      /* 5 - 6 - 7
         |   |   |
         2 - 3 - 4
         |   |
         0 - 1
      */
      Dune::FieldVector< double, 2 > pos;
      pos[0] = -1;  pos[1] = -1; factory.insertVertex(pos);
      pos[0] =  0;  pos[1] = -1; factory.insertVertex(pos);
      pos[0] = -1;  pos[1] =  0; factory.insertVertex(pos);
      pos[0] =  0;  pos[1] =  0; factory.insertVertex(pos);
      pos[0] =  1;  pos[1] =  0; factory.insertVertex(pos);
      pos[0] = -1;  pos[1] =  1; factory.insertVertex(pos);
      pos[0] =  0;  pos[1] =  1; factory.insertVertex(pos);
      pos[0] =  1;  pos[1] =  1; factory.insertVertex(pos);

      const Dune::GeometryType type( Dune::GeometryTypes::cube(2) );
      std::vector< unsigned int > cornerIDs( 4 );
      cornerIDs[0] = 0; cornerIDs[1] = 1; cornerIDs[2] = 2; cornerIDs[3] = 3;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 2; cornerIDs[1] = 3; cornerIDs[2] = 5; cornerIDs[3] = 6;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 3; cornerIDs[1] = 4; cornerIDs[2] = 6; cornerIDs[3] = 7;
      factory.insertElement( type, cornerIDs );
    }
}

//***********************************************************************
//***********************************************************************
// meshes for the unit square
//***********************************************************************
//***********************************************************************

template<class Factory>
void square_mesh (Factory& factory, Dune::GeometryType::BasicType elemtype)
{
  if (elemtype == Dune::GeometryType::simplex)
    {
      /* 5 - 6 - 7
         |   |   |
         2 - 3 - 4
         |   |   |
         0 - 1 - 8
      */
      Dune::FieldVector< double, 2 > pos;
      pos[0] = -1;  pos[1] = -1; factory.insertVertex(pos);
      pos[0] =  0;  pos[1] = -1; factory.insertVertex(pos);
      pos[0] = -1;  pos[1] =  0; factory.insertVertex(pos);
      pos[0] =  0;  pos[1] =  0; factory.insertVertex(pos);
      pos[0] =  1;  pos[1] =  0; factory.insertVertex(pos);
      pos[0] = -1;  pos[1] =  1; factory.insertVertex(pos);
      pos[0] =  0;  pos[1] =  1; factory.insertVertex(pos);
      pos[0] =  1;  pos[1] =  1; factory.insertVertex(pos);
      pos[0] =  1;  pos[1] = -1; factory.insertVertex(pos);

      const Dune::GeometryType type( Dune::GeometryTypes::simplex(2) );
      std::vector< unsigned int > cornerIDs( 3 );
      cornerIDs[0] = 3; cornerIDs[1] = 0; cornerIDs[2] = 1;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 0; cornerIDs[1] = 3; cornerIDs[2] = 2;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 3; cornerIDs[1] = 5; cornerIDs[2] = 2;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 5; cornerIDs[1] = 3; cornerIDs[2] = 6;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 7; cornerIDs[1] = 3; cornerIDs[2] = 4;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 3; cornerIDs[1] = 7; cornerIDs[2] = 6;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 8; cornerIDs[1] = 3; cornerIDs[2] = 1;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 3; cornerIDs[1] = 8; cornerIDs[2] = 4;
      factory.insertElement( type, cornerIDs );
    }
  if (elemtype == Dune::GeometryType::cube)
    {
      /* 5 - 6 - 7
         |   |   |
         2 - 3 - 4
         |   |   |
         0 - 1 - 8
      */
      Dune::FieldVector< double, 2 > pos;
      pos[0] = -1;  pos[1] = -1; factory.insertVertex(pos);
      pos[0] =  0;  pos[1] = -1; factory.insertVertex(pos);
      pos[0] = -1;  pos[1] =  0; factory.insertVertex(pos);
      pos[0] =  0;  pos[1] =  0; factory.insertVertex(pos);
      pos[0] =  1;  pos[1] =  0; factory.insertVertex(pos);
      pos[0] = -1;  pos[1] =  1; factory.insertVertex(pos);
      pos[0] =  0;  pos[1] =  1; factory.insertVertex(pos);
      pos[0] =  1;  pos[1] =  1; factory.insertVertex(pos);
      pos[0] =  1;  pos[1] = -1; factory.insertVertex(pos);

      const Dune::GeometryType type( Dune::GeometryTypes::cube(2) );
      std::vector< unsigned int > cornerIDs( 4 );
      cornerIDs[0] = 0; cornerIDs[1] = 1; cornerIDs[2] = 2; cornerIDs[3] = 3;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 2; cornerIDs[1] = 3; cornerIDs[2] = 5; cornerIDs[3] = 6;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 3; cornerIDs[1] = 4; cornerIDs[2] = 6; cornerIDs[3] = 7;
      factory.insertElement( type, cornerIDs );
      cornerIDs[0] = 1; cornerIDs[1] = 8; cornerIDs[2] = 3; cornerIDs[3] = 4;
      factory.insertElement( type, cornerIDs );
    }
}

//***********************************************************************
//***********************************************************************
// The driver
//***********************************************************************
//***********************************************************************

template<typename GM, unsigned int degree, Dune::GeometryType::BasicType elemtype,
         Dune::PDELab::MeshType meshtype, Dune::SolverCategory::Category solvertype>
void driver (double T, double dt, Dune::shared_ptr<GM> grid, int prerefine_level, double tol, int maxsteps,
             double fraction, std::string basename)
{
  // define parameters
  //const unsigned int dim = GM::dimension;
  typedef double NumberType;

  // make grid
  for (int i=0; i<prerefine_level; i++) grid->globalRefine(1);

  // make problem parameters
  typedef GenericProblem<typename GM::LeafGridView,NumberType> Problem;
  Problem problem;
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<Problem> BCType;
  BCType bctype(grid->leafGridView(),problem);

  // make a finite element space
  typedef Dune::PDELab::CGSpace<GM,NumberType,degree,BCType,elemtype,meshtype,solvertype> FS;
  FS fs(*grid,bctype);

  // make a degree of freedom vector
  typedef typename FS::DOF X;
  X x(fs.getGFS(),0.0);

  // set initial value
  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<Problem> G;
  G g(grid->leafGridView(),problem);
  problem.setTime(0.0);
  Dune::PDELab::interpolate(g,fs.getGFS(),x);

  // measure initial error
  {
    std::cout << "setTime in initial value" << std::endl;
    typename FS::DGF xdgf(fs.getGFS(),x);
    typedef DifferenceSquaredAdapter<G,typename FS::DGF> DifferenceSquared;
    DifferenceSquared differencesquared(g,xdgf);
    typename DifferenceSquared::Traits::RangeType l2errorsquared(0.0);
    Dune::PDELab::integrateGridFunction(differencesquared,l2errorsquared,8);
    std::cout << "l2error in step " << 0 << " is " << sqrt(l2errorsquared) << std::endl;
  }

  // graphics for initial guess
  Dune::PDELab::FilenameHelper fn(basename);
  {
    Dune::VTKWriter<typename GM::LeafGridView> vtkwriter(grid->leafGridView());
    typename FS::DGF xdgf(fs.getGFS(),x);
    vtkwriter.addVertexData(std::make_shared<typename FS::VTKF>(xdgf,"x_h"));
    vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<G> >(g,"x"));
    vtkwriter.write(fn.getName(),Dune::VTK::appendedraw);
    fn.increment();
  }

  // time loop
  NumberType time = 0.0;
  int step=1;
  NumberType exact_error = 0.0;
  NumberType energy_squared = 0.0;
  Dune::PDELab::TimeAdaptationStrategy strategy(tol,T,2);

  // parameters for problem 1
  strategy.setAdaptationOn();
  strategy.setTemporalScaling(3.5);
  strategy.setOptimisticFactor(1.0);
  strategy.setCoarsenLimit(0.7);
  strategy.setBalanceLimit(0.4);
  strategy.setRefineFractionWhileRefinement(0.75);
  strategy.setCoarsenFractionWhileRefinement(0.01);
  strategy.setCoarsenFractionWhileCoarsening(0.15);
  strategy.setTimeStepDecreaseFactor(0.5);
  strategy.setTimeStepIncreaseFactor(1.2);
  strategy.setMinEnergyRate(0.05);

  // parameters for problem 2
  //strategy.setAdaptationOff();
  //strategy.setTemporalScaling(1.0);
  ////strategy.setTemporalScaling(5.0);
  //strategy.setOptimisticFactor(1.0);
  //strategy.setCoarsenLimit(0.7);
  //strategy.setBalanceLimit(0.3333);
  //strategy.setRefineFractionWhileRefinement(0.4);
  //strategy.setCoarsenFractionWhileRefinement(0.05);
  //strategy.setCoarsenFractionWhileCoarsening(0.1);
  //strategy.setTimeStepDecreaseFactor(0.75);
  //strategy.setTimeStepIncreaseFactor(1.25);
  //strategy.setMinEnergyRate(0.0);

  std::cout << "# ACCEPT time_step time dt DOF estimated_error true_error q_s q_t energy" << std::endl;

  while (time<T-1e-10)
    {
      std::cout << "+++" << " step=" << step << std::endl;

      // adaptive loop within time step
      int substeps=40;
      bool success=false;
      strategy.startTimeStep();
      for (int substep=1; substep<=substeps; substep++)
        {
          std::cout << "+++ substep " << substep << std::endl;

          //============================
          // compute solution at time+dt
          //============================

          // assemble constraints for new time step
          problem.setTime(time+dt);
          fs.assembleConstraints(bctype);

          // assembler for finite element problem
          const int nonzeros = 7;
          typedef Dune::PDELab::ConvectionDiffusionFEM<Problem,typename FS::FEM> LOP;
          LOP lop(problem,4);
          typedef Dune::PDELab::GalerkinGlobalAssembler<FS,LOP,solvertype> SASS;
          SASS sass(fs,lop,nonzeros);
          typedef Dune::PDELab::L2 MLOP;
          MLOP mlop(2*degree);
          typedef Dune::PDELab::GalerkinGlobalAssembler<FS,MLOP,solvertype> TASS;
          TASS tass(fs,mlop,nonzeros);
          typedef Dune::PDELab::OneStepGlobalAssembler<SASS,TASS> ASSEMBLER;
          ASSEMBLER assembler(sass,tass);
          // typedef Dune::PDELab::OneStepGridOperator<typename ASS::GO,typename TASS::GO> IGO;
          // IGO igo(*ass,*tass);

          // linear solver backend
          typedef Dune::PDELab::ISTLSolverBackend_CG_AMG_SSOR<FS,ASSEMBLER,solvertype> SBE;
          SBE sbe(fs,assembler,5000,1);
          //typedef Dune::PDELab::ISTLBackend_SEQ_CG_SSOR SBE;
          //SBE sbe(5000,1);

          // make Newton for time-dependent problem
          typedef Dune::PDELab::Newton<typename ASSEMBLER::GO,typename SBE::LS,X> PDESOLVER;
          PDESOLVER tnewton(*assembler,*sbe);
          tnewton.setReassembleThreshold(0.0);
          tnewton.setVerbosityLevel(0);
          tnewton.setReduction(0.9);
          tnewton.setMinLinearReduction(1e-9);

          // time-stepper
          Dune::PDELab::OneStepThetaParameter<NumberType> method(1.0);
          Dune::PDELab::OneStepMethod<NumberType,typename ASSEMBLER::GO,PDESOLVER,X> osm(method,*assembler,tnewton);
          osm.setVerbosityLevel(2);

          // do time step
          X xnew(fs.getGFS(),0.0);
          osm.apply(time,dt,x,g,xnew);

          //====================================
          // compute a-posteriori error estimate
          //====================================

          // compute jump
          typedef Dune::PDELab::P0Space<GM,NumberType,elemtype,solvertype> ESTFS;
          ESTFS estfs(grid->leafGridView());
          X jump(fs.getGFS());
          jump = xnew;
          jump -= x;

          // spatial part
          typedef Dune::PDELab::ConvectionDiffusionFEMResidualEstimator<Problem> SESTLOP;
          SESTLOP sestlop(problem);
          typedef Dune::PDELab::GlobalAssembler<FS,ESTFS,SESTLOP,solvertype> SESTASS;
          SESTASS sestass(fs,estfs,sestlop,nonzeros);
          typedef typename ESTFS::DOF Y;
          Y eta1(estfs.getGFS(),0.0);
          problem.setTime(time+0.5*dt);
          sestass->residual(x,eta1);
          eta1 *= dt; // integration over time interval
          typedef Dune::PDELab::ConvectionDiffusionTemporalResidualEstimator1<Problem> TESTLOP1;
          TESTLOP1 testlop1(problem,time,dt);
          typedef Dune::PDELab::GlobalAssembler<FS,ESTFS,TESTLOP1,solvertype> TESTASS1;
          TESTASS1 testass1(fs,estfs,testlop1,nonzeros);
          testass1->residual(jump,eta1);

          // temporal part
          typedef Dune::PDELab::ConvectionDiffusionTemporalResidualEstimator<Problem> TESTLOP2;
          TESTLOP2 testlop2(problem,time,dt);
          typedef Dune::PDELab::GlobalAssembler<FS,ESTFS,TESTLOP2,solvertype> TESTASS2;
          TESTASS2 testass2(fs,estfs,testlop2,nonzeros);
          Y eta2(estfs.getGFS(),0.0);
          testass2->residual(jump,eta2);

          // compute energy of computed solution needed in adaptation strategy
          typename FS::DGF xnewdgf(fs.getGFS(),xnew);
          SquaredAdapter<typename FS::DGF> xnewsquareddgf(xnewdgf);
          typename SquaredAdapter<typename FS::DGF>::Traits::RangeType l2xnewsquared(0.0);
          Dune::PDELab::integrateGridFunction(xnewsquareddgf,l2xnewsquared,2*degree);
          typename FS::DGF xolddgf(fs.getGFS(),x);
          SquaredAdapter<typename FS::DGF> xoldsquareddgf(xolddgf);
          typename SquaredAdapter<typename FS::DGF>::Traits::RangeType l2xoldsquared(0.0);
          Dune::PDELab::integrateGridFunction(xoldsquareddgf,l2xoldsquared,2*degree);
          NumberType energy_timeslab = 0.5*dt*(l2xoldsquared+l2xnewsquared);
          std::cout << "energy_timeslab: " << energy_timeslab << std::endl;
          std::cout << "l2 xold squared: " << l2xoldsquared << std::endl;
          std::cout << "l2 xnew squared: " << l2xnewsquared << std::endl;

          //====================================
          // compute true error (known here)
          //====================================

          std::cout << "exact error part 1 " << std::endl;
          NumberType exact_error_timeslab = 0.0;
          // use simpson rule in time, numerical solution is piecewise constant in time
          {
            typename FS::DGF xdgf(fs.getGFS(),xnew);
            typedef DifferenceSquaredAdapter<G,typename FS::DGF> DifferenceSquared;
            DifferenceSquared differencesquared(g,xdgf);
            problem.setTime(time);
            typename DifferenceSquared::Traits::RangeType l2errorsquared(0.0);
            Dune::PDELab::integrateGridFunction(differencesquared,l2errorsquared,8);

            typedef Dune::PDELab::DiscreteGridFunctionGradient<typename FS::GFS,X> DGFGrad;
            DGFGrad xdgfgrad(fs.getGFS(),xnew);
            typedef ExactGradient<typename GM::LeafGridView,NumberType> Grad;
            Grad grad(grid->leafGridView());
            grad.setTime(time);
            typedef DifferenceSquaredAdapter<Grad,DGFGrad> GradDifferenceSquared;
            GradDifferenceSquared graddifferencesquared(grad,xdgfgrad);
            typename GradDifferenceSquared::Traits::RangeType h1semierrorsquared(0.0);
            Dune::PDELab::integrateGridFunction(graddifferencesquared,h1semierrorsquared,8);

            exact_error_timeslab += (1.0/6.0)*dt*(l2errorsquared+h1semierrorsquared);
          }

          {
            typename FS::DGF xdgf(fs.getGFS(),xnew);
            typedef DifferenceSquaredAdapter<G,typename FS::DGF> DifferenceSquared;
            DifferenceSquared differencesquared(g,xdgf);
            problem.setTime(time+0.5*dt);
            typename DifferenceSquared::Traits::RangeType l2errorsquared(0.0);
            Dune::PDELab::integrateGridFunction(differencesquared,l2errorsquared,8);

            typedef Dune::PDELab::DiscreteGridFunctionGradient<typename FS::GFS,X> DGFGrad;
            DGFGrad xdgfgrad(fs.getGFS(),xnew);
            typedef ExactGradient<typename GM::LeafGridView,NumberType> Grad;
            Grad grad(grid->leafGridView());
            grad.setTime(time+0.5*dt);
            typedef DifferenceSquaredAdapter<Grad,DGFGrad> GradDifferenceSquared;
            GradDifferenceSquared graddifferencesquared(grad,xdgfgrad);
            typename GradDifferenceSquared::Traits::RangeType h1semierrorsquared(0.0);
            Dune::PDELab::integrateGridFunction(graddifferencesquared,h1semierrorsquared,8);
            exact_error_timeslab += (2.0/3.0)*dt*(l2errorsquared+h1semierrorsquared);
          }

          {
            typename FS::DGF xdgf(fs.getGFS(),xnew);
            typedef DifferenceSquaredAdapter<G,typename FS::DGF> DifferenceSquared;
            DifferenceSquared differencesquared(g,xdgf);
            problem.setTime(time+dt);
            typename DifferenceSquared::Traits::RangeType l2errorsquared(0.0);
            Dune::PDELab::integrateGridFunction(differencesquared,l2errorsquared,8);

            typedef Dune::PDELab::DiscreteGridFunctionGradient<typename FS::GFS,X> DGFGrad;
            DGFGrad xdgfgrad(fs.getGFS(),xnew);
            typedef ExactGradient<typename GM::LeafGridView,NumberType> Grad;
            Grad grad(grid->leafGridView());
            grad.setTime(time+dt);
            typedef DifferenceSquaredAdapter<Grad,DGFGrad> GradDifferenceSquared;
            GradDifferenceSquared graddifferencesquared(grad,xdgfgrad);
            typename GradDifferenceSquared::Traits::RangeType h1semierrorsquared(0.0);
            Dune::PDELab::integrateGridFunction(graddifferencesquared,h1semierrorsquared,8);

            exact_error_timeslab += (1.0/6.0)*dt*(l2errorsquared+h1semierrorsquared);
          }

          //====================================
          // adaptation strategy
          //====================================

          strategy.evaluate_estimators(*grid,time,dt,eta1,eta2,energy_timeslab);
          std::cout << "estimate strategy finished" << std::endl;
          // output to VTK file if step is accepted
          if (strategy.acceptTimeStep())
            {
              Dune::VTKWriter<typename GM::LeafGridView> vtkwriter(grid->leafGridView());
              typename FS::DGF xdgf(fs.getGFS(),xnew);
              vtkwriter.addVertexData(std::make_shared<typename FS::VTKF>(xdgf,"x_h"));
              problem.setTime(time+dt);
              vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<G> >(g,"x"));
              typedef Dune::PDELab::DiscreteGridFunctionGradient<typename FS::GFS,X> DGFGrad;
              DGFGrad xdgfgrad(fs.getGFS(),xnew);
              vtkwriter.addCellData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGFGrad> >(xdgfgrad,"grad u_h"));
              typedef ExactGradient<typename GM::LeafGridView,NumberType> Grad;
              Grad grad(grid->leafGridView());
              grad.setTime(time+dt);
              vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<Grad> >(grad,"grad u"));
              typename ESTFS::DGF eta1dgf(estfs.getGFS(),eta1);
              vtkwriter.addCellData(std::make_shared<typename ESTFS::VTKF>(eta1dgf,"eta1"));
              typename ESTFS::DGF eta2dgf(estfs.getGFS(),eta2);
              vtkwriter.addCellData(std::make_shared<typename ESTFS::VTKF>(eta2dgf,"eta2"));
              vtkwriter.write(fn.getName(),Dune::VTK::appendedraw);
              fn.increment();
            }

          // accept time step
          if (strategy.acceptTimeStep())
            {
              exact_error += exact_error_timeslab;
              energy_squared += energy_timeslab;
              std::cout << "ACCEPT " << step << " " << time+dt << " " << dt << " " << x.N()
                        << " " << sqrt(strategy.accumulatedErrorSquared())
                        << " " << sqrt(exact_error)
                // << " " << strategy.accumulatedErrorSquared()
                // << " " << exact_error
                        << " " << strategy.qs()
                        << " " << strategy.qt()
                        << " " << sqrt(energy_squared)
                        << std::endl;
              x = xnew;
              time += dt;
              step++;
            }

          if (strategy.adaptDT())
            {
              std::cout << "+++ set new dt to " << strategy.newDT() << std::endl;
              dt = strategy.newDT();
            }

          // adapt grid
          if (strategy.adaptGrid())
            {
              std::cout << "+++ will adapt grid N=" << fs.getGFS().globalSize() << std::endl;
              Dune::PDELab::adapt_grid(*grid,fs.getGFS(),x,2*degree);
              std::cout << "+++ have adapted grid N=" << fs.getGFS().globalSize() << std::endl;
              if (time==0.0)
                {
                  // set inital value correctly
                  std::cout << "+++ interpolate initial condition" << std::endl;
                  problem.setTime(0.0);
                  Dune::PDELab::interpolate(g,fs.getGFS(),x);
                }
            }

          if (strategy.acceptTimeStep())
            {
              success=true;
              break;    // exit the adaptive loop
            }
          else
            {
              if (substep==substeps) success=false;
              continue; // else: redo time step
            }

        } // end adaptive loop within time step

      if (!success)
        {
          std::cout << "+++ COULD NOT SOLVE TIMESTEP " << step << " SUCCESSFULLY!" << std::endl;
          return;
        }
    }
}

//***********************************************************************
//***********************************************************************
// The main function
//***********************************************************************
//***********************************************************************

int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // read command line arguments
  if (argc!=4)
    {
      std::cout << "usage: " << argv[0] << " <dt> <prerefinelevel> <tol>" << std::endl;
      return 0;
    }
  double dt; sscanf(argv[1],"%lg",&dt);
  int prerefinelevel; sscanf(argv[2],"%d",&prerefinelevel);
  double tol; sscanf(argv[3],"%lg",&tol);

  // start try/catch block to get error messages from dune
  try {

    const int dim=2;
    const int degree=1;
    const Dune::SolverCategory::Category solvertype = Dune::SolverCategory::sequential;

    const Dune::GeometryType::BasicType elemtype = Dune::GeometryType::simplex;
    const Dune::PDELab::MeshType meshtype = Dune::PDELab::MeshType::conforming;
#if HAVE_UG
    typedef Dune::UGGrid<dim> GMa;
    Dune::UGGrid<dim>::setDefaultHeapSize(1500);
    Dune::GridFactory<GMa> factory;
    // ldomain_mesh(factory,elemtype);
    square_mesh(factory,elemtype);
    Dune::shared_ptr<GMa> grid(factory.createGrid());

    // std::array<double,dim> ll; ll.fill(-1.0);
    // std::array<double,dim> ur; ur.fill(1.0);
    // std::array<unsigned int,dim> cells; cells.fill(2);
    // Dune::PDELab::StructuredGrid<GM> grid(elemtype,ll,ur,cells);

    std::stringstream basename;
    basename << "problem2" << "_ug_adaptive_P" << degree;
    driver<GMa,degree,elemtype,meshtype,solvertype>(1.0,dt,grid,prerefinelevel,tol,50,0.5,basename.str());


#endif
#if HAVE_ALBERTA
    typedef Dune::AlbertaGrid<dim,dim> GM;

    Dune::GridFactory<GM> factory;
    // ldomain_mesh(factory,elemtype);
    square_mesh(factory,elemtype);
    Dune::shared_ptr<GM> grid(factory.createGrid());

    // std::array<double,dim> ll; ll.fill(-1.0);
    // std::array<double,dim> ur; ur.fill(1.0);
    // std::array<unsigned int,dim> cells; cells.fill(2);
    // Dune::PDELab::StructuredGrid<GM> grid(elemtype,ll,ur,cells);

    std::stringstream basename;
    basename << "problem2" << "_alberta_adaptive_P" << degree;
    driver<GM,degree,elemtype,meshtype,solvertype>(1.0,dt,grid,prerefinelevel,tol,50,0.5,basename.str());
#endif
  }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
