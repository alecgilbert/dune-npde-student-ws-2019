#ifndef NDPDELAB_TOOLS_HH
#define NDPDELAB_TOOLS_HH

#include <valarray>
#include <vector>
#include <algorithm>

//#include<dune/common/geometrytype.hh>
#include<dune/geometry/quadraturerules.hh>

class Tools
{
public:

  //! \brief get positions and solution out of the discrete grid function
  template<typename DGF, typename T>
  static void getSolutionVector(const DGF& udgf, int numSamplingPoints, std::valarray<T>& pos, std::valarray<T>& sol)
  {
    typedef typename DGF::Traits::GridViewType            GV;
    typedef typename GV::template Codim<0>::Entity        Entity;
    typedef typename GV::template Codim<0>::EntityPointer EntityPointer;
    typedef typename GV::template Codim<0>::Iterator      ElementLeafIterator;

    GV gv = udgf.getGridView();
    int solSize = gv.size(0) * numSamplingPoints + gv.size(1);

    pos.resize(solSize);
    sol.resize(solSize);

    typename DGF::Traits::DomainType x(0.0);
    typename DGF::Traits::RangeType y(0.0);
    typename DGF::Traits::DomainType x_l(0.0);
    typename DGF::Traits::DomainType x_r(0.0);

    int i = 0;
    for (ElementLeafIterator nit=gv.template begin<0>(); nit!=gv.template end<0>(); ++nit)
    {
      typename DGF::Traits::ElementType& e = *nit;

      x = typename DGF::Traits::DomainType(0.);
      x_l = e.geometry().corner(0)[0];
      x_r = e.geometry().corner(1)[0];


      for(int j=0; j<numSamplingPoints+1; ++j)
      {
        udgf.evaluate(e,x,y);
        pos[i] = (T) (x_l + (x_r-x_l)*x);
        sol[i] = (T) y;
        ++i;
        x += (1./(numSamplingPoints+1));
      }
      udgf.evaluate(e,x,y);
    }

    // handle last grid point
    pos[i] = (T) (x_l + (x_r-x_l)*x);
    sol[i] = (T) y;
  }


  //! \brief compute order of convergence for given error(dof)-curve through central differences
  template<typename T>
  static void convergenceOrder(const std::valarray<T>& dof,
                               const std::valarray<T>& err,
                                     std::valarray<T>& mdof,
                                     std::valarray<T>& ord)
  {
    for (int i=0; i<mdof.size(); ++i)
    {
      mdof[i] = 0.5 * ( dof[i] + dof[i+1] ); // midpoint between two dof values

      ord[i] = - ( log( err[i+1] ) - log( err[i] ) ) / ( log( dof[i+1] ) - log( dof[i] ) );
    }
  }

  //! \brief compute L2 norm of a function
  template<typename T>
  static T normL2(const std::valarray<T>& x, const std::valarray<T>& y)
  {
    T bernd = 0.0;
    for (int i=0; i<x.size()-1; ++i)
      bernd += ( x[i+1] - x[i] ) * 0.5 * ( y[i+1] * y[i+1] + y[i] * y[i] );
    return sqrt( bernd );
  }

  //! \brief globally refining a std::vector
  template<typename T>
  static void globalRefineVector( std::vector<T>& v, const int level )
  {
    for (int j=0; j<level; ++j)
    {
      int currentSize = v.size();

      for (int i=0; i<currentSize-1; ++i) v.push_back( 0.5 * ( v[i+1] + v[i] ) );
      sort( v.begin(), v.end() );
    }
  }

  //! \brief compute second derivative of an valarray
  template<typename T>
  static void secondDerivative(const std::valarray<T>& x,
                               const std::valarray<T>& f,
                                     std::valarray<T>& d2f_dx2)
  {
    d2f_dx2 = 0.0;

    for (int i=1; i<x.size()-1; ++i)
    d2f_dx2[i] = -(( f[i+1] - f[i] ) / ( x[i+1] - x[i] ) - ( f[i] - f[i-1] ) / ( x[i] - x[i-1] ))
    * 2.0 / ( x[i+1] - x[i-1] );
  }

  //! \brief  compute integral of f(x)
  template<typename T>
  static T integral(const std::valarray<T>& x, const std::valarray<T>& f)
  {
    T bernd = 0.0;
    for (int i=0; i<x.size()-1; ++i) bernd += 0.5 * ( f[i+1] + f[i] ) * ( x[i+1] - x[i] );
  }

  //! \brief compute || u - x^h ||_0, \Omega
  template<typename U, typename GFS, typename X, typename T>
  double l2Error(const U& u, const GFS& gfs, const X& x, T time, int qorder=2)
  {
    typedef typename GFS::Traits::GridViewType GV;
    const int dim = GV::Grid::dimension;
    typedef typename GV::Traits::template Codim<0>::Iterator ElementIterator;
    typedef typename GFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits FTraits;
    typedef typename FTraits::DomainFieldType D;
    typedef typename FTraits::RangeFieldType R;
    typedef typename FTraits::RangeType RangeType;

    // make local function space
    typedef typename GFS::LocalFunctionSpace LFS;
    LFS lfs(gfs);
    std::vector<R> xl(lfs.maxSize());        // local coefficients
    std::vector<RangeType> b(lfs.maxSize()); // shape function values

    // loop over grid view and accumulate
    T sum(0.0);
    for (ElementIterator eit=gfs.gridview().template begin<0>(); eit!=gfs.gridview().template end<0>(); ++eit)
    {
      lfs.bind(*eit);  // bind local function space to element /*@\label{l2int:bind}@*/
      lfs.vread(x, xl); // get local coefficients

      Dune::GeometryType gt = eit->geometry().type();
      const Dune::QuadratureRule<D,dim>& rule = Dune::QuadratureRules<D,dim>::rule(gt,qorder);
      for (typename Dune::QuadratureRule<D,dim>::const_iterator qit=rule.begin(); qit!=rule.end(); ++qit)
      {
        // evaluate finite element function at integration point
        RangeType u_fe(0.0);
        lfs.finiteElement().localBasis().evaluateFunction(qit->position(), b);
        for (size_t i=0; i<lfs.size(); i++)
        {
          u_fe.axpy(xl[i], b[i]);
        }

        // evaluate the given grid function at integration point
        RangeType u_given(0.0);
        u_given = u.evaluate(eit->geometry().global(qit->position()), time);

        // accumulate error
        u_fe -= u_given;
        sum  += u_fe.two_norm2()*qit->weight() * eit->geometry().integrationElement(qit->position());
      }
    }

    return sqrt(sum);
  }
};

#endif
