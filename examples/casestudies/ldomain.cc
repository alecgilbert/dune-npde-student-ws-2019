#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <dune/common/parallel/mpihelper.hh> // include mpi helper class
#include<dune/common/parametertreeparser.hh>
#include <dune/grid/onedgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif
#include<dune/grid/io/file/gmshreader.hh>
#include<dune/istl/bvector.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/io.hh>
#include<dune/istl/paamg/amg.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/common/functionutilities.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/stationary/linearproblem.hh>
#include<dune/pdelab/finiteelementmap/pkfem.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>

#include "../../dune/npde/utilities/functionutilities.hh"


template<typename GV, typename RF>
class ReentrantCornerProblem
{
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

public:
  typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

  //! tensor diffusion constant per cell? return false if you want more than one evaluation of A per cell.
  static constexpr bool permeabilityIsConstantPerCell()
  {
    return true;
  }

  //! tensor diffusion coefficient
  typename Traits::PermTensorType
  A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::PermTensorType I;
    for (std::size_t i=0; i<Traits::dimDomain; i++)
      for (std::size_t j=0; j<Traits::dimDomain; j++)
        I[i][j] = (i==j) ? 1 : 0;
    return I;
  }

  //! velocity field
  typename Traits::RangeType
  b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::RangeType v(0.0);
    return v;
  }

  //! sink term
  typename Traits::RangeFieldType
  c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! source term
  typename Traits::RangeFieldType
  f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! boundary condition type function
  BCType
  bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    //typename Traits::DomainType xglobal = is.geometry().global(x);
    return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
  }

  //! Dirichlet boundary condition value
  typename Traits::RangeFieldType
  g (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
  {
    typename Traits::DomainType x = e.geometry().global(xlocal);

    typename Traits::DomainFieldType theta = std::atan2(x[1], x[0]);
    if(theta < 0.0) theta += 2*M_PI;
    typename Traits::DomainFieldType r = x.two_norm();

    return pow(r,2.0/3.0)*std::sin(theta*2.0/3.0);
  }

  //! Neumann boundary condition
  typename Traits::RangeFieldType
  j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! Neumann boundary condition
  typename Traits::RangeFieldType
  o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }
};

//! exact gradient of solution
template<typename GV, typename RF>
class ExactGradient
  : public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<GV,RF,
                                                                           GV::dimension,Dune::FieldVector<RF,GV::dimension> >,
                                          ExactGradient<GV,RF> >
{
  GV gv;

public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,
                                           GV::dimension,Dune::FieldVector<RF,GV::dimension> > Traits;
  typedef Dune::PDELab::GridFunctionBase<Traits,ExactGradient<GV,RF> > BaseT;

  ExactGradient (const GV& gv_) : gv(gv_) {}

  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    typename Traits::DomainType x = e.geometry().global(xlocal);

    typename Traits::DomainFieldType theta = std::atan2(x[1], x[0]);
    if(theta < 0.0) theta += 2*M_PI;
    typename Traits::DomainFieldType r = x.two_norm();

    y[0] = (2.0/3.0)*pow(r,-1.0/3.0)*(cos(theta)*sin(2.0*theta/3.0) - sin(theta)*cos(2.0*theta/3.0));
    y[1] = (2.0/3.0)*pow(r,-1.0/3.0)*(sin(theta)*sin(2.0*theta/3.0) + cos(theta)*cos(2.0*theta/3.0));
  }

  inline const typename Traits::GridViewType& getGridView () const
  {
    return gv;
  }
};

//! solve the diffusion problem on a given GridView with a given finite element space
template<class GV, class FEM, class CON>
void driver (const GV& gv, FEM& fem, std::string filename_base, int& N, double& l2e, double& h1se)
{
  // result type
  typedef double Real;

  // make grid function space
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem);
  gfs.update();
  N = gfs.globalSize();

  // make problem
  typedef ReentrantCornerProblem<GV,double> Problem;
  Problem problem;

  // make a std::vector of degree of freedom vectors and initialize it with Dirichlet extension
  using U = Dune::PDELab::Backend::Vector<GFS,Real>;
  U u(gfs,0.0);
  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<Problem> G;
  G g(gv,problem);
  Dune::PDELab::interpolate(g,gfs,u);

  // make constraints container and initialize it
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;
  Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<Problem> bctype(gv,problem);
  Dune::PDELab::constraints(bctype,gfs,cc);
  Dune::PDELab::set_nonconstrained_dofs(cc,0.0,u);
  std::cout << "constrained dofs=" << cc.size() << " of " << gfs.globalSize() << std::endl;

  // make local operator
  typedef Dune::PDELab::ConvectionDiffusionFEM<Problem,FEM> LOP;
  LOP lop(problem);
  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
  MBE mbe(5); //this should be initialized in consisten way with problem (no relocation needed)
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;
  GO go(gfs,cc,gfs,cc,lop,mbe);

  // make linear solver and solve problem
  typedef Dune::PDELab::ISTLBackend_SEQ_UMFPack LS;
  LS ls(false);
  typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
  SLP slp(go,ls,u,1e-6);
  slp.apply();

  // compute errors
  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
  DGF udgf(gfs,u);
  typedef DifferenceSquaredAdapter<G,DGF> DifferenceSquared;
  DifferenceSquared differencesquared(g,udgf);
  typename DifferenceSquared::Traits::RangeType l2errorsquared(0.0);
  Dune::PDELab::integrateGridFunction(differencesquared,l2errorsquared,10);
  l2e = sqrt(l2errorsquared);
  typedef Dune::PDELab::DiscreteGridFunctionGradient<GFS,U> DGFGrad;
  DGFGrad udgfgrad(gfs,u);
  typedef ExactGradient<GV,double> Grad;
  Grad grad(gv);
  typedef DifferenceSquaredAdapter<Grad,DGFGrad> GradDifferenceSquared;
  GradDifferenceSquared graddifferencesquared(grad,udgfgrad);
  typename GradDifferenceSquared::Traits::RangeType h1semierrorsquared(0.0);
  Dune::PDELab::integrateGridFunction(graddifferencesquared,h1semierrorsquared,10);
  h1se = sqrt(h1semierrorsquared);

  // write vtk file
  typedef DifferenceAdapter<G,DGF> Difference;
  Difference difference(g,udgf);
  Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,Dune::refinementLevels(3));
  //Dune::VTKWriter<GV> vtkwriter(gv);
  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF> >(udgf,"u_h"));
  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<G> >(g,"u"));
  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<Difference> >(difference,"u-u_h"));
  vtkwriter.write(filename_base,Dune::VTK::appendedraw);
}

int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // start try/catch block to get error messages from dune
  try {

    // read parameters from file
    Dune::ParameterTree configuration;
    Dune::ParameterTreeParser parser;
    parser.readINITree("ldomain.ini", configuration);
    const int max_level = configuration.get<int>("grid.max_level");
    const int degree = configuration.get<int>("method.degree");
    std::string grid_file = configuration.get<std::string>("grid.filename");

    // UGGrid/Pk 2D version
#if HAVE_UG
    if (true)
      {
        // make uggrid
        typedef Dune::UGGrid<2> GridType;
        GridType grid;
        // read gmsh file
        Dune::GridFactory<GridType> factory(&grid);
        Dune::GmshReader<GridType>::read(factory,"ldomain.msh",true,true);
        factory.createGrid();

        std::vector<double> l2(max_level+1,0.0);
        std::vector<double> h1s(max_level+1,0.0);
        std::vector<int> N(max_level+1,0);

        for (int i=0; i<=max_level; i++)
          {
            typedef GridType::LeafGridView GV;
            GV gv=grid.leafGridView();

            // make finite element space and solve
            typedef Dune::PDELab::ConformingDirichletConstraints CON;
            if (degree==1) {
              typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GridType::ctype,double,1> FEM;
              FEM fem(gv);
              std::stringstream filename;
              filename << "ldomain_" << grid_file << "_l" << i << "_k" << degree;
              driver<GV,FEM,CON>(gv,fem,filename.str(),N[i],l2[i],h1s[i]);
            }
            if (degree==2) {
              typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GridType::ctype,double,2> FEM;
              FEM fem(gv);
              std::stringstream filename;
              filename << "ldomain_" << grid_file << "_l" << i << "_k" << degree;
              driver<GV,FEM,CON>(gv,fem,filename.str(),N[i],l2[i],h1s[i]);
            }
            if (degree==3) {
              typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GridType::ctype,double,3> FEM;
              FEM fem(gv);
              std::stringstream filename;
              filename << "ldomain_" << grid_file << "_l" << i << "_k" << degree;
              driver<GV,FEM,CON>(gv,fem,filename.str(),N[i],l2[i],h1s[i]);
            }
            if (degree==4) {
              typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GridType::ctype,double,4> FEM;
              FEM fem(gv);
              std::stringstream filename;
              filename << "ldomain_" << grid_file << "_l" << i << "_k" << degree;
              driver<GV,FEM,CON>(gv,fem,filename.str(),N[i],l2[i],h1s[i]);
            }
            if (degree==5) {
              typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GridType::ctype,double,5> FEM;
              FEM fem(gv);
              std::stringstream filename;
              filename << "ldomain_" << grid_file << "_l" << i << "_k" << degree;
              driver<GV,FEM,CON>(gv,fem,filename.str(),N[i],l2[i],h1s[i]);
            }
            if (i<max_level) grid.globalRefine(1);
          }

        std::cout << "Results on mesh=" << grid_file << " k=" << degree << std::endl;
        for (int i=0; i<=max_level; i++)
          {
            double rate1=0.0;
            if (i>0) rate1=log(l2[i]/l2[i-1])/log(0.5);
            double rate2=0.0;
            if (i>0) rate2=log(h1s[i]/h1s[i-1])/log(0.5);
            std::cout << std::setw(12) << N[i]
                      << std::setw(12) << std::setprecision(4) << std::scientific << l2[i]
                      << std::setw(12) << std::setprecision(4) << std::scientific << rate1
                      << std::setw(12) << std::setprecision(4) << std::scientific << h1s[i]
                      << std::setw(12) << std::setprecision(4) << std::scientific << rate2
                      << std::endl;
          }
      }
#endif
  }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
