// -*- tab-width: 2; indent-tabs-mode: nil -*-
// vi: set et ts=2 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/common/parallel/mpihelper.hh> // include mpi helper class
#include<dune/common/parametertreeparser.hh>
#include<dune/common/filledarray.hh>
#include <dune/grid/onedgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include<dune/grid/yaspgrid.hh>
#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif
#include<dune/grid/io/file/gmshreader.hh>
#include<dune/geometry/type.hh>
#include<dune/geometry/quadraturerules.hh>
#include<dune/istl/bvector.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/io.hh>
#include<dune/istl/paamg/amg.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/common/functionutilities.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/backend/istl/bcrsmatrixbackend.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/stationary/linearproblem.hh>
#include<dune/pdelab/finiteelementmap/pkfem.hh>
#include<dune/pdelab/finiteelementmap/qkfem.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>

#include "../../dune/npde/utilities/functionutilities.hh"
#include "../../dune/npde/utilities/meshsize.hh"

#include "problem.hh"


//! solve the diffusion problem on a given GridView with a given finite element space
template<class GV, class FEM, class CON>
void driver (const GV& gv, FEM& fem, std::string filename_base, int& N, double& l2e, Dune::ParameterTree& configuration)
{
  // coordinate and result type
  //typedef typename GV::Grid::ctype Coord; // type the grid uses for representing coordinates
  typedef double Real;                    // type to be used for degrees of freedom, parameters, etc.

  // make grid function space
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE; // basis on reference element
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS; // collect local to global map
  GFS gfs(gv,fem);
  gfs.update();
  N = gfs.globalSize();

  // make problem parameters
  typedef ConvectionDiffusionProblem<GV,double> Problem; // parameters for finite element method
  Problem problem(configuration);

  // make a std::vector of degree of freedom vectors and initialize it with Dirichlet extension
  using U = Dune::PDELab::Backend::Vector<GFS,Real>;                             // type for vectors of degrees of freedom
  U u(gfs,0.0);                                                                  // the solution vector u representing u_h
  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<Problem> G; // the extension function type
  G g(gv,problem);                                                               // the extension function u_g
  Dune::PDELab::interpolate(g,gfs,u);                                            // set coefficients such that u_h interpolates u_g

  // make constraints container and initialize it
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;                   // container type for storing indices of Dirichlet DOFs
  CC cc;                                                                                // an empty container
  Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<Problem> bctype(gv,problem);// the boundary condition function
  Dune::PDELab::constraints(bctype,gfs,cc);                                             // detect indices of constrained dofs
  Dune::PDELab::set_nonconstrained_dofs(cc,0.0,u);                                      // set non-constrained DOFs to zero
  std::cout << "constrained dofs=" << cc.size() << " of " << gfs.globalSize() << std::endl;

  // make local operator
  typedef Dune::PDELab::ConvectionDiffusionFEM<Problem,FEM> LOP;               // local operator type for the conforming finite element method
  LOP lop(problem);                                                            // local operator, implementation in dune/pdelab/localoperator/convectiondiffusionfem.hh
  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;                        // Backend for storing sparse matrices
  MBE mbe(5);
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO; // the assembler type
  GO go(gfs,cc,gfs,cc,lop,mbe);                                                    // the assembler (is able to assemble jacobian, residuum, ...)

  // make linear solver and solve problem
  typedef Dune::PDELab::ISTLBackend_SEQ_CG_SSOR LS;
  LS ls;
  typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
  SLP slp(go,ls,u,1e-12);
  slp.apply();                                               // solve problem

  // compute errors
  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;  // type for discrete function on grid
  DGF udgf(gfs,u);                                        // discrete grid function (DOFs from vector u)

  typedef DifferenceSquaredAdapter<G,DGF> DifferenceSquared;               // type for difference of 2 DGF (analytical solution u and numerical solution u_h)
  DifferenceSquared differencesquared(g,udgf);                             // solution difference discrete grid function
  typename DifferenceSquared::Traits::RangeType l2errorsquared(0.0);       // L2 error
  Dune::PDELab::integrateGridFunction(differencesquared,l2errorsquared,10);// compute L2 error squared
  l2e = sqrt(l2errorsquared);                                              // (u-u_h) L2 error


  typedef DifferenceAdapter<G,DGF> Difference;                              // type for difference of 2 DGF (analytical solution u and numerical solution u_h)
  Difference difference(g,udgf);                                            // solution difference discrete grid function


  // write vtk file
  //Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,Dune::refinementLevels(6));
  Dune::VTKWriter<GV> vtkwriter(gv);                                                                 // a VTK writer object
  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF> >(udgf,"u_h"));                // register discrete solution u_h for graphics output
  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<G> >(g,"u"));                       // register discrete solution u for graphics output
  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<Difference> >(difference,"u-u_h")); // register discrete function u-u_h for graphics output
  vtkwriter.write(filename_base,Dune::VTK::appendedraw);                                   // now write Paraview/VTK file
}



int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // start try/catch block to get error messages from dune
  try {

    // read parameters from file
    Dune::ParameterTree configuration;
    Dune::ParameterTreeParser parser;
    parser.readINITree("uebung10.ini", configuration);

    std::string grid_file = configuration.get<std::string>("grid.filename");
    const int max_level = configuration.get<int>("grid.max_level");
    const int degree = configuration.get<int>("method.degree");

    std::vector<double> l2(max_level+1,0.0);
    std::vector<int> N(max_level+1,0);
    std::vector<double> h(max_level+1,0.0);

    // YaspGrid/Qk2d version
    if (grid_file=="yasp")
    {
      // make YaspGrid
      Dune::FieldVector<double,2> LowerLeft(-1.0);
      Dune::FieldVector<double,2> UpperRight(1.0);
      std::array<int,2> S(Dune::filledArray<2,int>(2));
      std::bitset<2> B(false);
      typedef Dune::YaspGrid<2,Dune::EquidistantOffsetCoordinates<double,2>> GridType;
      GridType grid(LowerLeft,UpperRight,S,B,0);

      for (int i=0; i<=max_level; i++)
      {
        typedef GridType::LeafGridView GV;
        GV gv=grid.leafGridView();
        h[i] = meshsize(gv);

        // make finite element space and solve
        typedef Dune::PDELab::ConformingDirichletConstraints CON;
        if (degree==1) {
          typedef Dune::PDELab::QkLocalFiniteElementMap<GV,GridType::ctype,double,1> FEM;
          FEM fem(gv);
          std::string filename = "square_yaspgrid_l" + std::to_string(i) + "_k" + std::to_string(degree);
          driver<GV,FEM,CON>(gv,fem,filename,N[i],l2[i],configuration);
        }
        if (degree==2) {
          typedef Dune::PDELab::QkLocalFiniteElementMap<GV,GridType::ctype,double,2> FEM;
          FEM fem(gv);
          std::string filename = "square_yaspgrid_l" + std::to_string(i) + "_k" + std::to_string(degree);
          driver<GV,FEM,CON>(gv,fem,filename,N[i],l2[i],configuration);
        }
        if (i<max_level) grid.globalRefine(1);
      }
    }

    #if HAVE_UG
    // UGGrid/Pk2d version
    if (grid_file!="yasp")
    {
      // make uggrid
      typedef Dune::UGGrid<2> GridType;
      GridType grid;
      // read gmsh file
      Dune::GridFactory<GridType> factory(&grid);
      Dune::GmshReader<GridType>::read(factory,grid_file,true,true);
      factory.createGrid();

      for (int i=0; i<=max_level; i++)
      {
        typedef GridType::LeafGridView GV;
        GV gv=grid.leafGridView();
        h[i] = meshsize(gv);

        // make finite element space and solve
        typedef Dune::PDELab::ConformingDirichletConstraints CON;
        if (degree==1) {
          typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GridType::ctype,double,1> FEM;
          FEM fem(gv);
          std::string filename = "square_" + grid_file + "_l" + std::to_string(i) + "_k" + std::to_string(degree);
          driver<GV,FEM,CON>(gv,fem,filename,N[i],l2[i],configuration);
        }
        if (degree==2) {
          typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GridType::ctype,double,2> FEM;
          FEM fem(gv);
          std::string filename = "square_" + grid_file + "_l" + std::to_string(i) + "_k" + std::to_string(degree);
          driver<GV,FEM,CON>(gv,fem,filename,N[i],l2[i],configuration);
        }

        if (i<max_level) grid.globalRefine(1);
      }
    }
    #endif

    std::cout << "Results on mesh=" << grid_file << " k=" << degree << std::endl;
    std::cout << std::setw(12) << "N"
      << std::setw(12) << "1/h"
      << std::setw(12) << "l2"
      << std::setw(12) << "l2rate" << std::endl;

    for (int i=0; i<=max_level; i++)
    {
      double rate1=0.0;
      if (i>0) rate1=log(l2[i]/l2[i-1])/log(0.5);
      std::cout << std::setw(12) << N[i]
        << std::setw(12) << std::setprecision(4) << std::scientific << 1.0/h[i]
        << std::setw(12) << std::setprecision(4) << std::scientific << l2[i]
        << std::setw(12) << std::setprecision(4) << std::scientific << rate1
        << std::endl;
    }
  }

  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
