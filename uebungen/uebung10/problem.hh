 /** Parameter class for the stationary convection-diffusion equation of the following form:
  *
  * \f{align*}{
  *   \nabla\cdot(-A(x) \nabla u + b(x) u) + c(x)u &=& f \mbox{ in } \Omega,  \ \
  *                                              u &=& g \mbox{ on } \partial\Omega_D (Dirichlet)\ \
  *                (b(x,u) - A(x)\nabla u) \cdot n &=& j \mbox{ on } \partial\Omega_N (Neumann) \
  *                        -(A(x)\nabla u) \cdot n &=& o \mbox{ on } \partial\Omega_O (Outflow)
  * \f}
  *
  * The template parameters are:
  *  - GV a model of a GridView
  *  - RF numeric type to represent results
  */

 template<typename GV, typename RF>
 class ConvectionDiffusionProblem
 {
   typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

   const double pi = std::acos(-1);


   // Our parameters
   double k1, k2;


 public:
   ConvectionDiffusionProblem(Dune::ParameterTree& configuration)
   {
     // TODO: Read the k1 and k2 values from 'configuration' instead of having them hard-coded.
     // See main function for examples on how to read values from a Dune::ParameterTree.

     k1 = 1.0;
     k2 = 100.0;
   }

   typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

   // Return false if you want more than one evaluation of per cell.
   static constexpr bool permeabilityIsConstantPerCell()
   {
     return true;
   }

   //! tensor diffusion coefficient
   typename Traits::PermTensorType
   A (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
   {
     typename Traits::DomainType x = e.geometry().global(xlocal);

     // TODO: Modify the matrix A(x) - currently it's just the identity - such that it matches the problem given in the exercise.
     // See the comments at the top for how A(x) is defined.

     typename Traits::PermTensorType A;
     for (std::size_t i=0; i<Traits::dimDomain; i++) {
       for (std::size_t j=0; j<Traits::dimDomain; j++) {
         if (i == j)
           A[i][j] = 1.0;
         else
          A[i][j] = 0.0;
       }
     }
     return A;
   }

   //! velocity field
   typename Traits::RangeType
   b (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
   {
     typename Traits::RangeType v(0.0);
     return v;
   }

   //! sink term
   typename Traits::RangeFieldType
   c (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
   {
     return 0.0;
   }

   //! source term
   typename Traits::RangeFieldType
   f (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
   {
     typename Traits::DomainType x = e.geometry().global(xlocal);
     return 0.0;
   }

   //! boundary condition type function
   BCType bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& xlocal) const
   {
     typename Traits::DomainType x = is.geometry().global(xlocal);

     return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
   }

   //! Dirichlet boundary condition value
   typename Traits::RangeFieldType
   g (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
   {
     typename Traits::DomainType x = e.geometry().global(xlocal);

     // Exploit symmetry
     double factor = 1.0;
     if (x[1] < 0.0) {
       factor = -1.0;
       x[0] *= -1.0;
       x[1] *= -1.0;
     }

     // Compute polar coordinates
     double r = x.two_norm();
     double theta = std::atan2(x[1],x[0]);

     // Constants

     // TODO: Compute the constants as given in the script.
     // Trigonometric functions are provided by c++, look them up if necessary (e.g. std::sin).
     double alpha = 0.0;

     double b2 = 0.0;
     double a2 = 0.0;
     double b1 = 0.0;
     double a1 = 0.0;



     // Compute result picking the appropriate constants
     double a = 0.0;
     double b = 0.0;

     if (x[0] >= 0) {
       a = a1;
       b = b1;
     } else {
       a = a2;
       b = b2;
     }

     return factor * std::pow(r, alpha) * (a * std::sin(alpha * theta) + b * std::cos(alpha * theta));
   }

   //! Neumann boundary condition
   typename Traits::RangeFieldType
   j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& xlocal) const
   {
     return 0.0;
   }

   //! Outflow boundary condition
   typename Traits::RangeFieldType
   o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& xlocal) const
   {
     return 0.0;
   }
 };
