#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include<dune/common/parallel/mpihelper.hh> // include mpi helper class
#include<dune/common/parametertreeparser.hh>
#include<dune/grid/onedgrid.hh>
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#if HAVE_UG
#include<dune/grid/uggrid.hh>
#endif
#include<dune/grid/io/file/gmshreader.hh>
#include<dune/istl/bvector.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/io.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/functionutilities.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/backend/istl/bcrsmatrixbackend.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/genericdatahandle.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/constraints/common/constraints.hh>
#include<dune/pdelab/stationary/linearproblem.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/finiteelementmap/pkfem.hh>
#include<dune/pdelab/constraints/conforming.hh>

#include "../../dune/npde/utilities/functionutilities.hh"
#include "laplacefem.hh"
#include "boundary_conditions.hh"

/**
   solve the diffusion problem on a given GridView with a given finite element space

   \tparam GV The grid view in use
   \tparam FEM The finite element map
   \tparam CON The constraints type
   \tparam DBF The boundary function with the Dirichlet values
   \tparam BF The boundary type function, indicating the type of boundary condition
*/
template<class GV, class FEM, class CON, class DBF, class BF>
void driver (const GV& gv, FEM& fem, DBF& dbf, BF & bf, std::string filename_base)
{
  // result type
  typedef double Real;

  // make grid function space
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem);

  // make constraints container and initialize it
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;
  Dune::PDELab::constraints(bf,gfs,cc);
  std::cout << "constrained dofs=" << cc.size() << " of " << gfs.globalSize() << std::endl;

  // make local operator
  typedef LaplaceFEM<BF> LOP;
  LOP lop(bf);
  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
  MBE mbe(5);
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;
  GO go(gfs,cc,gfs,cc,lop,mbe);

  // make FE function extending Dirichlet boundary conditions
  typedef typename GO::Traits::Domain U;
  U u(gfs,0.0);
  Dune::PDELab::interpolate(dbf,gfs,u);

  // make linear solver and solve problem
  typedef Dune::PDELab::ISTLBackend_SEQ_CG_SSOR LS;
  LS ls;
  typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
  SLP slp(go,ls,u,1e-8);
  slp.apply();

  // define discrete grid function
  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
  DGF udgf(gfs,u);

  // compute l2 error
  typedef DifferenceSquaredAdapter<DBF,DGF> DifferenceSquared;
  DifferenceSquared differencesquared(dbf,udgf);
  typename DifferenceSquared::Traits::RangeType l2errorsquared(0.0);
  Dune::PDELab::integrateGridFunction(differencesquared,l2errorsquared,10);
  std::cout << std::setw(12) << std::setprecision(4) << std::scientific
            << "L2 error: " <<  sqrt(l2errorsquared) << std::endl;

  // write vtk file
  Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,Dune::refinementLevels(2));
  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF> >(udgf,"u"));
  vtkwriter.write(filename_base,Dune::VTK::appendedraw);

}

int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // start try/catch block to get error messages from dune
  try {

    // read parameters from file
    Dune::ParameterTree configuration;
    Dune::ParameterTreeParser parser;
    parser.readINITree("uebung07.ini", configuration);
    // UGGrid/Pk2d version
#if HAVE_UG
    if (true)
      {
	       // make uggrid
        const int dim = 2;
        typedef Dune::UGGrid<dim> GridType;
        GridType grid;
        typedef std::vector<int> GmshIndexMap;
        GmshIndexMap boundary_index_map;
        GmshIndexMap element_index_map;
        std::string grid_file = configuration.get<std::string>("grid.filename");
        // read gmsh file
        Dune::GridFactory<GridType> factory(&grid);
        Dune::GmshReader<GridType>::read(factory,grid_file,true,true);
        factory.createGrid();


        const int max_level = configuration.get<int>("grid.max_level");
        for (int i=0; i<max_level; i++) grid.globalRefine(1);
        typedef GridType::LeafGridView GV;
        GV gv=grid.leafGridView();

        typedef double RF;

        // make finite element space and solve
        typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GridType::ctype,RF,1> FEM;
        FEM fem(gv);
        typedef Dune::PDELab::ConformingDirichletConstraints CON;

        // boundary conditions (pure Dirichlet)
        typedef AllDirichletBCFunction<GV,LaplaceFEMBoundaryConditions> BF;
        BF bf(gv);

        // dirichlet values boundary function
        typedef FundamentalSolutionFunction<GV,double,dim> DBF;
        const Dune::FieldVector<double,dim> center(-0.5);
        DBF dbf(gv,center);

        const std::string filename_base = "laplace_uggrid2d";
        driver<GV,FEM,CON,DBF,BF>(gv,fem,dbf,bf,filename_base);
      }
#endif
  }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
