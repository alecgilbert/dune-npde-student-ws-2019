#ifndef BOUNDARY_CONDITIONS_HH
#define BOUNDARY_CONDITIONS_HH

/*!  A boundary condition type function which produces a pure
  Dirichlet problem */
template<class GV, class BC>
class AllDirichletBCFunction
  : public Dune::PDELab::BoundaryGridFunctionBase<
  Dune::PDELab::BoundaryGridFunctionTraits<
    GV,int,1,Dune::FieldVector<int,1> >,AllDirichletBCFunction<GV,BC> >
{
  GV gv;

public:
  typedef Dune::PDELab::BoundaryGridFunctionTraits<GV,int,1,Dune::FieldVector<int,1> > Traits;
  typedef Dune::PDELab::BoundaryGridFunctionBase<Traits,AllDirichletBCFunction<GV,BC> > BaseT;

  AllDirichletBCFunction (const GV & gv_) : gv(gv_) {}

  template<typename I>
  inline void evaluate (const Dune::PDELab::IntersectionGeometry<I>& ig,
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    // Return only Dirichlet marker value
    y = BC::Dirichlet;
  }

  //! get a reference to the GridView
  inline const GV & getGridView () const
  {
    return gv;
  }
};

/**
   This class implements the fundamental solution of the Laplace
   problem for two and three dimensions.
 */
template<typename GV, typename RF, int dim>
class FundamentalSolutionFunction
  : public Dune::PDELab::GridFunctionBase<
  Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, FundamentalSolutionFunction<GV,RF,dim> >
{};

template<typename GV, typename RF>
class FundamentalSolutionFunction<GV,RF,2>
  : public Dune::PDELab::GridFunctionBase<
  Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, FundamentalSolutionFunction<GV,RF,2> >
{
public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

private:
  GV gv;
  typedef typename Traits::DomainType DomainType;
  DomainType center;

public:
  //! construct from grid view
  FundamentalSolutionFunction (const GV& gv_, const DomainType & center_)
    : gv(gv_), center(center_)
  {}

  //! evaluate extended function on element
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    typedef typename GV::Grid::template Codim<0>::Geometry::GlobalCoordinate Point;
    Point x = e.geometry().global(xlocal);
    x -= center;
    y = std::sqrt(x*x);
    y = std::log(y);
  }

  //! get a reference to the grid view
  inline const GV& getGridView () const
  {return gv;}
};

template<typename GV, typename RF>
class FundamentalSolutionFunction<GV,RF,3>
  : public Dune::PDELab::GridFunctionBase<
  Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, FundamentalSolutionFunction<GV,RF,3> >
{
public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

private:
  GV gv;
  typedef typename Traits::DomainType DomainType;
  DomainType center;

public:
  //! construct from grid view
  FundamentalSolutionFunction (const GV& gv_, const DomainType & center_)
    : gv(gv_), center(center_)
  {}

  //! evaluate extended function on element
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    typedef typename GV::Grid::template Codim<0>::Geometry::GlobalCoordinate Point;
    Point x = e.geometry().global(xlocal);
    x -= center;
    y = - 1.0 / std::sqrt(x*x);
  }

  //! get a reference to the grid view
  inline const GV& getGridView () const
  {return gv;}
};

#endif
