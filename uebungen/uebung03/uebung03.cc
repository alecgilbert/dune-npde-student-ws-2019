// -*- Tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/common/parallel/mpihelper.hh> // include mpi helper class
#include <dune/common/parametertreeparser.hh>
#include<dune/grid/onedgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif
#include <dune/grid/io/file/gmshreader.hh>
#include <sstream>

#include "functors.hh"
#include "integrateentity.hh"

//! Write the VTK file.
template<class GridView, class ReferenceFunctor, class ApproxFunctor>
void writeVTKFile(const GridView & gridview, const ReferenceFunctor & rf,
                  const ApproxFunctor & af, std::string filename)
{
  Dune::SubsamplingVTKWriter<GridView> vtkwriter(gridview,Dune::refinementLevels(0));
  typedef FunctorVTKFunction<GridView,ApproxFunctor> VTKApproxFunctor;
  auto vtk_af = std::make_shared<VTKApproxFunctor>(af,"Approximation");
  typedef FunctorVTKFunction<GridView,ReferenceFunctor> VTKF;
  auto vtk_rf = std::make_shared<VTKF>(rf,"Reference");

  vtkwriter.addVertexData(vtk_af);
  vtkwriter.addVertexData(vtk_rf);
  vtkwriter.write(filename, Dune::VTK::appendedraw);

  std::cout << "Wrote VTK file" << std::endl;
}

//! uniform refinement test
template<class Grid, class Functor>
double uniformintegration (Grid& grid, const Functor & f, const int qorder,
                           const int max_level, const double accuracy)
{

  // loop over grid sequence
  double oldvalue = 0.0;
  for (int k = 0; k < max_level; k++)
  {
    auto gridView = grid.levelGridView(k);

    // compute integral with some order
    const double value = integrategrid(gridView,f,qorder);

    const double diff = std::abs(value - oldvalue);
    // print result and error estimate
    std::cout << "elements="
              << std::setw(8) << std::right
              << gridView.size(0)
              << " integral="
              << std::scientific << std::setprecision(12)
              << value
              << " difference=" << diff
              << std::endl;

    if(diff < accuracy)
      return value;

    // save value of integral
    oldvalue = value;
  }

  std::cerr << "Could not achieve sufficient quadrature accuracy." << std::endl;
  exit(1);
  return 0;
}

int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // read parameters from file
  Dune::ParameterTree configuration;
  Dune::ParameterTreeParser parser;
  parser.readINITree("uebung03.ini", configuration);

  // start try/catch block to get error messages from dune
  try {

    const int N = configuration.get<int>("grid.elements");
    typedef Dune::OneDGrid Grid;
    Grid grid(N,0,1);

    // function to integrate
    typedef Phi<Grid::ctype, Grid::dimension> F;
    F f;
    const double t0 = configuration.get<double>("setup.t0");
    f.setTime(t0);

    // integrate and compute error with extrapolation
    const int quad_order = configuration.get<int>("integration.order");
    const int max_level = configuration.get<int>("integration.max_level");
    const double accuracy = configuration.get<double>("integration.accuracy");

    // Create grid levels
    for(int l=0; l<max_level; ++l)
      grid.globalRefine(1);

    typedef SinNPiX<Grid::ctype> SinN;
    typedef CosNPiX<Grid::ctype> CosN;

    const int modes = configuration.get<int>("fourier.modes");
    std::vector<double> a_n(modes);
    std::vector<double> b_n(modes);

    for(int n = 0; n < modes; ++n){
      SinN sin_n(n);
      ProductFunctor<SinN,F> sin_n_f(sin_n,f);
      a_n[n] =  2 * uniformintegration(grid, sin_n_f, quad_order, max_level, accuracy);
      std::cout << "Coefficient a_" << n << " : " << a_n[n] << std::endl;

      CosN cos_n(n);
      ProductFunctor<CosN,F> cos_n_f(cos_n,f);
      b_n[n] = 2 * uniformintegration(grid, cos_n_f, quad_order, max_level, accuracy );
      std::cout << "Coefficient b_" << n << " : " << b_n[n] << std::endl;
    }

    typedef FourierSeries<Grid::ctype> FourierSeriesFunctor;
    FourierSeriesFunctor fsf(a_n, b_n);

    writeVTKFile(grid.leafGridView(), f, fsf, "gauss");

  }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
