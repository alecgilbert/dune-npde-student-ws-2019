// -*- Tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:

#ifndef DUNE_INTEGRATE_ENTITY_HH
#define DUNE_INTEGRATE_ENTITY_HH

#include <dune/common/exceptions.hh>
#include <dune/geometry/quadraturerules.hh>

//! compute integral of function over entity with given order
template<class Iterator, class Functor>
double integrateentity (const Iterator& iterator, const Functor& f, int p)
{
  // dimension of the entity
  const int dim = Iterator::Entity::dimension;

  // type used for coordinates in the grid
  typedef typename Iterator::Entity::Geometry::ctype ct;

  // get geometry type
  Dune::GeometryType gt = iterator->type();

  // get quadrature rule of order p
  auto quadrature_rule = Dune::QuadratureRules<ct,dim>::rule(gt,p);

  if (quadrature_rule.order() < p)
    DUNE_THROW(Dune::Exception,"quadrature order not available");

  // compute approximate integral
  double result = 0.0;
  for (auto quad_point : quadrature_rule)
  {
    double fval = f(iterator->geometry().global(quad_point.position()));
    double weight = quad_point.weight();
    double detjac = iterator->geometry().integrationElement(quad_point.position());
    result += fval * weight * detjac;
  }

  return result;
}

//! uniform refinement test
template<class GridView, class Functor>
double integrategrid (GridView& gridView, const Functor & f, const int qorder)
{
  // compute integral with some order
  double value = 0.0;

  // Iterate over entities
  for (auto iterator = gridView.template begin<0>(); iterator != gridView.template end<0>(); ++iterator)
    value += integrateentity(iterator, f, qorder);

  return value;
}
#endif
