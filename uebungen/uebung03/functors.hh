// -*- Tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:

#ifndef __DUNE_GRID_HOWTO_FUNCTORS_HH__
#define __DUNE_GRID_HOWTO_FUNCTORS_HH__

#include <dune/common/fvector.hh>

// A functor representing the fundamental solution of the heat
// transport equation.
template<typename ct, int dim>
class Phi {
public:
  Phi () {midpoint = 0.5; t = 0;}
  double operator() (const Dune::FieldVector<ct,dim>& x) const
  {
    Dune::FieldVector<ct,dim> y(x);
    y -= midpoint;
    const double ft = std::sqrt(4*t);
    double prefactor = 1.0;
    for(int d=0; d<dim; ++d) prefactor *= ft;
    return 1/prefactor*std::exp(-(y*y)/(4*t));
  }
  void setTime(const double t_){t=t_;}
private:
  Dune::FieldVector<ct,dim> midpoint;
  double t;
};

// A functor representing the sinus basis functions of the Fourier
// series
template<typename ct>
class SinNPiX {
public:
  SinNPiX (const int n_) : n(n_) {}
  double operator() (const Dune::FieldVector<ct,1>& x) const
  {
    return std::sin(double(n) * pi * x[0] * 2.0);
  }
private:
  int n;
  double pi = 3.141592653589793238;
};

// A functor representing the cosinus basis functions of the Fourier
// series
template<typename ct>
class CosNPiX {
public:
  CosNPiX (const int n_) : n(n_) {}
  double operator() (const Dune::FieldVector<ct,1>& x) const
  {
    return std::cos(double(n) * pi * x[0] * 2.0);
  }
private:
  int n;
  double pi = 3.141592653589793238;
};

// A functor representing a Fourier series.
template<typename ct>
class FourierSeries{
  typedef SinNPiX<ct> SinN;
  typedef CosNPiX<ct> CosN;

  typedef std::vector<double> Coefficients;

  typedef std::vector<SinN> SinNs;
  typedef std::vector<CosN> CosNs;

protected:
  const Coefficients & a_n;
  const Coefficients & b_n;

  SinNs sin_n;
  CosNs cos_n;

public:

  FourierSeries(const Coefficients & a_n_, const Coefficients & b_n_)
    : a_n(a_n_), b_n(b_n_)
  {
    assert(a_n.size() == b_n.size());
    for(unsigned int i=0; i<a_n.size(); ++i){
      sin_n.push_back(SinN(i));
      cos_n.push_back(CosN(i));
    }
  }

  double operator() (const Dune::FieldVector<ct,1>& x) const
  {
    double value(0);
    value += 0.5 * b_n[0];
    for(unsigned int i=1; i<a_n.size(); ++i){
      value += sin_n[i](x) * a_n[i];
      value += cos_n[i](x) * b_n[i];
    }
    return value;
  }

};


template< typename F1, typename F2>
class ProductFunctor
{
  const F1 & f1;
  const F2 & f2;
public:
  ProductFunctor(const F1 & f1_, const F2 & f2_)
    : f1(f1_), f2(f2_)
  {}

  template<typename Coordinate>
  double operator() (const Coordinate& x) const
  {
    return f1(x)*f2(x);
  }
};


/**
   This function may be used by the VTKWriter object to evaluate the
   functor object at a given local coordinate.
*/
template<class Grid, class Functor>
class FunctorVTKFunction : public Dune::VTKFunction<Grid>
{
private:

  // Some types which are exported by the base class
  typedef Dune::VTKFunction<Grid> Base;
  typedef typename Base::Entity Entity;
  typedef typename Base::ctype ctype;
  enum { dim =  Base::dim };

  // Constant reference to the functor object.
  const Functor & f;
  const std::string data_name;
public:

  FunctorVTKFunction(const Functor & f_, std::string name_) : f(f_), data_name(name_)
  {}

  //! The number of components of this function. This is necessary as
  //! the VTKWriter would also allow the visualization of vector
  //! valued functions.
  int ncomps() const {return 1;}

  //! Evaluate the functor at a given local coordinate
  double evaluate(int comp, const Entity & e, const Dune::FieldVector<ctype,dim> & xi) const
  {
    auto x_global = e.geometry().global(xi);
    return f(x_global);
  }

  //! This name will later be displayed as a label for the visualized
  //! data in paraview.
  std::string name() const
  {
    return data_name;
  }
};


#endif // __DUNE_GRID_HOWTO_FUNCTORS_HH__
