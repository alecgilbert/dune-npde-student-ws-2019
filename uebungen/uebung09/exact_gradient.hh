

//! exact gradient of solution
template<typename GV, typename RF>
class ExactGradient
 : public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<GV,RF,
GV::dimension,Dune::FieldVector<RF,GV::dimension> >,
ExactGradient<GV,RF> >
{
  GV gv;

  double a_ = 2.0;
  double b_ = 2.0;

public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,
  GV::dimension,Dune::FieldVector<RF,GV::dimension> > Traits;
  typedef Dune::PDELab::GridFunctionBase<Traits,ExactGradient<GV,RF> > BaseT;

  ExactGradient (const GV& gv_) : gv(gv_) {}

  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    typename Traits::DomainType x = e.geometry().global(xlocal);
    y[0] = x[1] * (b_-x[1]) * (b_/2.0-x[1]) * ( (a_-x[0]) * (a_/2.0-x[0]) + x[0] * (2*x[0] - 3.0/2.0 * a_) );
    y[1] = x[0] * (a_-x[0]) * (a_/2.0-x[0]) * ( (b_-x[1]) * (b_/2.0-x[1]) + x[1] * (2*x[1] - 3.0/2.0 * b_) );
  }

  inline const typename Traits::GridViewType& getGridView () const
  {
    return gv;
  }

};
