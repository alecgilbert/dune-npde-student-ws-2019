 /** Parameter class for the stationary convection-diffusion equation of the following form:
  *
  * \f{align*}{
  *   \nabla\cdot(-A(x) \nabla u + b(x) u) + c(x)u &=& f \mbox{ in } \Omega,  \ \
  *                                              u &=& g \mbox{ on } \partial\Omega_D (Dirichlet)\ \
  *                (b(x,u) - A(x)\nabla u) \cdot n &=& j \mbox{ on } \partial\Omega_N (Neumann) \
  *                        -(A(x)\nabla u) \cdot n &=& o \mbox{ on } \partial\Omega_O (Outflow)
  * \f}
  *
  * The template parameters are:
  *  - GV a model of a GridView
  *  - RF numeric type to represent results
  */

 template<typename GV, typename RF>
 class ConvectionDiffusionProblem
 {
   typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

   double a_ = 2.0;
   double b_ = 2.0;

 public:
   typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

   //! tensor diffusion constant per cell? return false if you want more than one evaluation of A per cell.
   static constexpr bool permeabilityIsConstantPerCell()
   {
      return true;
   }

   //! tensor diffusion coefficient
   typename Traits::PermTensorType
   A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
   {
     typename Traits::PermTensorType I;
     for (std::size_t i=0; i<Traits::dimDomain; i++)
       for (std::size_t j=0; j<Traits::dimDomain; j++)
         I[i][j] = (i==j) ? 1 : 0;
       return I;
   }

   //! velocity field
   typename Traits::RangeType
   b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
   {
     typename Traits::RangeType v(0.0);
     return v;
   }

   //! sink term
   typename Traits::RangeFieldType
   c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
   {
     return 0.0;
   }

   //! source term
   typename Traits::RangeFieldType
   f (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
   {
     typename Traits::DomainType x = e.geometry().global(xlocal);

     return (3.0*b_/2.0*std::pow(x[1],2) - std::pow(b_,2)/2.0*x[1]-std::pow(x[1],3))*(6*x[0]-3*a_)
          + (3.0*a_/2.0*std::pow(x[0],2) - std::pow(a_,2)/2.0*x[0]-std::pow(x[0],3))*(6*x[1]-3*b_);

     //return 6*(x[0]-1.)*(3.*x[1]*x[1]-2.*x[1]-x[1]*x[1]*x[1])+6*(x[1]-1.)*(3.*x[0]*x[0]-2.*x[0]-x[0]*x[0]*x[0]);
   }

   //! boundary condition type function
   BCType bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
   {
     typename Traits::DomainType xglobal = is.geometry().global(x);

     //return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann;
     return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
   }

   //! Dirichlet boundary condition value
   typename Traits::RangeFieldType
   g (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
   {
     typename Traits::DomainType x = e.geometry().global(xlocal);
     //return x[0]*x[1]*(2.-x[0])*(2.-x[1])*(1.-x[0])*(1.-x[1]); // is the same as the solution, which comes in handy in the L2 error computation
     return x[0]*x[1]*(a_-x[0])*(b_-x[1])*(a_/2-x[0])*(b_/2-x[1]);
   }

   //! Neumann boundary condition
   typename Traits::RangeFieldType
   j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
   {
     return 0.0;
   }

   //! Outflow boundary condition
   typename Traits::RangeFieldType
   o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
   {
     return 0.0;
   }
 };
