template< typename Coordinate >
class ConstGravityForceFunctor
{
  double f_value;

public:
  typedef Coordinate Coord;
  ConstGravityForceFunctor(double f_value_) : f_value(f_value_)
  {}

  Coordinate operator()(const Coordinate & ) const
  {
    Coordinate x(0);
    x[1]=-f_value;
    return x;
  }
};
