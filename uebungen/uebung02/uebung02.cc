#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <dune/common/parallel/mpihelper.hh> // include mpi helper class
#include<dune/common/parametertreeparser.hh>
#include<dune/grid/onedgrid.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/operators.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#if HAVE_UG
#include<dune/grid/uggrid.hh>
#endif
#include<dune/grid/io/file/gmshreader.hh>
#include "functors.hh"
#include "assembler.hh"

//! Write the VTK file.
template<class GridView, class Data>
void writeVTKFile(const GridView & gridview, const Data & d, const int dim, std::string filename)
{
  Dune::VTKWriter<GridView> vtkwriter(gridview);
  vtkwriter.addVertexData(d, "u", dim);
  vtkwriter.write(filename, Dune::VTK::appendedraw);
}

template <typename Matrix, int dim>
void setupSparsityPattern(Matrix & m)
{
  for(auto row = m.createbegin(); row != m.createend(); ++row){
    // Add nonzeros for left neighbour, diagonal and right neighbour
    if(row.index() >= dim)
      row.insert(row.index() - dim);
    row.insert(row.index());
    if(row.index() < m.N() - dim)
      row.insert(row.index() + dim);
  }
}

int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // start try/catch block to get error messages
  try {
    using namespace Dune;

    // read parameters from file
    Dune::ParameterTree configuration;
    Dune::ParameterTreeParser parser;
    parser.readINITree("uebung02.ini", configuration);

    // Get number type from OneDGrid
    typedef Dune::OneDGrid::ctype ctype;

    // Set number of vertices
    const int N = configuration.get<int>("grid.cells");
    const ctype L = configuration.get<int>("springchain.length");

    // Define position intervals in reference domain [0,1].
    typedef std::vector<ctype> Intervals;
    Intervals intervals(N+1);
    for(int i=0; i<N+1; ++i)
      intervals[i] = double(i) / double(N);

    // Construct grid
    Dune::OneDGrid grid(intervals);

    typedef Dune::BlockVector<Dune::FieldVector<ctype,1> > ScalarData;

    // Define initial position of vertices to be the interval position
    // in x-direction and zero in all other directions.
    const int dim = 3;
    ScalarData vertices(dim * (N+1));
    for(int i=0; i<N+1; ++i){
      vertices[i*dim] = intervals[i] * L;
      for(int j=1; j<dim; ++j){
        vertices[i*dim+j] = 0;
      }
    }

    // The length constants
    const ctype contraction_factor = configuration.get<ctype>("springchain.contraction");
    ScalarData lengths(N);
    lengths = L / double(N) / contraction_factor;

    // The string constants
    ScalarData kappa(N);
    const ctype elasticity_module = configuration.get<ctype>("springchain.elasticity_module");
    const ctype cross_section = configuration.get<ctype>("springchain.cross_section");
    for(int i=0; i<N; ++i)
      kappa[i] = cross_section * elasticity_module / lengths[i];


    //////////////////////////////////////////////////////////////////
    // Matrix setup

    // Number of degrees of freedom is equal to the number of interior
    // vertices times the number of components per vertex.
    const int dofs = dim * (N+1);

    // Number of non zeros per matrix row is one for each vertex
    // itself plus the number of neighbor vertices.
    const int nonzeros = 3 * dofs;

    // Construct matrix
    typedef BCRSMatrix< Dune::FieldMatrix<ctype,1,1> > Matrix;
    Matrix A(dofs,dofs,nonzeros,Matrix::row_wise);
    setupSparsityPattern<Matrix,dim>(A);

    //////////////////////////////////////////////////////////////////

    ctype initial_defect(0);
    while(true){

      // Reset the system matrix
      A=0;

      // Assemble the system matrix
      assembleMatrix<Matrix,ScalarData,dim>(A,vertices,kappa,lengths);

      // Construct the right hand side
      ScalarData rhs(vertices.size());

      // Define the external force
      const ctype density = configuration.get<ctype>("springchain.density");
      const ctype force_value = 9.81 * density * cross_section * L / double(N);
      typedef Dune::FieldVector<ctype,dim> Coordinate;
      ConstGravityForceFunctor<Coordinate> f(force_value);

      // Assemble the right hand side
      assembleRhs(rhs,vertices,f,dim);

      // Compute residual and defect
      ScalarData residual(vertices.size());
      A.mv(vertices,residual);
      residual -= rhs;
      const ctype defect = residual.two_norm();
      if(!initial_defect)
        initial_defect = defect;
      std::cout << "Defect: " << defect << std ::endl;

      // Check the breaking condition of non linear iteration
      if(defect < 1e-8 * initial_defect || defect <= 1e-12 * A.infinity_norm()){
        std::cout << "Nonlinear iteration converged!" << std::endl;
        break;
      }

      // Solve linear system
      typedef Dune::MatrixAdapter<Matrix,ScalarData,ScalarData> LinearOperator;
      LinearOperator op(A);
      typedef Dune::SeqILU0<Matrix,ScalarData,ScalarData> Preconditioner;
      Preconditioner prec(A,1.8);

      const ctype reduction = 1e-9;
      const int max_iterations = 5000;
      const int verbose = 3;

      typedef Dune::CGSolver<ScalarData> Solver;
      Solver solver(op,prec,reduction,max_iterations,verbose);
      Dune::InverseOperatorResult result;
      solver.apply(vertices,rhs,result);
    }

    writeVTKFile(grid.leafGridView(),vertices,dim,"string");

  }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
