#ifndef STRING_J_ASSEMBLER_HH
#define STRING_J_ASSEMBLER_HH

/* TODO */
template <typename Rhs, typename ForceFunctor>
void assembleRhs(Rhs & rhs, const Rhs & vertices, const ForceFunctor & f, const int dim)
{
  typedef typename ForceFunctor::Coord Coordinate;
  const int N = vertices.size() / dim - 1;
  // Reset right hand side
  rhs = 0;

  // Initialize right hand side of linear system. Remember that
  // trivial problems have to be set for the first and the last node's
  // coordinates.

}

/* TODO */
// Assemble system matrix of string chain system. Remember that
// trivial problems have to be set for the first and the last node's
// coordinates.
template<typename Matrix, typename ScalarData, int dim>
void assembleMatrix(Matrix & A, const ScalarData & u,
                    const ScalarData & kappa, const ScalarData & lengths)
{

  typedef typename ScalarData::value_type ctype;

  const unsigned int N = u.size() / dim - 1;

  // Iterate over rows
  for(auto row_iter = A.begin(); row_iter != A.end(); ++row_iter) {
    const int row_index = row_iter.index();

    // Iterate over columns
    for(auto col_iter = row_iter->begin(); col_iter != row_iter->end(); ++col_iter){
      const int col_index = col_iter.index();

      // *col_iter = some value
    }
  }
}
#endif
