// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include<dune/common/parallel/mpihelper.hh> // include mpi helper class
#include<dune/common/parametertreeparser.hh>
#include<dune/common/filledarray.hh>
#include<dune/grid/onedgrid.hh>
#include <dune/grid/yaspgrid.hh>
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#if HAVE_UG
#include<dune/grid/uggrid.hh>
#endif
#include<dune/grid/io/file/gmshreader.hh>
#include<dune/istl/bvector.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/io.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/functionutilities.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/backend/istl/bcrsmatrixbackend.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/genericdatahandle.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/constraints/common/constraints.hh>
#include<dune/pdelab/stationary/linearproblem.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/finiteelementmap/pkfem.hh>
#include<dune/pdelab/constraints/conforming.hh>

#include<dune/pdelab/finiteelementmap/qkfem.hh>

#include "convectiondiffusion.hh"
#include "boundary_conditions.hh"
#include "utilities.hh"


//! solve the diffusion problem on a given GridView with a given finite element space
template<class GV, class FEM, class CON, class EF, class CF, class BF, class DBF, class NBF, class OF, class SF>
void driver (const GV& gv, FEM& fem, EF & ef, CF& cf, BF & bf, DBF & dbf, NBF & nbf, OF & of, SF & sf, std::string filename_base)
{
  // coordinate and result type
  //typedef typename GV::Grid::ctype Coord;
  typedef double Real;

  // make grid function space
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem);

  // make constraints container and initialize it
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;
  Dune::PDELab::constraints(bf,gfs,cc);
  std::cout << "constrained dofs=" << cc.size() << " of " << gfs.globalSize() << std::endl;

  // make local operator
  typedef CDFEM<EF,CF,BF,DBF,NBF,OF,SF> LOP;
  LOP lop(ef,cf,bf,dbf,nbf,of, sf);
  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
  MBE mbe(5);
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;
  GO go(gfs,cc,gfs,cc,lop,mbe);

  // make FE function extending Dirichlet boundary conditions
  typedef typename GO::Traits::Domain U;
  U u(gfs,0.0);
  Dune::PDELab::interpolate(dbf,gfs,u);

  typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_AMG_SSOR<GO> LS;      // linear solver type from solver backend
  LS ls(500);
  // typedef Dune::PDELab::ISTLBackend_SEQ_CG_SSOR LS;      // linear solver type from solver backend
  //  LS ls(5000);
  typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
  SLP slp(go,ls,u,1e-8);
  slp.apply();

  // define discrete grid function
  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
  DGF udgf(gfs,u);

  // define discrete grid function for permeability field
  typedef EllipticDGF<EF> EDGF;
  EDGF edgf(ef);

  // write vtk file
  Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,Dune::refinementLevels(2));
  //Dune::VTKWriter<GV> vtkwriter(gv,Dune::VTKOptions::nonconforming);
  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF> >(udgf,"u"));
  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<EDGF> >(edgf,"k"));
  vtkwriter.write(filename_base,Dune::VTK::appendedraw);

  std::cout << "total flux " << flux(gv,gfs,u,ef,1.5) << std::endl;
}

int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // start try/catch block to get error messages from dune
  try {

    // read parameters from file
    Dune::ParameterTree configuration;
    Dune::ParameterTreeParser parser;
    parser.readINITree("uebung11.ini", configuration);

    // Yaspgrid, Q1/Q2 version
    if (true)
    {
      // make YaspGrid 2d
      Dune::FieldVector<double,2> L(2.0);
      std::array<int,2> S(Dune::filledArray<2,int>(2));
      std::bitset<2> B(false);
      typedef Dune::YaspGrid<2> GridType;
      GridType grid(L,S,B,0);

      const int max_level = configuration.get<int>("grid.max_level");
      for (int i=0; i<max_level; i++) grid.globalRefine(1);
      typedef GridType::LeafGridView GV;
      GV gv=grid.leafGridView();

      typedef double RF;

      // make finite element space and solve
      typedef Dune::PDELab::QkLocalFiniteElementMap<GV,GridType::ctype,RF,1> FEM1;
      FEM1 fem1(gv);
      typedef Dune::PDELab::QkLocalFiniteElementMap<GV,GridType::ctype,RF,2> FEM2;
      FEM2 fem2(gv);
      typedef Dune::PDELab::ConformingDirichletConstraints CON;

      // boundary conditions (left-right Dirichlet)
      typedef LRDirichletBCFunction<GV,CDBoundaryConditions> BF;
      BF bf(gv);

      // newton boundary condition (top-bottom zero Neumann)
      typedef ConstScalarFunction<GV,RF> NBF;
      NBF nbf(gv,0.0);

      // Dirichlet boundary function
      typedef DirichletFunction<GV,RF> DBF;
      DBF dbf(gv);

      // Outflow function
      typedef ZeroFunction<GV,RF> OF;
      OF of(gv);

      // source
      typedef ZeroFunction<GV,RF> SF;
      SF sf(gv);

      // convectivity
      typedef ConstVectorFunction<GV,RF> CF;
      CF cf(gv,configuration.get<RF>("convection"),0.);

      // ellipticity
      typedef EllipticityFunction<GV,RF> EF;
      const RF ef_1 = configuration.get<RF>("heterogeneity.k1");
      const RF ef_2 = configuration.get<RF>("heterogeneity.k2");
      EF ef(gv,ef_1,ef_2);

      const std::string filename_base = configuration.get<std::string>("output.filename");
      int degree = configuration.get<int>("degree");
      if (degree ==1)
        driver<GV,FEM1,CON,EF,CF,BF,DBF,NBF,SF>(gv,fem1,ef,cf,bf,dbf,nbf,of,sf,filename_base);
      if (degree ==2)
        driver<GV,FEM2,CON,EF,CF,BF,DBF,NBF,SF>(gv,fem2,ef,cf,bf,dbf,nbf,of,sf,filename_base);
    }

  }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
