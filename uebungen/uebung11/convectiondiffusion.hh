// -*- tab-width: 2; indent-tabs-mode: nil -*-
#ifndef DUNE_NPDE_CONVECTIONDIFFUSIONFEM_HH
#define DUNE_NPDE_CONVECTIONDIFFUSIONFEM_HH

#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/geometry/type.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/quadraturerules.hh>

#include<dune/pdelab/common/geometrywrapper.hh>
#include<dune/pdelab/common/function.hh>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>


/** a local operator for solving convection-diffusion equation with standard FEM
 *
 * \f{align*}{
 *   \nabla\cdot(-k(x) \nabla u + a(x) u)  &=& f \mbox{ in } \Omega,  \\
 *                                       u &=& g \mbox{ on } \partial\Omega_D \\
 *           (a(x) - k(x)\nabla u) \cdot n &=& j \mbox{ on } \partial\Omega_N \\
 *                 -(k(x)\nabla u) \cdot n &=& h \mbox{ on } \partial\Omega_O
 * \f}
 * Note:
 *  - This formulation is valid for velocity fields which are non-divergence free.
 *  - Outflow boundary conditions should only be set on the outflow boundary
 *
 * \tparam T model of ConvectionDiffusionParameterInterface
 */

/** \brief Class to define the boundary condition types
 */
struct CDBoundaryConditions
{
  enum Type { Dirichlet=1, Neumann=-1, Outflow=-2 }; // BC requiring constraints must be >0 if
                                                     // constraints assembler coming with PDELab is used
};

/** a local operator for solving the convection diffusion equation defined above
 *
 * with conforming finite elements on all types of grids in any dimension
 \tparam EF Ellipticity Function
 \tparam B  Convection Function
 \tparam BF Boundary condition type function
 \tparam DF Dirichlet condition function
 \tparam NF Neumann condition function
 \tparam SF Source function
*/
template<typename EF, typename CF, typename BF, typename DBF, typename NBF, typename OF, typename SF>
class CDFEM :
  public Dune::PDELab::NumericalJacobianApplyVolume<CDFEM<EF,CF,BF,DBF,NBF,OF,SF> >,
  public Dune::PDELab::NumericalJacobianApplyBoundary<CDFEM<EF,CF,BF,DBF,NBF,OF,SF> >,
  public Dune::PDELab::NumericalJacobianVolume<CDFEM<EF,CF,BF,DBF,NBF,OF,SF> >,
  public Dune::PDELab::NumericalJacobianBoundary<CDFEM<EF,CF,BF,DBF,NBF,OF,SF> >,
  public Dune::PDELab::FullVolumePattern,
  public Dune::PDELab::LocalOperatorDefaultFlags,
  public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
public:
  // pattern assembly flags
  enum { doPatternVolume = true };

  // residual assembly flags
  enum { doAlphaVolume = true };
  enum { doAlphaBoundary = true };

  CDFEM (const EF & ef_,  const CF &cf_, const BF & bf_, const DBF & dbf_, const NBF & nbf_, const OF & of_, const SF & sf_)
    : ef(ef_), cf(cf_), bf(bf_), dbf(dbf_), nbf(nbf_), of(of_), sf(sf_)
  {}


  // TODO: What integrals are being computed here? Note that volume integrals (over entire elements)
  // as well as boundary integrals (over elements' faces) are being treated separately.

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
    // domain and range field type
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::JacobianType JacobianType;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType RangeType;

    typedef typename LFSU::Traits::SizeType size_type;

    // dimensions
    const int dim = EG::Geometry::mydimension;
    const int dimw = EG::Geometry::coorddimension;

    // select quadrature rule
    Dune::GeometryType gt = eg.geometry().type();
    const int intorder = 2*lfsu.finiteElement().localBasis().order();
    auto quadrature_rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

    // loop over quadrature points
    for (auto quad_point : quadrature_rule)
    {
      // evaluate basis functions
      std::vector<RangeType> phi(lfsu.size());
      lfsu.finiteElement().localBasis().evaluateFunction(quad_point.position(),phi);

      // evaluate u
      RF u=0.0;
      for (size_type i=0; i<lfsu.size(); i++)
        u += x(lfsu,i)*phi[i];

      // evaluate gradient of shape functions (we assume Galerkin method lfsu=lfsv)
      std::vector<JacobianType> js(lfsu.size());
      lfsu.finiteElement().localBasis().evaluateJacobian(quad_point.position(),js);

      // transform gradients of shape functions to real element
      const Dune::FieldMatrix<DF,dimw,dim> jac = eg.geometry().jacobianInverseTransposed(quad_point.position());
      std::vector<Dune::FieldVector<RF,dim> > gradphi(lfsu.size());
      for (size_type i=0; i<lfsu.size(); i++)
        jac.mv(js[i][0],gradphi[i]);

      // compute gradient of u
      Dune::FieldVector<RF,dim> gradu(0.0);
      for (size_type i=0; i<lfsu.size(); i++)
        gradu.axpy(x(lfsu,i),gradphi[i]);

      // evaluate ellipticity
      typename EF::Traits::RangeType ellipticity;
      ef.evaluate(eg.entity(),quad_point.position(),ellipticity);

      // evaluate convection coefficient
      typename CF::Traits::RangeType convectivity;
      cf.evaluate(eg.entity(),quad_point.position(),convectivity);

      // evaluate source
      typename SF::Traits::RangeType source;
      sf.evaluate(eg.entity(),quad_point.position(),source);



      // integrate grad u*grad phi_i
      RF factor = quad_point.weight() * eg.geometry().integrationElement(quad_point.position());
      for (size_type i=0; i<lfsu.size(); i++)
        r.accumulate(lfsu,i,(ellipticity * ( gradu*gradphi[i] ) - u*(convectivity*gradphi[i])+ source * phi[i])*factor);

    }
  }

  // boundary integral
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_boundary (const IG& ig,
                       const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                       R& r_s) const
  {
    // domain and range field type
    typedef typename LFSV::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSV::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSV::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType RangeType;

    typedef typename LFSV::Traits::SizeType size_type;

    // dimensions
    const int dim = IG::Entity::dimension;

    // evaluate boundary condition type
    Dune::GeometryType gtface = ig.geometryInInside().type();
    Dune::FieldVector<DF,dim-1> facecenterlocal
      = Dune::ReferenceElements<DF,dim-1>::general(gtface).position(0,0);

    CDBoundaryConditions::Type bctype;
    typename BF::Traits::RangeType bf_value;
    bf.evaluate(ig,facecenterlocal,bf_value);
    bctype = CDBoundaryConditions::Type(bf_value[0]);

    // skip rest if we are on Dirichlet boundary
    if (bctype==CDBoundaryConditions::Dirichlet) return;

    // select quadrature rule
    const int intorder = 2*lfsu_s.finiteElement().localBasis().order();
    auto quadrature_rule = Dune::QuadratureRules<DF,dim-1>::rule(gtface,intorder);

    // loop over quadrature points and integrate normal flux
    for (auto quad_point : quadrature_rule)
    {
      // position of quadrature point in local coordinates of element
      Dune::FieldVector<DF,dim> local = ig.geometryInInside().global(quad_point.position());

      // evaluate shape functions (assume Galerkin method)
      std::vector<RangeType> phi(lfsu_s.size());
      lfsu_s.finiteElement().localBasis().evaluateFunction(local,phi);

      if (bctype==CDBoundaryConditions::Neumann)
      {
        // evaluate flux boundary condition
        typename NBF::Traits::RangeType j;
        nbf.evaluate(ig.inside(),local,j);

        // integrate j
        RF factor = quad_point.weight()*ig.geometry().integrationElement(quad_point.position());
        for (size_type i=0; i<lfsu_s.size(); i++)
          r_s.accumulate(lfsu_s,i,(j*phi[i]*factor ));
      }

      if (bctype==CDBoundaryConditions::Outflow)
      {
        // evaluate u
        RF u=0.0;
        for (size_type i=0; i<lfsu_s.size(); i++)
          u += x_s(lfsu_s,i)*phi[i];

        // evaluate flux boundary condition
        typename CF::Traits::RangeType convectivity;
        cf.evaluate(ig.inside(),local,convectivity);

        // evaluate flux boundary condition
        typename OF::Traits::RangeType outflow;
        of.evaluate(ig.inside(),local,outflow);
        // evaluate velocity field and outer unit normal
        Dune::FieldVector<RF,dim> convectivity_vec(1.);

        const Dune::FieldVector<DF,dim> n = ig.unitOuterNormal(quad_point.position());

        // integrate o
        RF factor = quad_point.weight()*ig.geometry().integrationElement(quad_point.position());
        for (size_type i=0; i<lfsu_s.size(); i++)
          r_s.accumulate(lfsu_s,i,( (convectivity*n)*u+outflow)*phi[i]*factor);
      }

    }


  }

private:
  int intorder;
  const EF & ef; // ellipticity function
  const CF & cf; // convection function
  const BF & bf; // boundary condition type function
  const DBF & dbf; // Dirichlet condition function
  const NBF & nbf; // Neumann condition function
  const OF & of; // Outflow boundary condition
  const SF & sf; // Source function
};



#endif
