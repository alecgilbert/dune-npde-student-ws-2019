// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#include <dune/pdelab/common/elementmapper.hh>

template<typename GV, typename RF>
class EllipticityFunction
  : public Dune::PDELab::GridFunctionBase<
  Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, EllipticityFunction<GV,RF> >
{
public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

private:
  GV gv;
  typedef typename Traits::DomainType DomainType;
  RF k1;
  RF k2;


public:

  //! construct from grid view
  EllipticityFunction (const GV& gv_,
                       const RF & k1_, const RF & k2_)
      : gv(gv_), k1(k1_), k2(k2_)
  {

  }

  //! evaluate extended function on element
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    auto x = e.geometry().global(xlocal);

    // TODO: Here we define the permeability field. Adjust it appropriately.

    DomainType center;
    center[0]=1.;
    center[1]=1.;
    x -= center;

    if(x[0] >= 0 && x[1] >= 0)
        y = k1;
    else if(x[0] < 0 && x[1] >= 0)
        y = k2;
    else if(x[0] < 0 && x[1] < 0)
        y = k1;
    else
        y = k2;
  }

  //! get a reference to the grid view
  inline const GV& getGridView () const
  {return gv;}
};


// discrete grid function for permeability fied visualisation
template<typename EF>
class EllipticDGF
    : public Dune::PDELab::GridFunctionBase<
    Dune::PDELab::GridFunctionTraits<typename EF::Traits::GridViewType,
                                     typename EF::Traits::RangeFieldType,
                                     EF::Traits::dimRange,
                                     typename EF::Traits::RangeType>,
    EllipticDGF<EF> >
{
  const EF& ef;

public:
  typedef Dune::PDELab::GridFunctionTraits<typename EF::Traits::GridViewType,
                                            typename EF::Traits::RangeFieldType,
                                            EF::Traits::dimRange,
                                            typename EF::Traits::RangeType> Traits;

  EllipticDGF ( const EF& ef_) : ef(ef_) {}

  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    typename EF::Traits::RangeType k_value;
    ef.evaluate(e,x,k_value);
    y = k_value;
  }

  inline const typename Traits::GridViewType& getGridView ()
  {
    return ef.getGridView();
  }
};

// compute flux through boundary
template<typename GV, typename GFS, typename X, typename EF, typename R>
double flux (const GV& gv, const GFS& gfs, const X& x, const EF& ef, R pos, int intorder=4)
{
  // first we extract the dimensions of the grid
  const int dim = GV::dimension;
  typedef typename Dune::PDELab::LocalFunctionSpace<GFS> LFS;
  typedef Dune::PDELab::LFSIndexCache<LFS> LFSCache;

  typedef typename LFS::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
  typedef typename LFS::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
  typedef typename LFS::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::JacobianType JacobianType;
  typedef typename GV::ctype ct;
  typedef typename GV::template Codim<0>::Iterator ElementLeafIterator;
  typedef typename GV::Traits::template Codim<0>::Entity Element;

  typedef typename GV::IntersectionIterator IntersectionIterator;

  // Map each cell to unique id
  Dune::PDELab::ElementMapper<GV> cell_mapper(gfs.gridView());

  LFS lfs(gfs); // local function spacel
  LFSCache lfs_cache(lfs);
  typename X::template ConstLocalView<LFSCache> x_view(x);

  RF flux_sum=0.0;
  const RF eps=1e-6;

  for (auto it = gv.template begin<0>(); it!=gv.template end<0>(); ++it)
  {
    // Compute unique id
    const typename GV::IndexSet::IndexType ids = cell_mapper.map(*it);

    Dune::PDELab::ElementGeometry<Element> eg(*it);

    // run through all intersections with neighbors and boundary
    for (auto is = gv.ibegin(*it); is != gv.iend(*it); ++is)
    {
      // get geometry type of face
      Dune::GeometryType gtf = is->type();

      const typename GV::IndexSet::IndexType idn = cell_mapper.map(is->outside());

      // center in face's reference element
      const Dune::FieldVector<ct,dim-1>& facelocal = Dune::ReferenceElements<ct,dim-1>::general(gtf).position(0,0);

      // get normal vector scaled with volume
      Dune::FieldVector<ct,dim> integrationOuterNormal = is->integrationOuterNormal(facelocal);
      integrationOuterNormal *= Dune::ReferenceElements<ct,dim-1>::general(gtf).volume();

      // center of face in global coordinates
      Dune::FieldVector<ct,dim> faceglobal = is->geometry().global(facelocal);

      // go throuth the face with x-coordinate pos, just elements on the left from this face
      if ( (faceglobal[0]-eps)<pos && (faceglobal[0]+eps)>pos && ids < idn)
      {
        // loop over quadrature points
        auto quadrature_rule = Dune::QuadratureRules<DF,dim-1>::rule(gtf,intorder);
        for (auto quad_point : quadrature_rule)
        {

          // position of quadrature point in local coordinates of element
          Dune::FieldVector<DF,dim> local = is->geometryInInside().global(quad_point.position());

          lfs.bind(*it);
          lfs_cache.update(); // write local container into x_view
          std::vector<RF> xl(lfs.size()); //local vector
          x_view.bind(lfs_cache);
          x_view.read(xl);
          x_view.unbind();

          // evaluate ellipticity
          typename EF::Traits::RangeType ellipticity(0.);
          ef.evaluate(eg.entity(),local,ellipticity);

          // evaluate jacobian
          std::vector<JacobianType> js(lfs.size());
          lfs.finiteElement().localBasis().evaluateJacobian(local,js);

          // transform gradient to real element
          const Dune::FieldMatrix<DF,dim,dim> jac = it->geometry().jacobianInverseTransposed(local);
          std::vector<Dune::FieldVector<RF,dim> > gradphi(lfs.size());
          for (size_t i=0; i<lfs.size(); i++) jac.mv(js[i][0],gradphi[i]);

          // compute gradient of u
          Dune::FieldVector<RF,dim> gradu(0.0);
          for (size_t i=0; i<lfs.size(); i++) gradu.axpy(xl[i],gradphi[i]);

          RF factor = quad_point.weight();
          flux_sum -= ellipticity*(integrationOuterNormal*gradu)*factor;
        } // quadrature

      } // border

    } // is
  }  //it
  return flux_sum;
}
