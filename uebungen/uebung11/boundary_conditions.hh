#ifndef BOUNDARY_CONDITIONS_HH
#define BOUNDARY_CONDITIONS_HH


/*!  A boundary condition type function which produces a mixed
  problem.*/
template<class GV, class BC>
class LRDirichletBCFunction
  : public Dune::PDELab::BoundaryGridFunctionBase<
  Dune::PDELab::BoundaryGridFunctionTraits<
    GV,int,1,Dune::FieldVector<int,1> >,LRDirichletBCFunction<GV,BC> >
{
  GV gv;

public:
  typedef Dune::PDELab::BoundaryGridFunctionTraits<GV,int,1,Dune::FieldVector<int,1> > Traits;
  typedef Dune::PDELab::BoundaryGridFunctionBase<Traits,LRDirichletBCFunction<GV,BC> > BaseT;

  LRDirichletBCFunction (const GV & gv_) : gv(gv_) {}

  template<typename I>
  inline void evaluate (const Dune::PDELab::IntersectionGeometry<I>& ig,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    typedef typename GV::Grid::template Codim<0>::Geometry::GlobalCoordinate Point;
    Point x = ig.geometry().global(xlocal);

    // Return only Dirichlet marker value
    y = BC::Neumann;
    if(x[0]<1e-6 || x[0] > 2.0 - 1e-6)
      y = BC::Dirichlet;
  }

  //! get a reference to the GridView
  inline const GV & getGridView () const
  {
    return gv;
  }
};


template<typename GV, typename RF>
class ConstScalarFunction
  : public Dune::PDELab::GridFunctionBase<
  Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, ConstScalarFunction<GV,RF> >
{
public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

private:
  GV gv;
  const RF value;
  typedef typename Traits::DomainType DomainType;

public:
  //! construct from grid view
  ConstScalarFunction (const GV& gv_, const RF value_)
    : gv(gv_), value(value_)
  {}

  //! evaluate extended function on element
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    y = value;
  }

  //! get a reference to the grid view
  inline const GV& getGridView () const
  {return gv;}
};


template<typename GV, typename RF>
class ConstVectorFunction
  : public Dune::PDELab::GridFunctionBase<
  Dune::PDELab::GridFunctionTraits<GV,RF,GV::dimension,Dune::FieldVector<RF,GV::dimension> >, ConstVectorFunction<GV,RF> >
{
public:
  enum { dim = GV::dimension };
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,dim,Dune::FieldVector<RF,dim> > Traits;

private:
  GV gv;
  const RF valuex;
  const RF valuey;
  typedef typename Traits::DomainType DomainType;

public:
  //! construct from grid view
  ConstVectorFunction (const GV& gv_, const RF valuex_, const RF valuey_)
    : gv(gv_), valuex(valuex_), valuey(valuey_)
  {}

  //! evaluate extended function on element
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    y[0] = valuex;
    y[1] = valuey;
  }

  //! get a reference to the grid view
  inline const GV& getGridView () const
  {return gv;}
};

template<typename GV, typename RF>
class DirichletFunction
  : public Dune::PDELab::GridFunctionBase<
  Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, DirichletFunction<GV,RF> >
{
public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

private:
  GV gv;
  typedef typename Traits::DomainType DomainType;

public:
  //! construct from grid view
  DirichletFunction (const GV& gv_)
    : gv(gv_)
  {}

  //! evaluate extended function on element
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    typedef typename GV::Grid::template Codim<0>::Geometry::GlobalCoordinate Point;
    Point x = e.geometry().global(xlocal);

    y = 1.-x[0];
  }

  //! get a reference to the grid view
  inline const GV& getGridView () const
  {return gv;}
};

template<typename GV, typename RF>
class ZeroFunction
  : public Dune::PDELab::GridFunctionBase<
  Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, ZeroFunction<GV,RF> >
{
public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

private:
  GV gv;
  typedef typename Traits::DomainType DomainType;

public:
  //! construct from grid view
  ZeroFunction (const GV& gv_)
    : gv(gv_)
  {}

  //! evaluate extended function on element
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    // typedef typename GV::Grid::template Codim<0>::Geometry::GlobalCoordinate Point;
    //Point x = e.geometry().global(xlocal);

    y = 0.;
  }

  //! get a reference to the grid view
  inline const GV& getGridView () const
  {return gv;}
};

#endif
