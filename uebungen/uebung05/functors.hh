#ifndef __DUNE_GRID_HOWTO_FUNCTORS_HH__
#define __DUNE_GRID_HOWTO_FUNCTORS_HH__

#include <dune/common/fvector.hh>
#include <dune/geometry/type.hh>

// Skeleton of functor for evaluation of PK simplex basis
template<typename DF, typename RF, int dim, int k>
class PkFunctor{
private:

  // Define our finite element
  typedef Dune::PkLocalFiniteElement<DF,RF,dim,k> PkLocalFE;

  // Get some types it provides
  typedef typename PkLocalFE::Traits::LocalBasisType::Traits::RangeType RT;
  typedef typename PkLocalFE::Traits::LocalBasisType::Traits::RangeFieldType RFT;
  typedef typename PkLocalFE::Traits::LocalBasisType::Traits::DomainType DT;
  typedef std::vector<RFT> Coefficients;

  PkLocalFE local_fe;

  const Coefficients coeffs;

public:

  // We receive the coefficients in the constructor
  PkFunctor(const Coefficients & coeffs_)
    : coeffs(coeffs_)
  {}

  RT operator() (const DT & x) const
  {

    auto local_basis = local_fe.localBasis();

    /* TODO: Implement this functor evaluation!

       Look for the class LocalBasisVirtualInterface in the DUNE documentation.
       It defines an abstract interface that each local basis (like our 'local_basis' here) fulfills.
       Find out how to evaluate the basis functions and use that to implement the desired function.
    */

    RT value = 0.0;





    return value;
  }
};

// Skeleton of functor for evaluation of Monomial basis
template<typename DF, typename RF, int dim, int k>
class MonomialFunctor{

  // TODO: Implement the MonomialFunctor.
  // You can start by copying your PkFunctor and simply replacing PkLocalFiniteElement
  // by MonomialLocalFiniteElement.
  // Note that a MonomialLocalFiniteElement requires the geometry type for its constructor,
  // as it supports both simplices and quadrilaterals.
  // The appropriate GeometryType can be retrieved by Dune::GeometryType(Dune::GeometryTypes::simplex(dim)).

};


template< typename F1, typename F2>
class ProductFunctor
{
  const F1& f1;
  const F2& f2;
public:
  ProductFunctor(const F1& f1_, const F2& f2_)
    : f1(f1_), f2(f2_)
  {}

  template<typename Coordinate>
  double operator() (const Coordinate& x) const
  {
    return f1(x) * f2(x);
  }
};


/**
   This function may be used by the VTKWriter object to evaluate the
   functor object at a given local coordinate.
*/
template<class Grid, class Functor>
class FunctorVTKFunction : public Dune::VTKFunction<Grid>
{
private:

  // Some types which are exported by the base class
  typedef Dune::VTKFunction<Grid> Base;
  typedef typename Base::Entity Entity;
  typedef typename Base::ctype ctype;
  enum { dim =  Base::dim };

  // Constant reference to the functor object.
  const Functor & f;
  const std::string data_name;
public:

  FunctorVTKFunction(const Functor & f_, std::string name_) : f(f_), data_name(name_)
  {}

  //! The number of components of this function. This is necessary as
  //! the VTKWriter would also allow the visualization of vector
  //! valued functions.
  int ncomps() const {return 1;}

  //! Evaluate the functor at a given local coordinate
  double evaluate(int comp, const Entity & e, const Dune::FieldVector<ctype,dim> & xi) const
  {
    auto x_global = e.geometry().global(xi);
    return f(x_global);
  }

  //! This name will later be displayed as a label for the visualized
  //! data in paraview.
  std::string name() const
  {
    return data_name;
  }
};


#endif // __DUNE_GRID_HOWTO_FUNCTORS_HH__
