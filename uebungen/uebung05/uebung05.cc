#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/common/parallel/mpihelper.hh> // include mpi helper class
#include <dune/common/parametertreeparser.hh>
#include <dune/grid/onedgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif
#include <dune/grid/io/file/gmshreader.hh>
#include <sstream>

#include <dune/localfunctions/monomial.hh>
#include <dune/localfunctions/lagrange/pk.hh>
#include "functors.hh"
#include "integrateentity.hh"

//! Write the VTK file.
template<class GridView, class Functor>
void writeVTKFile(const GridView & gridview, const Functor & f, std::string filename)
{
  Dune::SubsamplingVTKWriter<GridView> vtkwriter(gridview,Dune::refinementLevels(0));
  typedef FunctorVTKFunction<GridView,Functor> VTKFunctor;
  auto vtk_f = std::make_shared<VTKFunctor>(f,"Functor");
  vtkwriter.addVertexData(vtk_f);
  vtkwriter.write(filename, Dune::VTK::appendedraw);
  std::cout << "Wrote VTK file" << std::endl;
}

//! integration with given accuracy
template<class Grid, class Functor>
double uniformintegration (Grid& grid, const Functor & f, const int qorder,
                           const int max_level, const double accuracy)
{
  // get GridView on leaf grid - type
  typedef typename Grid :: LevelGridView GridView;

  // loop over grid sequence
  double oldvalue=0;
  const std::string filename_base = "functor_";
  for (int k=0; k<max_level; k++)
    {
      // get GridView instance
      GridView gridView = grid.levelGridView(k);

      // compute integral with some order
      const double value = integrategrid(gridView,f,qorder);

      const double diff = std::abs(value-oldvalue);
      // print result and error estimate
      /*std::cout << "elements="
                << std::setw(8) << std::right
                << gridView.size(0)
                << " integral="
                << std::scientific << std::setprecision(12)
                << value
                << " difference=" << diff
                << std::endl;*/

      if(diff < accuracy)
        return value;

      // save value of integral
      oldvalue=value;

    }

  std::cerr << "Could not achieve sufficient quadrature accuracy." << std::endl;
  exit(1);
  return 0;
}

template<typename Grid, int dim>
Grid* createSingleSimplexGrid()
{
  Dune::GridFactory<Grid> grid_factory;
  typedef typename Grid::template Codim<0>::Geometry::LocalCoordinate Coordinate;
  Coordinate vertex;

  vertex = 0;
  grid_factory.insertVertex(vertex);

  for(int i=0; i<dim; ++i){
    vertex = 0;
    vertex[i] = 1.0;
    grid_factory.insertVertex(vertex);
  }

  const int n_vertices = dim + 1;
  std::vector<unsigned int> indices(n_vertices);
  for(int i=0; i<n_vertices; ++i)
    indices[i]=i;

  Dune::GeometryType simplex_type(Dune::GeometryTypes::simplex(dim));
  grid_factory.insertElement(simplex_type,indices);

  return grid_factory.createGrid();
}

int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // read parameters from file
  Dune::ParameterTree configuration;
  Dune::ParameterTreeParser parser;
  parser.readINITree("uebung05.ini", configuration);

  // start try/catch block to get error messages from dune
  try {
#if HAVE_UG
    using namespace Dune;

    // The dimension of the domain
    const int dim = 2;

    // The polynomial order of the Pk basis
    const int k = 3;

    // Get the corresponding basis dimension
    const unsigned int coeff_size = Dune::MonomImp::Size<dim,k>::val;

    // Type  of the simplex grid
    typedef Dune::UGGrid<dim> Grid;

    // Create a grid consisting of a single simplex embedded in the
    // unit cube.
    Grid* pgrid = createSingleSimplexGrid<Grid,dim>();
    Grid& grid = *pgrid;

    // integrate and compute error with extrapolation
    const int quad_order = configuration.get<int>("integration.order");
    const double max_level = configuration.get<double>("integration.max_level");
    const double accuracy = configuration.get<double>("integration.accuracy");

    // Create grid levels.
    for(int l=0; l<max_level; ++l)
      grid.globalRefine(1);

    // Define Pk basis type
    typedef Grid::ctype DF;
    typedef double RF;
    typedef PkFunctor<DF, RF, dim, k> PKFunctor;
    typedef MonomialFunctor<DF, RF, dim, k> MonomialFunctor;


    // Write VTK files for each basis function
    for(unsigned int c = 0; c < coeff_size; ++c){

      std::vector<RF> coefficients(coeff_size,0);
      coefficients[c] = 1;

      PKFunctor local_functor(coefficients);
      writeVTKFile(grid.leafGridView(), local_functor, "pk_functor_" + std::to_string(c));

      // TODO: Once implemented, print your monomial basis too and check if it looks right



    }


    // A matrix with dimension equal to the number of basis functions (or coefficients)
    Dune::FieldMatrix<double,coeff_size,coeff_size> A(0);

    // TODO: Once you are sure that both functors work fine, fill the matrix A
    // with L2 inner products of the respective Pk and Monomial basis functions.
    // (Use ProductFunctor and uniformintegration appropriately)








    // Check whether the matrix is invertible.
    try {

      std::cout << "Matrix: " << std::endl << A << std::endl;
      Dune::FieldMatrix<double,coeff_size,coeff_size> unit(A);
      unit.invert();
      std::cout << "Inverse: " << std::endl << unit << std::endl;
      unit.rightmultiply(A);
      std::cout << "Unit: " << std::endl << std::fixed << unit << std::endl;

    } catch (Dune::Exception & e) {
      std::cout << "Well, we got an exception down here. Most likely A is not invertible yet." << std::endl
                << "Looks like you still have to do some work :)" << std::endl;
      std::cout << "The error message was: " << e.what() << std::endl;
    }

    delete pgrid;
#endif
  }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
