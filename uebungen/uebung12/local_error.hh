// -*- tab-width: 2; indent-tabs-mode: nil -*-
#ifndef DUNE_NPDE_LOCAL_ERROR_HH
#define DUNE_NPDE_LOCAL_ERROR_HH

#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/geometry/type.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/quadraturerules.hh>

#include<dune/pdelab/common/geometrywrapper.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/gridoperator/common/assemblerutilities.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>

template<typename GV, typename GFG, typename E>
void computeLocalError(const GV & gv, const GFG & gfg, E & errors)
{
  typedef GV GridView;
  typedef typename GV::ctype DF;

  // Works only for P1
  const int int_order = 2;

  // dimensions
  const int dim = GV::dimension;

  errors.resize(0);
  errors.resize(gv.size(0),0);

  // Iterate over cells
  for(auto it = gv.template begin<0>(); it!=gv.template end<0>(); ++it){

    // compute h of cell
    typedef typename GV::template Codim<0>::Geometry::GlobalCoordinate Coord;
    double h = 0;
    const int N = it->geometry().corners();
    for(int i=0; i<N; ++i){
      const Coord vi = it->geometry().corner(i);
      for(int j=i+1; j<N; ++j){
        Coord vj = it->geometry().corner(j);
        vj -= vi;
        h = std::max(h, vj.two_norm());
      }
    }

    // Get global index of cell
    const typename GV::IndexSet::IndexType index = gv.indexSet().index(*it);

    // Iterate over faces
    for(auto iit = gv.ibegin(*it); iit != gv.iend(*it); ++iit) {

      // process only interior faces (=intersections)
      if(!iit->neighbor())
        continue;

      // normal vector of intersection
      const Coord normal = iit->centerUnitOuterNormal();

      // evaluate boundary condition type
      Dune::GeometryType gtface = iit->geometryInInside().type();

      // select quadrature rule
      auto quadrature_rule = Dune::QuadratureRules<DF,dim-1>::rule(gtface,int_order);

      // loop over quadrature points and integrate normal flux
      for (auto quad_point : quadrature_rule)
      {
        const Coord local_s = iit->geometryInInside().global(quad_point.position());
        const Coord local_n = iit->geometryInOutside().global(quad_point.position());
        Coord grad_s; gfg.evaluate(iit->inside(),local_s,grad_s); // Gradient of u_h on current cell
        Coord grad_n; gfg.evaluate(iit->outside(),local_n,grad_n); // Gradient of u_h on neighboring cell

        double integretion_element = iit->geometry().integrationElement(quad_point.position());

        // TODO: Implement eta_t squared from the a-posteriori error estimation here
        // Look up the error estimate in the lecture; Two of the three terms
        // in eta_t vanish for the problem we are solving here!
        // As always when integrating over an entity, consider both the weight
        // of your quadrature point as well as the 'integrationElement' factor
        // of the intersection's geometry. (See numerical integrations in previous exercises)
        // How often will each intersection be visited in this function? Compensate for that factor
        // in your integral.


        errors[index] += 0.0;

      }

    } // iit

    errors[index] = std::sqrt(errors[index]);
  } // it

}

#endif
