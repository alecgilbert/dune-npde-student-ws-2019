// -*- tab-width: 2; indent-tabs-mode: nil -*-
// vi: set et ts=2 sw=2 sts=2:


#ifndef DUNE_NPDE_LAPLACEFEM_HH
#define DUNE_NPDE_LAPLACEFEM_HH

#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/geometry/type.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/quadraturerules.hh>

#include<dune/pdelab/common/geometrywrapper.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/gridoperator/common/assemblerutilities.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>


/** a local operator for solving the Laplace equation
 *
 * A local operator for solving the linear convection-diffusion equation
 * \f{align*}{
 *   -\Delta u &=& 0 \mbox{ in } \Omega,  \\
 *   u &=& g \mbox{ on } \partial\Omega \\
 * \f}
 */

// BC requiring constraints must be >0 if
// constraints assembler coming with PDELab is used
struct LaplaceFEMBoundaryConditions{
  enum Type { Dirichlet=1, Neumann=-1};
};

/** a local operator for solving the Laplace equation defined above
 *
 * with conforming finite elements on all types of grids in any dimension
 \tparam EF Ellipticity Function
 \tparam BF Boundary condition type function
 \tparam DF Dirichlet condition function
 \tparam NF Neumann condition function
 \tparam SF Source function
*/
template<typename EF, typename BF, typename DBF, typename NBF, typename SF>
class LaplaceFEM :
  public Dune::PDELab::NumericalJacobianApplyVolume<LaplaceFEM<EF,BF,DBF,NBF,SF> >,
  public Dune::PDELab::NumericalJacobianApplyBoundary<LaplaceFEM<EF,BF,DBF,NBF,SF> >,
  public Dune::PDELab::NumericalJacobianVolume<LaplaceFEM<EF,BF,DBF,NBF,SF> >,
  public Dune::PDELab::NumericalJacobianBoundary<LaplaceFEM<EF,BF,DBF,NBF,SF> >,
  public Dune::PDELab::FullVolumePattern,
  public Dune::PDELab::LocalOperatorDefaultFlags,
  public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
public:
  // pattern assembly flags
  enum { doPatternVolume = true };

  // residual assembly flags
  enum { doAlphaVolume = true };
  enum { doAlphaBoundary = true };

  LaplaceFEM (const EF & ef_, const BF & bf_, const DBF & dbf_, const NBF & nbf_, const SF & sf_)
    : ef(ef_), bf(bf_), dbf(dbf_), nbf(nbf_), sf(sf_)
  {}

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
    // domain and range field type
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::JacobianType JacobianType;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType RangeType;

    typedef typename LFSU::Traits::SizeType size_type;

    // dimensions
    const int dim = EG::Geometry::mydimension;
    const int dimw = EG::Geometry::coorddimension;

    // select quadrature rule
    Dune::GeometryType gt = eg.geometry().type();
    const int intorder = 2*lfsu.finiteElement().localBasis().order() - 2;
    const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

    // loop over quadrature points
    for (typename Dune::QuadratureRule<DF,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
      {
        // evaluate basis functions
        std::vector<RangeType> phi(lfsu.size());
        lfsu.finiteElement().localBasis().evaluateFunction(it->position(),phi);

        // evaluate gradient of shape functions (we assume Galerkin method lfsu=lfsv)
        std::vector<JacobianType> js(lfsu.size());
        lfsu.finiteElement().localBasis().evaluateJacobian(it->position(),js);

        // transform gradients of shape functions to real element
        const Dune::FieldMatrix<DF,dimw,dim> jac = eg.geometry().jacobianInverseTransposed(it->position());
        std::vector<Dune::FieldVector<RF,dim> > gradphi(lfsu.size());
        for (size_type i=0; i<lfsu.size(); i++)
          jac.mv(js[i][0],gradphi[i]);

        // compute gradient of u
        Dune::FieldVector<RF,dim> gradu(0.0);
        for (size_type i=0; i<lfsu.size(); i++)
          gradu.axpy(x(lfsu,i),gradphi[i]);

        // evaluate ellipticity
        typename EF::Traits::RangeType ellipticity;
        ef.evaluate(eg.entity(),it->position(),ellipticity);

        // evaluate source
        typename SF::Traits::RangeType source;
        sf.evaluate(eg.entity(),it->position(),source);

        // integrate grad u*grad phi_i
        RF factor = it->weight() * eg.geometry().integrationElement(it->position());
        for (size_type i=0; i<lfsu.size(); i++)
          r.accumulate(lfsu,i,(ellipticity * ( gradu*gradphi[i] ) + source * phi[i])*factor);
      }
  }

  // boundary integral
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_boundary (const IG& ig,
                       const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                       R& r_s) const
  {
    // domain and range field type
    typedef typename LFSV::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSV::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSV::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType RangeType;

    typedef typename LFSV::Traits::SizeType size_type;

    // dimensions
    const int dim = IG::Entity::dimension;

    // evaluate boundary condition type
    Dune::GeometryType gtface = ig.geometryInInside().type();
    Dune::FieldVector<DF,dim-1> facecenterlocal
      = Dune::ReferenceElements<DF,dim-1>::general(gtface).position(0,0);

    LaplaceFEMBoundaryConditions::Type bctype;
    typename BF::Traits::RangeType bf_value;
    bf.evaluate(ig,facecenterlocal,bf_value);
    bctype = LaplaceFEMBoundaryConditions::Type(bf_value[0]);

    // skip rest if we are on Dirichlet boundary
    if (bctype==LaplaceFEMBoundaryConditions::Dirichlet) return;

    // select quadrature rule
    const int intorder = 2*lfsu_s.finiteElement().localBasis().order();
    const Dune::QuadratureRule<DF,dim-1>& rule = Dune::QuadratureRules<DF,dim-1>::rule(gtface,intorder);

    // loop over quadrature points and integrate normal flux
    for (typename Dune::QuadratureRule<DF,dim-1>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
      {
        // position of quadrature point in local coordinates of element
        Dune::FieldVector<DF,dim> local = ig.geometryInInside().global(it->position());

        // evaluate shape functions (assume Galerkin method)
        std::vector<RangeType> phi(lfsu_s.size());
        lfsu_s.finiteElement().localBasis().evaluateFunction(local,phi);

        if (bctype==LaplaceFEMBoundaryConditions::Neumann)
          {
            // evaluate flux boundary condition
            typename NBF::Traits::RangeType j;
            nbf.evaluate(ig.inside(),local,j);

            // integrate j
            RF factor = it->weight()*ig.geometry().integrationElement(it->position());
            for (size_type i=0; i<lfsu_s.size(); i++)
              r_s.accumulate(lfsu_s,i,(j*phi[i]*factor ));
          }

      }


  }

private:
  int intorder;
  const EF & ef; // ellipticity function
  const BF & bf; // boundary condition type function
  const DBF & dbf; // Dirichlet condition function
  const NBF & nbf; // Neumann condition function
  const SF & sf; // Source function
};



#endif
