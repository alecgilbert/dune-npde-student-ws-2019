#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <dune/common/parallel/mpihelper.hh> // include mpi helper class
#include<dune/common/parametertreeparser.hh>
#include <dune/grid/onedgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#if HAVE_UG
#include<dune/grid/uggrid.hh>
#endif
#include<dune/grid/io/file/gmshreader.hh>
#include<dune/istl/bvector.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/io.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/functionutilities.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/backend/istl/bcrsmatrixbackend.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridoperator/common/assemblerutilities.hh>
#include<dune/pdelab/gridfunctionspace/genericdatahandle.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/constraints/common/constraints.hh>
#include<dune/pdelab/stationary/linearproblem.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/finiteelementmap/pkfem.hh>
#include<dune/pdelab/constraints/conforming.hh>

#include<dune/pdelab/finiteelementmap/qkfem.hh>

#include "../../dune/npde/utilities/functionutilities.hh"
#include "laplacefem.hh"
#include "local_error.hh"
#include "boundary_conditions.hh"

template<typename GV, typename EI>
void computeSolution(const int step, const GV & gv, EI & errors)
{
  typedef double RF;
  typedef typename GV::ctype DF;
  // make finite element space and solve
  typedef Dune::PDELab::PkLocalFiniteElementMap<GV,DF,RF,1> FEM;
  FEM fem(gv);
  typedef Dune::PDELab::ConformingDirichletConstraints CON;

  // boundary conditions
  typedef AllDirichletBCFunction<GV,LaplaceFEMBoundaryConditions> BF;
  BF bf(gv);
  typedef ConstScalarFunction<GV,RF> NBF;
  NBF nbf(gv,0.0);
  typedef RadialLDomainFunction<GV,RF> DBF;
  DBF dbf(gv);

  // source term
  typedef ConstScalarFunction<GV,RF> SF;
  SF sf(gv,0.0);

  // ellipticity term
  typedef ConstScalarFunction<GV,RF> EF;
  EF ef(gv,1.0);

  const std::string filename_base = "ldomain_" + std::to_string(step);

  // coordinate and result type
  // typedef typename GV::Grid::ctype Coord;
  typedef double Real;
  //typedef EI ErrorIndicators;

  // make grid function space
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem);

  // make constraints container and initialize it
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;
  Dune::PDELab::constraints(bf,gfs,cc);
  std::cout << "constrained dofs=" << cc.size() << " of " << gfs.globalSize() << std::endl;

  // make local operator
  typedef LaplaceFEM<EF,BF,DBF,NBF,SF> LOP;
  LOP lop(ef,bf,dbf,nbf,sf);
  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
  MBE mbe(5);
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;
  GO go(gfs,cc,gfs,cc,lop,mbe);

  // make FE function extending Dirichlet boundary conditions
  typedef typename GO::Traits::Domain U;
  U u(gfs,0.0);
  Dune::PDELab::interpolate(dbf,gfs,u);

  // make linear solver and solve problem
  typedef Dune::PDELab::ISTLBackend_SEQ_CG_SSOR LS;
  LS ls;
  typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
  SLP slp(go,ls,u,1e-8);
  slp.apply();

  // define discrete grid function for solution
  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
  DGF udgf(gfs,u);

  // compute l2 error
  typedef DifferenceSquaredAdapter<DBF,DGF> DifferenceSquared;
  DifferenceSquared differencesquared(dbf,udgf);
  typename DifferenceSquared::Traits::RangeType l2errorsquared(0.0);
  Dune::PDELab::integrateGridFunction(differencesquared,l2errorsquared,10);
  std::cout << std::setw(12) << std::setprecision(4) << std::scientific
  << "L2 error: " <<  sqrt(l2errorsquared) << std::endl;



  Dune::PDELab::DiscreteGridFunctionGradient<GFS,U> gradient_u(gfs,u);
  computeLocalError(gv,gradient_u, errors);

  // write vtk file
  Dune::VTKWriter<GV> vtkwriter(gv);
  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF> >(udgf,"u"));
  vtkwriter.addCellData(errors,"errors");
  vtkwriter.write(filename_base,Dune::VTK::appendedraw);
}

template<typename G, typename EI>
void adaptGrid(G & grid, const EI & indicators)
{
  auto leafgridview = grid.leafGridView();

  // Mark elements for adaptation
  for(auto it = leafgridview.template begin<0>(); it!=leafgridview.template end<0>(); ++it) {
    auto index = leafgridview.indexSet().index(*it);
    if(indicators[index])
      grid.mark(1,*it);
  }

  // Execute adaptation
  std::cout << "Adapting grid from " << grid.size(0);
  grid.preAdapt();
  grid.adapt();
  grid.postAdapt();
  std::cout << " to " << grid.size(0) << " entities." << std::endl;
}

int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // start try/catch block to get error messages from dune
  try {

    // read parameters from file
    Dune::ParameterTree configuration;
    Dune::ParameterTreeParser parser;
    parser.readINITree("uebung12.ini", configuration);

    // UGGrid/Pk2d version
#if HAVE_UG
    // make uggrid
    const int dim = 2;
    typedef Dune::UGGrid<dim> GridType;
    GridType grid;
    std::string grid_file = configuration.get<std::string>("grid.filename");
    // read gmsh file
    Dune::GridFactory<GridType> factory(&grid);
    Dune::GmshReader<GridType>::read(factory,grid_file,true,true);
    factory.createGrid();

    const int max_level = configuration.get<int>("grid.level");
    for (int i=0; i<max_level; i++) grid.globalRefine(1);

    typedef GridType::LeafGridView GV;
    GV gv=grid.leafGridView();

    typedef std::vector<double> ErrorIndicators;
    ErrorIndicators errors;

    const int adaption_steps = configuration.get<int>("adaptation.steps");
    for(int i=0; i<adaption_steps; i++){

      computeSolution(i,gv,errors);

      // Adaption criterion
      const std::string adaption_method = configuration.get<std::string>("adaptation.method");
      if(adaption_method == "treshold"){
        const double refinement_treshold = configuration.get<double>("adaptation.treshold");
        const double max_error = *std::max_element(errors.begin(),errors.end());
        for(unsigned int j=0; j<errors.size(); ++j){
          if(errors[j]/max_error >= refinement_treshold * max_error)
            errors[j] = 1.0;
          else
            errors[j] = 0.0;
        }
      }
      else if(adaption_method == "percentage"){
        const double percentage = configuration.get<double>("adaptation.percentage");
        const int n_adapted_cells = int( double(grid.size(0)) * percentage );
        for(int j=0; j<n_adapted_cells; ++j)
          *( std::max_element(errors.begin(),errors.end()) ) = -1.0;
        for(int j=0; j<grid.size(0); ++j)
          errors[j] = errors[j] > 0 ? 0 : 1.0;
      }
      else {
        DUNE_THROW(Dune::Exception, "Undefined adaptation type selected in ini file!");
      }

      adaptGrid(grid,errors);
    }

#endif
  }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
