// -*- tab-width: 2; indent-tabs-mode: nil -*-
// vi: set et ts=2 sw=2 sts=2:

#ifndef BOUNDARY_CONDITIONS_HH
#define BOUNDARY_CONDITIONS_HH

/*!  A boundary condition type function which produces a pure
  Dirichlet problem */
template<class GV, class BC>
class AllDirichletBCFunction
  : public Dune::PDELab::BoundaryGridFunctionBase<
  Dune::PDELab::BoundaryGridFunctionTraits<
    GV,int,1,Dune::FieldVector<int,1> >,AllDirichletBCFunction<GV,BC> >
{
  GV gv;

public:
  typedef Dune::PDELab::BoundaryGridFunctionTraits<GV,int,1,Dune::FieldVector<int,1> > Traits;
  typedef Dune::PDELab::BoundaryGridFunctionBase<Traits,AllDirichletBCFunction<GV,BC> > BaseT;

  AllDirichletBCFunction (const GV & gv_) : gv(gv_) {}

  template<typename I>
  inline void evaluate (const Dune::PDELab::IntersectionGeometry<I>& ig,
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    // Return only Dirichlet marker value
    y = BC::Dirichlet;
  }

  //! get a reference to the GridView
  inline const GV & getGridView () const
  {
    return gv;
  }
};

template<typename GV, typename RF>
class ConstScalarFunction
  : public Dune::PDELab::GridFunctionBase<
  Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, ConstScalarFunction<GV,RF> >
{
public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

private:
  GV gv;
  const RF value;
  typedef typename Traits::DomainType DomainType;

public:
  //! construct from grid view
  ConstScalarFunction (const GV& gv_, const RF value_)
    : gv(gv_), value(value_)
  {}

  //! evaluate extended function on element
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    y = value;
  }

  //! get a reference to the grid view
  inline const GV& getGridView () const
  {return gv;}
};

template<typename GV, typename RF>
class RadialLDomainFunction
  : public Dune::PDELab::GridFunctionBase<
  Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, RadialLDomainFunction<GV,RF> >
{
public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

private:
  GV gv;
  typedef typename Traits::DomainType DomainType;

public:
  //! construct from grid view
  RadialLDomainFunction (const GV& gv_)
    : gv(gv_)
  {}

  //! evaluate extended function on element
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    auto x = e.geometry().global(xlocal);
    const RF eps = 1e-10;
    const double norm = x.two_norm();
    if(norm < eps)
      y = 0.0;
    else {
      const double pi = 3.141592693589;
      double phi(0);
      if(x[0]>0 && x[1] > 0){
        phi = std::asin(x[1]/norm);
        assert(phi <= pi* 0.5);
        assert(phi >= 0);
      }
      else if(x[0]<=0 && x[1] > 0){
        phi = std::asin(-x[0]/norm) + pi*0.5;
        assert(phi <= pi);
        assert(phi >= 0);
      }
      else if(x[0]<=0 && x[1] <= 0){
        phi = std::asin(-x[1] / norm) + pi;
        assert(phi <= pi*1.5);
        assert(phi >= 0);
      }
      const double PHI = 3./2.*pi;
      y = std::pow(norm,pi/PHI) * std::sin(phi * pi / PHI);
    }

  }

  //! get a reference to the grid view
  inline const GV& getGridView () const
  {return gv;}
};

#endif
