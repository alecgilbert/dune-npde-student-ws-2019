#ifndef __DUNE_GRID_HOWTO_FUNCTORS_HH__
#define __DUNE_GRID_HOWTO_FUNCTORS_HH__

#include <dune/common/fvector.hh>

// a harmonic function
template<typename GV, typename RF>
class Harmonic :
  public Dune::PDELab::AnalyticGridFunctionBase<
  Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,
  Harmonic<GV,RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits,Harmonic<GV,RF> > Base;
  typedef typename Traits::DomainType DT;
  typedef typename Traits::RangeType RT;


  Harmonic (const GV& gv)
    : Base(gv)  {}

  inline void evaluateGlobal (const DT & x, RT & y) const
  {
    y = 0.;
    for (size_t i = 0; i<x.size(); ++i)
      y = 1./(x[i]+0.5);
  }
};

// TODO: Here might be a good place for your implementation of g. Again, copy-paste is allowed ;)




template<typename Grid, typename DGF>
class GridLevelFunction
{
private:
  const DGF & dgf;
  int level;
  typedef typename DGF::Traits::DomainType DT;
  typedef typename DGF::Traits::RangeType RT;
  typedef typename Grid::template Codim<0>::Entity Entity;

public:

  typedef typename DGF::Traits Traits;

  GridLevelFunction(const DGF & dgf_, const int level_)
    : dgf(dgf_), level(level_)
  {}

  void evaluate(const Entity & e, const DT & lx, RT & y) const
  {
    Entity ep(e);
    while(ep.level() != level)
      ep = ep.father();
    const DT gx(e.geometry().global(lx));
    const DT cx(ep.geometry().local(gx));
    dgf.evaluate(ep,cx,y);
  }
};

// The difference between two grid functions
template<typename Grid, typename A, typename B>
class MinusGridFunction
{
private:
  const A & a; // discrete grid function
  const B & b; // discrete grid function
  typedef typename A::Traits::DomainType DT;
  typedef typename A::Traits::RangeType RT;
  typedef typename Grid::template Codim<0>::Entity Entity;

public:

  typedef typename A::Traits Traits;

  MinusGridFunction(const A & a_, const B & b_)
    : a(a_), b(b_)
  {}

  void evaluate(const Entity & e, const DT & lx, RT & y) const
  {
    RT av; a.evaluate(e,lx,av);
    RT bv; b.evaluate(e,lx,bv);
    y = av - bv;
  }
};

// The product of two grid functions
template<typename Grid, typename A, typename B>
class ProductGridFunction
{
private:
  const A & a; // discrete grid function
  const B & b; // discrete grid function
  typedef typename A::Traits::DomainType DT;
  typedef typename A::Traits::RangeType RT;
  typedef typename Grid::template Codim<0>::Entity Entity;

public:

  typedef typename A::Traits Traits;

  ProductGridFunction(const A & a_, const B & b_)
    : a(a_), b(b_)
  {}

  void evaluate(const Entity & e, const DT & lx, RT & y) const
  {
    RT av; a.evaluate(e,lx,av);
    RT bv; b.evaluate(e,lx,bv);
    y = av * bv;
  }
};

#endif // __DUNE_GRID_HOWTO_FUNCTORS_HH__
