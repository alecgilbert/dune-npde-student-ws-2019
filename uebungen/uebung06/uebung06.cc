// -*- tab-width: 2; indent-tabs-mode: nil -*-
// vi: set et ts=2 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/common/parallel/mpihelper.hh> // include mpi helper class
#include <dune/common/parametertreeparser.hh> // include mpi helper class
#include <dune/grid/onedgrid.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif
#include <dune/grid/io/file/gmshreader.hh>
#include <sstream>

#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/finiteelementmap/pkfem.hh>
#include <dune/pdelab/finiteelementmap/pk1d.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>


#include <dune/localfunctions/monomial.hh>
#include <dune/localfunctions/lagrange/pk.hh>

#include"functors.hh"
#include"integrateentity.hh"

template<typename Grid, int dim>
Grid* createSingleSimplexGrid()
{
  Dune::GridFactory<Grid> grid_factory;
  typedef typename Grid::template Codim<0>::Geometry::LocalCoordinate Coordinate;
  Coordinate vertex;

  vertex = 0;
  grid_factory.insertVertex(vertex);

  for(int i=0; i<dim; ++i){
    vertex = 0;
    vertex[i] = 1.0;
    grid_factory.insertVertex(vertex);
  }

  const int n_vertices = dim + 1;
  std::vector<unsigned int> indices(n_vertices);
  for(int i=0; i<n_vertices; ++i)
    indices[i]=i;

  Dune::GeometryType simplex_type(Dune::GeometryTypes::simplex(dim));
  grid_factory.insertElement(simplex_type,indices);

  return grid_factory.createGrid();
}

//! A function demonstrating a finite element space on a grid view
template<class GV, class FEM>
void plot_basis (const GV& gv, const FEM & fem, int order)
{
  // coordinate and result type
  //typedef typename GV::Grid::ctype Coord;
  typedef double Real;

  // make grid function space
  typedef Dune::PDELab::NoConstraints CON;
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem);
  gfs.update();

  // make a std::vector of degree of freedom vectors
  using U = Dune::PDELab::Backend::Vector<GFS,Real>;
  std::vector<U> u(gfs.globalSize(),U(gfs,0.0));

  // write a file for each global finite element function
  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
  std::vector<DGF *> udgf(gfs.globalSize());
  Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,Dune::refinementLevels(6));
  std::cout << "Writing VTK file for " << gfs.globalSize() << " basis functions" << std::endl;
  for (std::size_t i=0; i<u.size(); i++)
    {
      u[i].block(i) = 1.0;
      std::string componentName = "comp_" + std::to_string(i);
      udgf[i] = new DGF(gfs,u[i]);
      vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF> >(*(udgf[i]),componentName));
    }
  vtkwriter.write("basis_" + std::to_string(gfs.globalSize()),Dune::VTK::appendedraw);
  for (std::size_t i=0; i<udgf.size(); i++) delete udgf[i];
}


//! uniform refinement test
template<class Grid, class GridFunction>
double uniformintegration (Grid& grid, const GridFunction & f, const int qorder, const int min_level,
                           const int max_level, const double relative_accuracy)
{
  // loop over grid sequence
  double oldvalue=0;
  for (int k=min_level; k<max_level; k++)
    {
      // get GridView instance
      auto gridView = grid.levelGridView(k);

      // compute integral with some order
      const double value = integrategrid(gridView,f,qorder);

      const double diff = std::abs(value-oldvalue);
      // print result and error estimate
      std::cout << "elements="
                << std::setw(8) << std::right
                << gridView.size(0)
                << " integral="
                << std::scientific << std::setprecision(12)
                << value
                << " difference=" << diff
                << std::endl;

      if(diff < relative_accuracy * value)
        return value;

      // save value of integral
      oldvalue=value;

    }

  std::cerr << "Could not achieve sufficient quadrature accuracy." << std::endl;
  exit(1);
  return 0;
}

//! A function demonstrating a finite element space on a grid view
template<class Grid, class FEM, class Function>
double interpolate_function (const Grid& grid, const int level, const FEM & fem, const Function & f,
                             const int k, Dune::ParameterTree & configuration)
{
  typedef typename Grid::LevelGridView GV;
  typedef double Real;

  // make grid function space
  typedef Dune::PDELab::NoConstraints CON;
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;

  GV gv = grid.levelGridView(level);
  GFS gfs(gv,fem);
  gfs.update();

  std::cout << "Degrees of freedom: " << gfs.globalSize() << std::endl;

  // make a degree of freedom vector
  using U = Dune::PDELab::Backend::Vector<GFS,Real>;
  U u(gfs,0.0);

  // interpolate from a given function
  Dune::PDELab::interpolate(f,gfs,u);

  // create discrete grid function
  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
  DGF interpolated(gfs,u);

  { // Plot interpolation to VTK file
    std::string filename = "interpolated_" + std::to_string(Grid::dimension) + "d_p" + std::to_string(k) + "_" + std::to_string(level);
    Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,Dune::refinementLevels(6));
    vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF> >(interpolated,"u"));
    vtkwriter.write(filename,Dune::VTK::appendedraw);
  }

  // integration parameters
  const int quad_order = configuration.get<int>("integration.order");
  const double max_level = configuration.get<double>("integration.max_level");
  const double relative_accuracy = configuration.get<double>("integration.relative_accuracy");

  // Wrapper which allow to evaluate this function on a finer grid
  // level
  typedef GridLevelFunction<Grid,DGF> DLGF;
  DLGF wrap_interpolated(interpolated,level);

  typedef MinusGridFunction<Grid,DLGF,Function> L2DiffGridFunction;
  L2DiffGridFunction l2diff_gf(wrap_interpolated,f);
  typedef ProductGridFunction<Grid,L2DiffGridFunction,L2DiffGridFunction> L22ErrorGridFunction;
  L22ErrorGridFunction diffsquare_gf(l2diff_gf,l2diff_gf);

  // perform integration on whole grid beginning on the given level
  const double value = uniformintegration(grid,diffsquare_gf,quad_order,level,max_level,relative_accuracy);
  double error = std::sqrt(value);
  std::cout << "Error : " << error << std::endl;

  return error;
}

template<class Grid, class ReferenceFunction, int k>
void interpolateOnLevels2d(Grid & grid, const ReferenceFunction & f,
                           Dune::ParameterTree & configuration)
{
  std::cout << "Interpolate with polynomial order " << k << std::endl;
  typedef typename Grid::LevelGridView GV;
  typedef typename Grid::ctype DF;
  typedef double RF;

  // The PK local basis functions and their coefficients
  // make the finite element map
  typedef Dune::PDELab::PkLocalFiniteElementMap<GV,DF,RF,k> FEM;

  // Plot basis functions in VTK files
  FEM fem_l0(grid.levelGridView(0));
  plot_basis(grid.levelGridView(0),fem_l0,k);

  const int max_level = configuration.get<int>("interpolation.max_level");
  double old_error = 0.0;
  for(int level = 0; level <= max_level; ++level){

    const GV gv = grid.levelGridView(level);
    FEM fem(gv);

    double error = interpolate_function(grid, level, fem, f, k, configuration);
    std::cout << "Convergence order (asymptotic): " << std::log(old_error / error)/std::log(2.0) << std::endl;
    old_error = error;
  }
}

template<class Grid, class ReferenceFunction, int k>
void interpolateOnLevels1d(Grid & grid, const ReferenceFunction & f,
                           Dune::ParameterTree & configuration)
{
  std::cout << "Interpolate with polynomial order " << k << std::endl;
  typedef typename Grid::LevelGridView GV;
  typedef typename Grid::ctype DF;
  typedef double RF;

  // The PK local basis functions and their coefficients
  // make the finite element map
  typedef Dune::PDELab::Pk1dLocalFiniteElementMap<DF,RF> FEM;

  // Plot basis functions in VTK files
  FEM fem_l0(k);
  plot_basis(grid.levelGridView(0),fem_l0,k);

  const int max_level = configuration.get<int>("interpolation.max_level");
  double old_error = 0.0;
  for(int level = 0; level <= max_level; ++level){

    const GV gv = grid.levelGridView(level);
    FEM fem(k);

    double error = interpolate_function(grid, level, fem, f, k, configuration);
    std::cout << "Convergence order (asymptotic): " << std::log(old_error / error)/std::log(2.0) << std::endl;
    old_error = error;
  }
}

int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // read parameters from file
  Dune::ParameterTree configuration;
  Dune::ParameterTreeParser parser;
  parser.readINITree("uebung06.ini", configuration);

  // start try/catch block to get error messages from dune
  try {
    using namespace Dune;

    { // 1d case

      // Set number of vertices
      typedef double ctype;
      const unsigned int N = 1;

      // Define position intervals in reference domain [0,1].
      typedef std::vector<ctype> Intervals;
      Intervals intervals(N+1);
      for(unsigned int i=0; i<N+1; ++i)
        intervals[i] = ctype(i) / ctype(N);

      // Construct grid
      typedef Dune::OneDGrid Grid;
      Grid grid(intervals);

      typedef Grid::LevelGridView GV;
      typedef double RF;

      // Create grid levels.
      const double max_level = configuration.get<double>("integration.max_level");
      for(int l=0; l<max_level; ++l)
        grid.globalRefine(1);

      // The reference function
      typedef Harmonic<GV,RF> ReferenceGridFunction;
      const int reference_host_level = 0;
      ReferenceGridFunction f(grid.levelGridView(reference_host_level));

      // A wrapper which allows to evaluate the reference function on
      // a finer grid level than the host level
      typedef GridLevelFunction<Grid,ReferenceGridFunction> ReferenceGridLevelFunction;
      ReferenceGridLevelFunction lf(f,reference_host_level);

      {
        Dune::SubsamplingVTKWriter<GV> vtkwriter(grid.levelGridView(reference_host_level),Dune::refinementLevels(6));
        vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<ReferenceGridLevelFunction> >(lf,"u"));
        vtkwriter.write("reference_1d",Dune::VTK::appendedraw);
      }

      interpolateOnLevels1d<Grid,ReferenceGridFunction,1>(grid, f, configuration);
      interpolateOnLevels1d<Grid,ReferenceGridFunction,2>(grid, f, configuration);
      interpolateOnLevels1d<Grid,ReferenceGridFunction,3>(grid, f, configuration);
      interpolateOnLevels1d<Grid,ReferenceGridFunction,4>(grid, f, configuration);

    }

    { // 2d case

      // The dimension of the domain
      const int dim = 2;

      // Type  of the simplex grid
      typedef Dune::UGGrid<dim> Grid;

      // Create a grid consisting of a single simplex embedded in the
      // unit cube.
      Grid * pgrid = createSingleSimplexGrid<Grid,dim>();
      Grid & grid = *pgrid;

      typedef Grid::LevelGridView GV;
      typedef double RF;

      // Create grid levels.
      const double max_level = configuration.get<double>("integration.max_level");
      for(int l=0; l<max_level; ++l)
        grid.globalRefine(1);

      // Reference function
      typedef Harmonic<GV,RF> ReferenceGridFunction;
      const int reference_host_level = 0;
      ReferenceGridFunction f(grid.levelGridView(reference_host_level));

      // A wrapper which allows to evaluate the reference function on
      // a finer grid level than the host level
      typedef GridLevelFunction<Grid,ReferenceGridFunction> ReferenceGridLevelFunction;
      ReferenceGridLevelFunction lf(f,reference_host_level);

      {
        // write vtk file
        Dune::SubsamplingVTKWriter<GV> vtkwriter(grid.levelGridView(reference_host_level),Dune::refinementLevels(6));
        vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<ReferenceGridLevelFunction> >(lf,"u"));
        vtkwriter.write("reference_2d",Dune::VTK::appendedraw);
      }

      interpolateOnLevels2d<Grid,ReferenceGridFunction,1>(grid, f, configuration);
      interpolateOnLevels2d<Grid,ReferenceGridFunction,2>(grid, f, configuration);
      interpolateOnLevels2d<Grid,ReferenceGridFunction,3>(grid, f, configuration);
      interpolateOnLevels2d<Grid,ReferenceGridFunction,4>(grid, f, configuration);

      delete pgrid;
    }

    // TODO: Put your 2D unit square, structured grid, Qk element version here.
    // You can start by copying the above 2D case and replacing the UG grid
    // by a Yasp grid. Look th^rough the previous exercises to see how
    // Yasp grid is used (copy-paste is your friend :) ).
    //
    // Then, make use of QkLocalFiniteElementMap instead of PkLocalFiniteElementMap.
    // Ideally, your code should run both Pk and Qk.
    //
    // Have a look at the basis_* VTK outputs to compare the different types of basis functions.
    //
    // Finally, replace the old functor representing f by your implementation of g
    // and conduct the numerical experiments.







  }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
