
//! exact gradient of solution
template<typename GV, typename RF>
class ExactGradient
: public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<GV,RF,GV::dimension,Dune::FieldVector<RF,GV::dimension> >,
                                                                         ExactGradient<GV,RF> >
{
  GV gv;

public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,
  GV::dimension,Dune::FieldVector<RF,GV::dimension> > Traits;
  typedef Dune::PDELab::GridFunctionBase<Traits,ExactGradient<GV,RF> > BaseT;

  ExactGradient (const GV& gv_) : gv(gv_) {}

  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    typename Traits::DomainType x = e.geometry().global(xlocal);

    // TODO: Implement the analytical gradient of the solution here.

    y[0] = 0.0;
    y[1] = 0.0;

  }

  inline const typename Traits::GridViewType& getGridView () const
  {
    return gv;
  }
};
