// -*- tab-width: 2; indent-tabs-mode: nil -*-
// vi: set et ts=2 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/common/parallel/mpihelper.hh> // include mpi helper class
#include <dune/common/parametertreeparser.hh>
#include <dune/grid/onedgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/grid/yaspgrid.hh>
#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/geometry/type.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/io.hh>
#include <dune/istl/paamg/amg.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/common/functionutilities.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/backend/istl/bcrsmatrixbackend.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/stationary/linearproblem.hh>
#include <dune/pdelab/finiteelementmap/pkfem.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>

#include "../../dune/npde/utilities/functionutilities.hh"
#include "../../dune/npde/utilities/meshsize.hh"

#include "problem.hh"
#include "exact_gradient.hh"


//! solve the diffusion problem on a given GridView with a given finite element space
template<class GV, class FEM, class CON>
void driver (const GV& gv, FEM& fem, std::string filename_base, int& N, double& l2e, double& h1se)
{
  typedef double Real;



  // Solve PDE
  // ==============

  // Make grid function space
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem);
  gfs.update();
  N = gfs.globalSize();

  // Define problem parameters
  typedef ConvectionDiffusionProblem<GV,double> Problem;
  Problem problem;

  // Make a degree of freedom (DOF) vector representing u_h and initialize it with Dirichlet extension
  using U = Dune::PDELab::Backend::Vector<GFS,Real>;
  U u(gfs,0.0);
  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<Problem> G; // extract Dirichlet extension (u_g) from problem class
  G g(gv,problem);
  Dune::PDELab::interpolate(g,gfs,u);                                            // set coefficients such that u_h interpolates u_g

  // Setup constraints container (stores indices of Dirichlet constrained DOFs)
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;
  Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<Problem> bctype(gv,problem); // extract boundary condition types from problem class
  Dune::PDELab::constraints(bctype,gfs,cc);                                              // setup constraints
  Dune::PDELab::set_nonconstrained_dofs(cc,0.0,u);                                       // set non-constrained DOFs of u to zero

  std::cout << "constrained dofs=" << cc.size() << " of " << gfs.globalSize() << std::endl;

  // Prepare matrix assembly
  typedef Dune::PDELab::ConvectionDiffusionFEM<Problem,FEM> LOP;                   // predefined local operator representing the bilinear form per element
  LOP lop(problem);                                                                // for the class of PDEs in problem.hh
  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
  MBE mbe(5);
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;     // the assembler type
  GO go(gfs,cc,gfs,cc,lop,mbe);                                                    // the assembler (is able to assemble jacobian, residuum, ...)


  // Run matrix assembly and linear solver
  typedef Dune::PDELab::ISTLBackend_SEQ_CG_ILU0 LS;                // linear solver type from solver backend
  LS ls(2000);
  typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;// type for linear problem solver (assembles A,b, solves it)
  SLP slp(go,ls,u,1e-12);
  slp.apply();

  // TODO: What are the parameters set to 2000 and 1e-12 here? Use the PDELab documentation.






  // Compute errors
  // ==============

  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;  // type for discrete function on grid
  DGF udgf(gfs,u);                                        // discrete grid function (DOFs from vector u)

  typedef DifferenceAdapter<G,DGF> Difference;            // type for difference of 2 DGF (analytical solution u and numerical solution u_h)
  Difference difference(g,udgf);                          // solution difference discrete grid function


  // Note that, as our Dirichlet constraint g is already defined
  // to be the analytical soultion, we can simply use it as our reference solution
  typedef DifferenceSquaredAdapter<G,DGF> DifferenceSquared;               // type for difference of 2 DGF (analytical solution u and numerical solution u_h)
  DifferenceSquared differencesquared(g,udgf);                             // solution difference discrete grid function

  typename DifferenceSquared::Traits::RangeType l2errorsquared(0.0);       // L2 error
  Dune::PDELab::integrateGridFunction(differencesquared,l2errorsquared,10);// compute L2 error squared
  l2e = sqrt(l2errorsquared);                                              // (u-u_h) L2 error


  // TODO: Compute the H1 seminorm of the error here.
  // You can follow the same approach as for the L2 norm above.
  // In contrast to the L2 norm, we need the solution's gradient instead
  // of the solution itself. You can obtain it by creating a
  // DiscreteGridFunctionGradient in analogy to the DiscreteGridFunction above.
  // As your reference, use the ExactGradient.
  // (Make sure to pass the right types as template parameters).




  h1se = 0.0; // Return the H1 seminorm here







  // Write VTK output
  // ================


  // TODO: Here you can try SubsamplingVTKWriter instead of VTKWriter. What does it do?
  Dune::VTKWriter<GV> vtkwriter(gv);

  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF> >(udgf,"u_h"));
  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<G> >(g,"u"));
  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<Difference> >(difference,"u-u_h"));
  vtkwriter.write(filename_base,Dune::VTK::appendedraw);
}


int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  try {

    // read parameters from file
    Dune::ParameterTree configuration;
    Dune::ParameterTreeParser parser;
    parser.readINITree("uebung08.ini", configuration);

    std::string grid_file = configuration.get<std::string>("grid.filename");
    const int max_level = configuration.get<int>("grid.max_level");
    const int degree = configuration.get<int>("method.degree");

    std::vector<double> l2(max_level+1,0.0);
    std::vector<double> h1s(max_level+1,0.0);
    std::vector<int> N(max_level+1,0);
    std::vector<double> h(max_level+1,0.0);


    // make uggrid
    typedef Dune::UGGrid<2> GridType;
    GridType grid;
    // read gmsh file
    Dune::GridFactory<GridType> factory(&grid);
    Dune::GmshReader<GridType>::read(factory,grid_file,true,true);
    factory.createGrid();

    // Solve for all refinement levels
    for (int i=0; i<=max_level; i++)
    {
      typedef GridType::LeafGridView GV;
      GV gv=grid.leafGridView();
      h[i] = meshsize(gv);

      // Call driver for chosen polynomial degree
      std::string filename = "square_" + grid_file + "_l" + std::to_string(i) + "_k" + std::to_string(degree);
      typedef Dune::PDELab::ConformingDirichletConstraints CON;

      if (degree==1) {
        typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GridType::ctype,double,1> FEM;
        FEM fem(gv);
        driver<GV,FEM,CON>(gv,fem,filename,N[i],l2[i],h1s[i]);
      }
      if (degree==2) {
        typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GridType::ctype,double,2> FEM;
        FEM fem(gv);
        driver<GV,FEM,CON>(gv,fem,filename,N[i],l2[i],h1s[i]);
      }
      if (degree==3) {
        typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GridType::ctype,double,3> FEM;
        FEM fem(gv);
        driver<GV,FEM,CON>(gv,fem,filename,N[i],l2[i],h1s[i]);
      }
      if (degree==4) {
        typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GridType::ctype,double,4> FEM;
        FEM fem(gv);
        driver<GV,FEM,CON>(gv,fem,filename,N[i],l2[i],h1s[i]);
      }
      if (degree==5) {
        typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GridType::ctype,double,5> FEM;
        FEM fem(gv);
        driver<GV,FEM,CON>(gv,fem,filename,N[i],l2[i],h1s[i]);
      }
      if (i<max_level) grid.globalRefine(1);
    }


    // Print numerical convergene rates
    std::cout << "Results on mesh=" << grid_file << " k=" << degree << std::endl;
    std::cout << "\tN\t  1/h\t\tl2\tl2rate\t  h1semi\th1semirate" << std::endl;
    for (int i=0; i<=max_level; i++)
    {
      double rate1=0.0;
      if (i>0) rate1=log(l2[i]/l2[i-1])/log(0.5);
      double rate2=0.0;
      if (i>0) rate2=log(h1s[i]/h1s[i-1])/log(0.5);

      std::cout << std::setw(12) << N[i]
        << std::setw(12) << std::setprecision(4) << std::scientific << 1.0/h[i]
        << std::setw(12) << std::setprecision(4) << std::scientific << l2[i]
        << std::setw(12) << std::setprecision(4) << std::scientific << rate1
        << std::setw(12) << std::setprecision(4) << std::scientific << h1s[i]
        << std::setw(12) << std::setprecision(4) << std::scientific << rate2
        << std::endl;
    }


  }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  return 0;
}
