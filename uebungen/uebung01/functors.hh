// -*- Tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:

#ifndef __DUNE_GRID_HOWTO_FUNCTORS_HH__
#define __DUNE_GRID_HOWTO_FUNCTORS_HH__

#include <dune/common/fvector.hh>

// a smooth function
template<typename ct, int dim>
class Exp {
public:
  Exp () {midpoint = 0.5;}
  double operator() (const Dune::FieldVector<ct,dim>& x) const
  {
    Dune::FieldVector<ct,dim> y(x);
    y -= midpoint;
    return exp(-3.234*(y*y));
  }
private:
  Dune::FieldVector<ct,dim> midpoint;
};

// a function with a local feature
template<typename ct, int dim>
class Needle {
public:
  Needle ()
  {
    midpoint = 0.5;
    midpoint[dim-1] = 1;
  }
  double operator() (const Dune::FieldVector<ct,dim>& x) const
  {
    Dune::FieldVector<ct,dim> y(x);
    y -= midpoint;
    return 1.0/(1E-4+y*y);
  }
private:
  Dune::FieldVector<ct,dim> midpoint;
};

/**
   This function may be used by the VTKWriter object to evaluate the
   functor object at a given local coordinate.
*/
template<class Grid, class Functor>
class FunctorVTKFunction : public Dune::VTKFunction<Grid>
{
private:

  // Some types which are exported by the base class
  typedef Dune::VTKFunction<Grid> Base;
  typedef typename Base::Entity Entity;
  typedef typename Base::ctype ctype;
  enum { dim =  Base::dim };

  // Constant reference to the functor object.
  const Functor & f;
public:

  FunctorVTKFunction(const Functor & f_) : f(f_)
  {}

  //! The number of components of this function. This is necessary as
  //! the VTKWriter would also allow the visualization of vector
  //! valued functions.
  int ncomps() const {return 1;}

  //! Evaluate the functor at a given local coordinate
  double evaluate(int comp, const Entity & e, const Dune::FieldVector<ctype,dim> & xi) const
  {
    auto global_coordinate = e.geometry().global(xi);
    return f(global_coordinate);
  }

  //! This name will later be displayed as a label for the visualized
  //! data in paraview.
  std::string name() const
  {
    return std::string("Gamma function");
  }
};


#endif // __DUNE_GRID_HOWTO_FUNCTORS_HH__
