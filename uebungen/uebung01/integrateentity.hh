// -*- Tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:

#ifndef DUNE_INTEGRATE_ENTITY_HH
#define DUNE_INTEGRATE_ENTITY_HH

#include<dune/common/exceptions.hh>
#include <dune/geometry/quadraturerules.hh>

//! compute integral of function over entity with given order
template<class Iterator, class Functor>
double integrateentity (const Iterator& it, const Functor& f, int p)
{
  // dimension of the entity
  const int dim = Iterator::Entity::dimension;

  // type used for coordinates in the grid
  typedef typename Iterator::Entity::Geometry::ctype ct;

  // get geometry type
  Dune::GeometryType gt = it->type();

  // get quadrature rule of order p
  auto rule = Dune::QuadratureRules<ct,dim>::rule(gt, p);

  // ensure that rule has at least the requested order
  if (rule.order()<p)
    DUNE_THROW(Dune::Exception,"integration order not available");

  // compute approximate integral
  double result = 0;
  for (auto quad_point = rule.begin(); quad_point != rule.end(); ++quad_point)
  {
    double fval = f(it->geometry().global(quad_point->position()));
    double weight = quad_point->weight();
    double detjac = it->geometry().integrationElement(quad_point->position());
    result += fval * weight * detjac;
  }

  return result;
}

#endif
