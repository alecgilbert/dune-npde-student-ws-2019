#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <dune/common/parallel/mpihelper.hh> // include mpi helper class
#include<dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#if HAVE_UG
#include<dune/grid/uggrid.hh>
#endif
#include<dune/grid/io/file/gmshreader.hh>

#include"functors.hh"
#include"integrateentity.hh"

//! Write the VTK file.
template<class GridView, class Functor>
void writeVTKFile(const GridView & gridview, const Functor & f, std::string filename)
{
  Dune::SubsamplingVTKWriter<GridView> vtkwriter(gridview,Dune::refinementLevels(0));
  typedef FunctorVTKFunction<GridView,Functor> FunctorVTKFunction;
  auto vtk_function = std::make_shared<FunctorVTKFunction>(f);
  vtkwriter.addVertexData(vtk_function);
  vtkwriter.write(filename, Dune::VTK::appendedraw);
  std::cout << "Wrote VTK file" << std::endl;
}

template<class Grid>
void uniformintegration (Grid& grid)
{
  // function to integrate
  Exp<typename Grid::ctype, Grid::dimension> function;

  // loop over grid levels
  double oldvalue = 0;
  const int max_level = 9;
  for (int level = 0; level < max_level; level++)
  {
    // get GridView instance
    auto gridView = grid.leafGridView();

    // Write VTK file
    writeVTKFile(gridView, function, "functor_" + std::to_string(level));

    // Iterate over elements and integrate
    double value = 0.0;
    for (auto iterator = gridView.template begin<0>(); iterator != gridView.template end<0>(); ++iterator) {

      // TODO: Here is a good point to start working on the exercise.
      // From the grid iterator, you can obtain the element's geometry via iterator->geometry().
      // Consult its documentation to see how you can determine element corner coordinates:
      // https://dune-project.org/doxygen/2.5.1/classDune_1_1Geometry.html

      value += integrateentity(iterator, function, 1);
    }

    // print results
    std::cout << "elements="
              << std::setw(8) << std::right
              << grid.size(0)
              << " integral="
              << std::scientific << std::setprecision(12)
              << value
              << " difference=" << std::abs(value-oldvalue)
              << std::endl;

    // save value of integral
    oldvalue = value;

    // refine all elements
    if(level < max_level - 1)
      grid.globalRefine(1);
  }
}

int main(int argc, char **argv)
{
  // initialize MPI
  Dune::MPIHelper::instance(argc,argv);

  // start try/catch block to get error messages
  try {

    Dune::FieldVector<double,2> domain_size(1.0);
    std::array<int,2> elements = {1, 1};
    std::bitset<2> periodicity(0);
    Dune::YaspGrid<2> grid(domain_size, elements, periodicity);

    // integrate and compute error with extrapolation
    uniformintegration(grid);
  }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  return 0;
}
